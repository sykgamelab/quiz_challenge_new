from openpyxl import load_workbook
import json
import sys

def getIndex(s):
    if 'B' in s:
        return 0
    elif 'C' in s:
        return 1
    elif 'D' in s:
        return 2
    elif 'E' in s:
        return 3

def isInt(value):
    try:
        if value == int(value):
            return True
    except:
        return False

def createFiles(path):
    for sheetName in sheetnames:
        number = 2
        data = { "items": [] }
        
        i = 0
        while True:
            cellName = 'A' + str(number)
            val = wb[sheetName][cellName].value
            
            if val == None:
                break
            
            item = {"text": val}
            item["answers"] = []
            item["correct"] = -1

            for letter in letters:
                cellName = letter + str(number)
                val = wb[sheetName][cellName].value

                if isInt(val):
                    item["answers"].append(str(int(val)))
                else:
                    item["answers"].append(str(val))

                color = wb[sheetName][cellName].fill.fgColor.rgb

                r  = int(color[2:4],16)
                g  = int(color[4:6],16)
                b  = int(color[6:],16)
                
                #if r == 255 and g == 255 and b == 255:
                 #   print(sheetName + " " +  str(i) + ": "  + item["text"] + " не имеет правильного ответа")
                 #   exit(-1)
                if r < 150 and g > 200 and b < 150:
                 #   print(str(r) + " " + str(g) + " " + str(b))
                    correctAnswer = getIndex(cellName)
                    item["correct"] = correctAnswer

            if item["correct"] == -1:
                print(sheetName + " " +  str(i) + ": "  + item["text"] + " не имеет правильного ответа")
                # exit(-1)

            number += 1
            data["items"].append(item)
            i += 1
            if i % 25 == 0:
              #  print("time to category " + sheetName + str(int(i/25))) 
                with open('result/{0}/{1}.json'.format(path, sheetName + str(int(i/25) - 1)), 'w', encoding="utf-8") as outfile:
                    json.dump(data, outfile, ensure_ascii=False)
                    outfile.close
                    data = { "items": [] }

if __name__ == "__main__":
    if len(sys.argv) == 2:
        wb = load_workbook("xlsx/" +  sys.argv[1] + ".xlsx", read_only=True)
        sheetnames =  wb.sheetnames
        letters = ('B', 'C', 'D', 'E')
        
        createFiles(sys.argv[1])
    else:
        print('No argument in command line!')