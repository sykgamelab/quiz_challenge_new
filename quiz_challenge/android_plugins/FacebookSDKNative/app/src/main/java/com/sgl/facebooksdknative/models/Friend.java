package com.sgl.facebooksdknative.models;

public class Friend {
    String fullName, url, id;

    public Friend(String fullName, String url, String id) {
        this.fullName = fullName;
        this.url = url;
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }
}
