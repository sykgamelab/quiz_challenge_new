package com.sgl.facebooksdknative.views;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.sgl.facebooksdknative.services.FacebookSDKService;

import java.util.Arrays;
import java.util.List;

public class FacebookSDK extends AppCompatActivity {
    CallbackManager callbackManager;
    private static boolean isLoginDone = false;
    private static boolean authenticationSuccess = false;
    private static boolean authenticationCancel = false;
    private static String lastError = null;
    private static final String TAG = "FacebookSDK";
    private enum Command {
        login,
        showFriendList
    }

    public boolean isLoginDone() {
        return isLoginDone;
    }

    public boolean isLoggedIn() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        if (token == null || token.isExpired())
            return false;

        return true;
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG,"START onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void login(String[] permsArray, final String errorTitle) {
        Log.d(TAG, "START AUTH");
        callbackManager = CallbackManager.Factory.create();
        List<String> permsList = Arrays.asList(permsArray);
        LoginManager manager = LoginManager.getInstance();
        manager.registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d(TAG, "FB AUTH SUCCESS");
                    authenticationSuccess = true;
                    isLoginDone = true;
                    finish();
                }

                @Override
                public void onCancel() {
                    Log.d(TAG, "FB AUTH CANCEL");
                    authenticationCancel = true;
                    isLoginDone = true;
                    finish();
                }

                @Override
                public void onError(FacebookException error) {
                    lastError = error.getLocalizedMessage();
                    Log.d(TAG, "login: error = " + lastError);
                    isLoginDone = true;
                    showAlertDialog(errorTitle, lastError, false);
                }
            });
        Log.d(TAG, "END AUTH");
        manager.logInWithReadPermissions(this, permsList);
    }

    public boolean isAuthenticationSuccess() {
        return authenticationSuccess;
    }

    public boolean isAuthenticationCancel() {
        return authenticationCancel;
    }

    public String getLastError() {
        return lastError;
    }

    public static void setLastError(String error) {
        lastError = error;
    }

    private void resetAuthVars() {
        isLoginDone = false;
        authenticationSuccess = false;
        authenticationCancel = false;
        lastError = null;
    }

    public void loginCommand(Activity activity, String[] permissions, String errorTitle) {
        Log.d(TAG, "FB SDK AUTH COMMAND");
        resetAuthVars();
        Intent intent = new Intent(activity, FacebookSDK.class);
        intent.putExtra("cmd", Command.login.toString());
        intent.putExtra("permissions", permissions);
        intent.putExtra("errorTitle", errorTitle);
        activity.startActivity(intent);
    }

    public void showFriendListCommand(Activity activity, String graphPath, String ids,
            String alertTitleError, String alertTitleInfo, String alertMessageNoFriends,
            String alertMessageNoFriendsNow, String alertMessageConfirmSend, String alertMessageConfirmClose,
            String appRequestMessage, String alertButtonYesText, String alertButtonNoText,
            String actionBarTitle, String buttonSendText) {
        Log.d(TAG, "RUN graphRequestCommand");

        FacebookSDKService.getInstance().setSendedIdFriend(null);

        Intent intent = new Intent(activity, FacebookSDK.class);
        intent.putExtra("cmd", Command.showFriendList.toString());
        intent.putExtra("graphPath", graphPath);
        intent.putExtra("ids", ids);
        intent.putExtra("alertTitleError", alertTitleError);
        intent.putExtra("alertTitleInfo", alertTitleInfo);
        intent.putExtra("alertMessageNoFriends", alertMessageNoFriends);
        intent.putExtra("alertMessageNoFriendsNow", alertMessageNoFriendsNow);
        intent.putExtra("alertMessageConfirmSend", alertMessageConfirmSend);
        intent.putExtra("alertMessageConfirmClose", alertMessageConfirmClose);
        intent.putExtra("appRequestMessage", appRequestMessage);
        intent.putExtra("alertButtonYesText", alertButtonYesText);
        intent.putExtra("alertButtonNoText", alertButtonNoText);
        intent.putExtra("actionBarTitle", actionBarTitle);
        intent.putExtra("buttonSendText", buttonSendText);
        activity.startActivity(intent);
    }

    private void showFriendList(String ids, String alertTitleError, String alertTitleInfo,
            String alertMessageConfirmSend, String alertMessageConfirmClose,
            String appRequestMessage, String alertButtonYesText, String alertButtonNoText,
            String actionBarTitle, String buttonSendText) {
        Log.d(TAG, "RUN FacebookSDK.showFriendList");
        Intent intent = new Intent(this, FriendListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("ids", ids);
        intent.putExtra("alertTitleError", alertTitleError);
        intent.putExtra("alertTitleInfo", alertTitleInfo);
        intent.putExtra("alertMessageConfirmSend", alertMessageConfirmSend);
        intent.putExtra("alertMessageConfirmClose", alertMessageConfirmClose);
        intent.putExtra("appRequestMessage", appRequestMessage);
        intent.putExtra("alertButtonYesText", alertButtonYesText);
        intent.putExtra("alertButtonNoText", alertButtonNoText);
        intent.putExtra("actionBarTitle", actionBarTitle);
        intent.putExtra("buttonSendText", buttonSendText);
        startActivity(intent);
        finish();
    }

    private void showAlertDialog(String title, String message, final boolean cancelable) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(cancelable);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FacebookSDKService.getInstance().setSendedIdFriend("");
                finish();
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hideSystemUI();

        Bundle bundle = getIntent().getExtras();
        String cmd = bundle.getString("cmd", "");
        Log.d(TAG, "FacebookSDK.onCreate: cmd = " + cmd);
        if (cmd.equals(Command.login.toString())) {
            String[] permissions = bundle.getStringArray("permissions");
            String errorTitle = bundle.getString("errorTitle");
            login(permissions, errorTitle);
        } else if (cmd.equals(Command.showFriendList.toString())) {
            String graphPath = bundle.getString("graphPath");
            String ids = bundle.getString("ids");
            String alertTitleError = bundle.getString("alertTitleError");
            String alertTitleInfo = bundle.getString("alertTitleInfo");
            String alertMessageNoFriends = bundle.getString("alertMessageNoFriends");
            String alertMessageNoFriendsNow = bundle.getString("alertMessageNoFriendsNow");
            String alertMessageConfirmSend = bundle.getString("alertMessageConfirmSend");
            String alertMessageConfirmClose = bundle.getString("alertMessageConfirmClose");
            String appRequestMessage = bundle.getString("appRequestMessage");
            String alertButtonYesText = bundle.getString("alertButtonYesText");
            String alertButtonNoText = bundle.getString("alertButtonNoText");
            String actionBarTitle = bundle.getString("actionBarTitle");
            String buttonSendText = bundle.getString("buttonSendText");
            if (ids.equals("0")) {
                showAlertDialog(alertTitleInfo, alertMessageNoFriends, false);
            } else if (ids.equals("")) {
                showAlertDialog(alertTitleInfo, alertMessageNoFriendsNow, false);
            } else {
                FacebookSDKService instance =  FacebookSDKService.getInstance();
                instance.graphRequest(graphPath);
                while (!instance.isGraphRequestDone()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                showFriendList(ids, alertTitleError, alertTitleInfo, alertMessageConfirmSend,
                        alertMessageConfirmClose, appRequestMessage, alertButtonYesText, alertButtonNoText,
                        actionBarTitle, buttonSendText);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void hideSystemUI() {
        Log.d(TAG, "RUN hideSystemUI");
        getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}