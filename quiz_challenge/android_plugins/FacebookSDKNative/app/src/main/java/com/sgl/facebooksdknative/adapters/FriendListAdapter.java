package com.sgl.facebooksdknative.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.sgl.facebooksdknative.R;
import com.sgl.facebooksdknative.models.Friend;
import com.sgl.facebooksdknative.services.FacebookSDKService;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {
    private ArrayList<Friend> dataset;
    private Context context;
    private AlertDialog alertDialog;
    private final String TAG = "FacebookSDK";
    private String appRequestMessage, alertButtonYesText, alertButtonNoText, buttonSendText;

    public FriendListAdapter(ArrayList<Friend> dataset, Context context, AlertDialog alertDialog,
             String appRequestMessage, String alertButtonYesText, String alertButtonNoText, String buttonSendText) {
        this.dataset = dataset;
        this.context = context;
        this.alertDialog = alertDialog;
        this.appRequestMessage = appRequestMessage;
        this.alertButtonYesText = alertButtonYesText;
        this.alertButtonNoText = alertButtonNoText;
        this.buttonSendText = buttonSendText;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView fullName;
        private CircleImageView image;
        private Button btnSend;

        public ViewHolder(@NonNull View view) {
            super(view);
            fullName = view.findViewById(R.id.friend_full_name);
            image = view.findViewById(R.id.friend_image);
            btnSend = view.findViewById(R.id.friend_btn_send);
        }
    }

    @NonNull
    @Override
    public FriendListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.friend_item, parent, false);
        return new FriendListAdapter.ViewHolder(view);
    }

    private void appRequest(String id) {
        Log.d(TAG, "FriendListAdapter.appRequest: id = " + id);
        FacebookSDKService instance = FacebookSDKService.getInstance();
        instance.graphRequest(String.format("/%s/apprequests?action_type=SEND&message=%s", id, appRequestMessage));
        instance.setSendedIdFriend(id);
        ((Activity) context).finish();
    }

    @Override
    public void onBindViewHolder(@NonNull FriendListAdapter.ViewHolder holder, final int position) {
        holder.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, alertButtonYesText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appRequest(dataset.get(position).getId());
                    }
                });
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, alertButtonNoText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
        holder.btnSend.setText(buttonSendText);

        holder.fullName.setText(dataset.get(position).getFullName());

        Calendar calendar = Calendar.getInstance();
        int versionNumber = calendar.get(Calendar.WEEK_OF_YEAR) * 100 + calendar.get(Calendar.YEAR);

        RequestListener requestListener = new RequestListener() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                Log.d(TAG, "FriendListAdapter.onBindViewHolder.onLoadFailed: not internet connection");
                FacebookSDKService.getInstance().setSendedIdFriend("error_connection");
                ((Activity) context).finish();
                return false;
            }

            @Override
            public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        };

        String url = dataset.get(position).getUrl();
        Glide
            .with(context)
            .load(url)
            .centerCrop()
            .apply(new RequestOptions()
                .placeholder(R.drawable.default_profile)
                .signature(new IntegerVersionSignature(versionNumber)))
            .timeout(10000)
            .listener(requestListener)
            .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class IntegerVersionSignature implements Key {
        private int currentVersion;

        public IntegerVersionSignature(int currentVersion) {
            this.currentVersion = currentVersion;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof IntegerVersionSignature) {
                IntegerVersionSignature other = (IntegerVersionSignature) o;
                return currentVersion == other.currentVersion;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return currentVersion;
        }

        @Override
        public void updateDiskCacheKey(MessageDigest md) {
            md.update(ByteBuffer.allocate(Integer.SIZE).putInt(currentVersion).array());
        }
    }
}