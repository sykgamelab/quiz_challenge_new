package com.sgl.facebooksdknative.views;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.sgl.facebooksdknative.R;
import com.sgl.facebooksdknative.adapters.FriendListAdapter;
import com.sgl.facebooksdknative.models.Friend;
import com.sgl.facebooksdknative.services.FacebookSDKService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FriendListActivity extends AppCompatActivity {
    private static final String TAG = "FacebookSDK";
    private RecyclerView recyclerView;
    private FriendListAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<Friend> dataset;
    private boolean clearSendedIdFriend = false;
    private String alertTitleInfo, alertMessageConfirmClose, alertButtonYesText, alertButtonNoText;

    public FriendListActivity() {
        dataset = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        actionBar.setTitle(bundle.getString("actionBarTitle"));
        setContentView(R.layout.activity_friend_list);

        recyclerView = findViewById(R.id.friend_list);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        String ids = bundle.getString("ids");
        String alertTitleError = bundle.getString("alertTitleError");
        alertTitleInfo = bundle.getString("alertTitleInfo");
        String alertMessageConfirmSend = bundle.getString("alertMessageConfirmSend");
        alertMessageConfirmClose = bundle.getString("alertMessageConfirmClose");
        String appRequestMessage = bundle.getString("appRequestMessage");
        alertButtonYesText = bundle.getString("alertButtonYesText");
        alertButtonNoText = bundle.getString("alertButtonNoText");
        String buttonSendText = bundle.getString("buttonSendText");
        fillDataset(ids, alertTitleError);

        adapter = new FriendListAdapter(dataset, this, getAlertDialog(alertTitleInfo, alertMessageConfirmSend, true),
                appRequestMessage, alertButtonYesText, alertButtonNoText, buttonSendText);
        recyclerView.setAdapter(adapter);
    }

    private void fillDataset(String ids, String alertTitleError) {
        Log.d(TAG, "RUN FriendListActivity.fillDataset: ids = " + ids);
        FacebookSDKService instance = FacebookSDKService.getInstance();
        JSONObject response = instance.getGraphResponseObject();
        try {
            JSONArray data = response.getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                JSONObject obj = data.getJSONObject(i);
                String fullName = String.format("%s %s", obj.getString("first_name"), obj.getString("last_name"));
                String url = obj.getJSONObject("picture").getJSONObject("data").getString("url");
                String id = obj.getString("id");
                if (ids.contains(id)) {
                    dataset.add(new Friend(fullName, url, id));
                }
            }
        } catch (JSONException e) {
            AlertDialog alertDialog = getAlertDialog(alertTitleError, e.getLocalizedMessage(), false);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    clearSendedIdFriend = true;
                    finish();
                }
            });
            alertDialog.show();
        }
    }

    private AlertDialog getAlertDialog(String title, String message, final boolean cancelable) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(cancelable);

        return alertDialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        clearSendedIdFriend = false;
        AlertDialog alertDialog = getAlertDialog(alertTitleInfo, alertMessageConfirmClose, true);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, alertButtonYesText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearSendedIdFriend = true;
                finish();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, alertButtonNoText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        if (clearSendedIdFriend) FacebookSDKService.getInstance().setSendedIdFriend("");
        super.onDestroy();
    }
}
