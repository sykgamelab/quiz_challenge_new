package com.sgl.facebooksdknative.services;

import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.sgl.facebooksdknative.views.FacebookSDK;

import org.json.JSONObject;

public class FacebookSDKService {
    private static String graphResponse = null;
    private static JSONObject graphResponseObject = null;
    private static boolean graphRequestDone = false;
    private final String TAG = "FacebookSDK";
    private static FacebookSDKService instance = null;
    private static String sendedIdFriend = null;

    private FacebookSDKService() {}

    public static FacebookSDKService getInstance() {
        if (instance == null) {
            instance = new FacebookSDKService();
        }

        return instance;
    }

    public void graphRequest(final String graphPath) {
        Log.d(TAG, "RUN graphRequest: graphPath = " + graphPath);
        graphRequestDone = false;
        Thread thread = new Thread() {
            public void run() {
                try {
                    GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(), graphPath);
                    GraphResponse response = request.executeAndWait();
                    graphResponse = response.getRawResponse();
                    graphResponseObject = response.getJSONObject();
                    graphRequestDone = true;
                    if (graphResponse != null) {
                        Log.d(TAG, "FacebookSDKService.graphRequest: response = " + graphResponse);
                    } else if (response.getError() != null) {
                        FacebookRequestError error = response.getError();
                        FacebookSDK.setLastError(error.getErrorMessage());
                        Log.d(TAG, String.format("FacebookSDKService.graphRequest: error = %s, code = %d", error.getErrorMessage(), error.getErrorCode()));
                        if (error.getErrorCode() == -1) sendedIdFriend = "error_connection";
                    }
                } catch (Exception e) {
                    Log.d(TAG, "FacebookSDKService.graphRequest: exception = " + e.getLocalizedMessage());
                    sendedIdFriend = "";
                }
            }
        };
        thread.start();
    }

    public boolean isGraphRequestDone() {
        return graphRequestDone;
    }

    public String getGraphResponse() {
        return graphResponse;
    }

    public JSONObject getGraphResponseObject() {
        return graphResponseObject;
    }

    public String getSendedIdFriend() {
        return sendedIdFriend;
    }

    public void setSendedIdFriend(String id) {
        sendedIdFriend = id;
    }
}
