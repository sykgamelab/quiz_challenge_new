package games.urmobi.socialplugin;

import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.games.AchievementsClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.unity3d.player.UnityPlayerActivity;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

public class SocialManagerActivity extends UnityPlayerActivity  {
    private final String TAG = "SocialManagerActivity";
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_LEADERBOARD_UI = 9002;
    private static final int RC_ACHIEVEMENT_UI = 9003;
    private GoogleSignInOptions signInOptions;
    private GoogleSignInAccount signInAccount;
    private GoogleSignInClient signInClient;
    private Intent signInIntent;
    private boolean isAuthenticationDone = false;
    private boolean isLogoutDone = false;
    private String lastError = null;
    private boolean isPlayerScoreLoaded = false;
    private long playerScore = 0;
    private String playerUID = null;
    private boolean isScoreSubmited = false;
    private boolean isAchievementsLoaded = false;
    private boolean isAchievementProgressReported = false;
    private ArrayList<AchievementForUnity> achievements = null;

    private class AchievementForUnity {
        private String id;
        private int currentSteps;
        private int totalSteps;
        private boolean completed;
        private long lastUpdatedTimestamp;

        public AchievementForUnity(String id, int currentSteps, int totalSteps, boolean completed, long lastUpdatedTimestamp) {
            this.id = id;
            this.currentSteps = currentSteps;
            this.totalSteps = totalSteps;
            this.completed = completed;
            this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        }

        public String getId() {
            return id;
        }

        public int getCurrentSteps() {
            return currentSteps;
        }

        public int getTotalSteps() {
            return totalSteps;
        }

        public boolean isCompleted() {
            return completed;
        }

        public long getLastUpdatedTimestamp() {
            return lastUpdatedTimestamp;
        }
    }

    public String getPlayerUID() {
        return playerUID;
    }

    private boolean isScoreSubmited() {
        return isScoreSubmited;
    }

    public long getPlayerScore() {
        return playerScore;
    }

    public boolean isPlayerScoreLoaded() {
        return  isPlayerScoreLoaded;
    }

    public boolean isAuthenticated() {
        return signInAccount != null;
    }

    public boolean isAuthenticationDone() {
        return isAuthenticationDone;
    }

    public String getLastError() {
        return lastError;
    }

    public boolean isAchievementsLoaded() {
        return isAchievementsLoaded;
    }

    public boolean isAchievementProgressReported() {
        return isAchievementProgressReported;
    }

    public void authenticate() {
        isAuthenticationDone = false;
        lastError = null;
        silentSignIn();
    }

    private void silentSignIn() {
        Log.d(TAG, "START silentSignIn in Android");
        if (signInClient != null) {
            signInClient.silentSignIn()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        signInAccount = task.getResult();
                        showGamesPopup();
                        Log.d(TAG, "SILENT AUTH DONE");
                        playerUID = signInAccount.getId();
                        isAuthenticationDone = true;
                    } else {
                        Exception e = task.getException();
                        if (!e.getLocalizedMessage().contains("4:")) {
                            Log.d(TAG, "FAILED SILENT AUTH: " + e.getLocalizedMessage());
                        }

                        startActivityForResult(signInIntent, RC_SIGN_IN);
                    }
                });
        } else {
            Log.d(TAG, "SILENT SIGN IN CLIENT IS NULL");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "START onActivityResult: requestCode = " + requestCode);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignIn.getSignedInAccountFromIntent(data)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onActivityResult.onComplete: SUCCESS");
                        signInAccount = task.getResult();
                        playerUID = signInAccount.getId();
                        showGamesPopup();
                        Log.d(TAG, "AUTH DONE");
                    } else {
                        Exception e = task.getException();
                        Log.d(TAG, "FAILED AUTH: " + e.getLocalizedMessage());
                        lastError = e.getLocalizedMessage();
                    }
                    isAuthenticationDone = true;
                });
        }
    }

    public boolean isLogoutDone() {
        return isLogoutDone;
    }

    public void logout() {
        isLogoutDone = false;
        lastError = null;
        signInClient
            .signOut()
            .addOnSuccessListener(success -> {
                isLogoutDone = true;
            })
            .addOnFailureListener(error -> {
                lastError = error.getLocalizedMessage();
                isLogoutDone = true;
            });
    }

    private void showGamesPopup() {
        if (signInAccount != null) {
            GamesClient client = Games.getGamesClient(SocialManagerActivity.this, signInAccount);
            client.setGravityForPopups(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
            client.setViewForPopups(getWindow().getDecorView().findViewById(android.R.id.content));
        }
    }

    public void showLeaderboard(String leaderboardID) {
        Log.d(TAG, "START showLeaderboard in Android");
        lastError = null;
        if (signInAccount == null) return;

        Games.getLeaderboardsClient(getApplicationContext(), signInAccount)
            .getAllLeaderboardsIntent()
            .addOnSuccessListener(intent -> startActivityForResult(intent, RC_LEADERBOARD_UI))
            .addOnFailureListener(error -> {
                lastError = error.getLocalizedMessage();
                Log.d(TAG, "showLeaderboard: error = " + lastError);
            });
    }

    private void loadPlayerCurrentScore(String leaderboardID) {
        Log.d(TAG, "START loadPlayerCurrentScore in Android");

        isPlayerScoreLoaded = false;
        lastError = null;
        if (signInAccount == null) {
            isPlayerScoreLoaded = true;
            return;
        }

        Games.getLeaderboardsClient(getApplicationContext(), signInAccount)
            .loadCurrentPlayerLeaderboardScore(leaderboardID,
                LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC)
                .addOnSuccessListener(leaderboardScoreAnnotatedData -> {
                    LeaderboardScore score = leaderboardScoreAnnotatedData.get();
                    String message = "rank = " + score.getRank() + "score = " + score.getRawScore();
                    playerScore = score.getRawScore();
                    Log.d(TAG, "loadScore.onSuccess: "+ message);
                    isPlayerScoreLoaded = true;
                })
                .addOnFailureListener(error -> {
                    lastError = error.getLocalizedMessage();
                    Log.d(TAG, "loadScore.onFailure: error = "+ lastError);
                    isPlayerScoreLoaded = true;
                });
    }

    private void submitScore(long score, String leaderboardID) {
        Log.d(TAG, "START submitScore in Android");
        lastError = null;
        isScoreSubmited = false;

        if (signInAccount == null) {
            isPlayerScoreLoaded = true;
            return;
        }

        Games.getLeaderboardsClient(getApplicationContext(), signInAccount)
            .submitScoreImmediate(leaderboardID, score)
                .addOnSuccessListener(scoreSubmissionData -> {
                    Log.d(TAG, "submitScore: SUCCESS");
                    isScoreSubmited = true;
                })
                .addOnFailureListener(error -> {
                    lastError = error.getLocalizedMessage();
                    Log.d(TAG, "submitScore: error = " + lastError);
                    isScoreSubmited = true;
                });
    }

    public String generateIdentityVerificationSignature() {
        String ivs = null;
        if (signInAccount != null)
            ivs =  signInAccount.getServerAuthCode();

        Log.d(TAG, "IVS = " + ((ivs == null) ? "null" : ivs));

        return ivs;
    }

    public void showAchievements() {
        Log.d(TAG, "START showAchievements in Android");
        lastError = null;
        if (signInAccount == null) return;

        Games.getAchievementsClient(this, signInAccount)
            .getAchievementsIntent()
            .addOnSuccessListener(intent -> startActivityForResult(intent, RC_ACHIEVEMENT_UI))
            .addOnFailureListener(e -> {
                lastError = e.getLocalizedMessage();
                Log.d(TAG, "showAchievements: error = " + lastError);
            });
    }

    public void loadAchievements() {
        Log.d(TAG, "START loadAchievements in Android");
        isAchievementsLoaded = false;
        lastError = null;
        if (signInAccount == null) {
            isAchievementsLoaded = true;
            return;
        }

        if (achievements == null) achievements = new ArrayList<>();
        else achievements.clear();

        Games.getAchievementsClient(this, signInAccount)
            .load(true)
            .addOnSuccessListener(achievementBufferAnnotatedData -> {
                AchievementBuffer buffer = achievementBufferAnnotatedData.get();
                Iterator<Achievement> iterator = buffer.iterator();
                Achievement achievement;

                while (iterator.hasNext()) {
                    achievement = iterator.next();
                    boolean completed = false;
                    if (achievement.getState() == Achievement.STATE_UNLOCKED) completed = true;
                    AchievementForUnity achievementForUnity = new AchievementForUnity(
                        achievement.getAchievementId(), achievement.getCurrentSteps(), achievement.getTotalSteps(), completed, achievement.getLastUpdatedTimestamp()
                    );
                    achievements.add(achievementForUnity);
                }

                isAchievementsLoaded = true;
            })
            .addOnFailureListener(error -> {
                isAchievementsLoaded = true;
                lastError = error.getLocalizedMessage();
                Log.d(TAG, "loadAchievements: error = " + lastError);
            });
    }

    public ArrayList<AchievementForUnity> getAchievements() {
        return achievements;
    }

    private void unlockAchievement(AchievementsClient achievementsClient, String achievementID) {
        achievementsClient
            .unlockImmediate(achievementID)
            .addOnSuccessListener(success -> {
                isAchievementProgressReported = true;
                    Log.d(TAG, String.format("achievement with id = %s successfully unlocked", achievementID));
                }
            )
            .addOnFailureListener(error -> {
                isAchievementProgressReported = true;
                lastError = error.getLocalizedMessage();
                Log.d(TAG, String.format("failed to unlock achievement with id = %s, error = %s", achievementID, lastError));
            });
    }

    private void incrementAchievement(AchievementsClient achievementsClient, String achievementID, double progress) {
        loadAchievements();
        Log.d(TAG, "incrementAchievement: wait achievements loading");
        while (!isAchievementsLoaded) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, "incrementAchievement: achievements loaded");

        AchievementForUnity achievement = null;
        for (AchievementForUnity achievementForUnity : achievements) {
            if (achievementID.equals(achievementForUnity.id)) {
                achievement = achievementForUnity;
                break;
            }
        }

        if (achievement != null) {
            int currSteps = achievement.getCurrentSteps();
            int totalSteps = achievement.getTotalSteps();
            int needSteps = (int) (progress * totalSteps + 0.5);
            int incr = needSteps - currSteps;
            Log.d(TAG, String.format("REPORT ACHIEVEMENT curr:%d total:%d need:%d incr:%d progress:%f", currSteps, totalSteps, needSteps,
                    incr, progress));

            if (incr <= 0) {
                Log.d(TAG, "REPORT ACHIEVEMENT nothing to report");
                isAchievementProgressReported = true;
                return;
            }
            achievementsClient
                    .incrementImmediate(achievementID, incr)
                    .addOnSuccessListener(success -> {
                                isAchievementProgressReported = true;
                                Log.d(TAG, String.format("achievement with id = %s successfully incremented on %d steps", achievementID, incr));
                            }
                    )
                    .addOnFailureListener(error -> {
                        isAchievementProgressReported = true;
                        lastError = error.getLocalizedMessage();
                        Log.d(TAG, String.format("failed to increment achievement with id = %s, error = %s", achievementID, lastError));
                    });
        }
        else {
            lastError = "Achievement not found";
            isAchievementProgressReported = true;
        }
    }

    public void reportAchievementProgress(int type, String id, double progress) {
        Log.d(TAG, "START reportAchievementProgress in Android");
        lastError = null;
        isAchievementProgressReported = false;

        if (signInAccount == null) {
            isAchievementProgressReported = true;
            return;
        }

        GamesClient gamesClient = Games.getGamesClient(this, signInAccount);
        gamesClient.setGravityForPopups(Gravity.TOP);
        gamesClient.setViewForPopups(findViewById(android.R.id.content));

        AchievementsClient client = Games.getAchievementsClient(this, signInAccount);

        try {
            if (type == 0) {
                unlockAchievement(client, id);
            } else if (type == 1) {
                incrementAchievement(client, id, progress);
            }
        } catch (Exception e) {
            Log.d(TAG, "reportAchievementProgress: error = " + e.getLocalizedMessage());
            e.printStackTrace();
            lastError = e.getLocalizedMessage();
            isAchievementProgressReported = true;
        }
    }

    public void resetAchievements() {
        /*if (signInAccount == null) return;
        try {
            String rawData = "access_token=" + getString(R.string.server_client_id);
            String type = "application/x-www-form-urlencoded";
            String encodedData = URLEncoder.encode(rawData, "UTF-8");
            URL url = new URL("https://www.googleapis.com/games/v1management/achievements/reset");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", type);
            conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
            OutputStream os = conn.getOutputStream();
            os.write(encodedData.getBytes());
            int statusCode = connection.getResponseCode();
            Log.d(TAG, String.format("resetAchievements: rawData = %s, statusCode = %d", rawData, statusCode));
            if (statusCode == 200) Log.d(TAG, "resetAchievements SUCCESS");
        } catch (IOException e) {
            Log.d(TAG, "resetAchievements FAIL: " + e.getLocalizedMessage());
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();

        signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
            .requestScopes(new Scope(Scopes.GAMES_LITE))
            .requestId()
            .requestProfile()
            .requestEmail()
            .requestServerAuthCode(getString(R.string.server_client_id), false)
            .requestIdToken(getString(R.string.server_client_id))
            .build();
        signInClient = GoogleSignIn.getClient(this, signInOptions);
        signInIntent = signInClient.getSignInIntent();
        signInAccount = GoogleSignIn.getLastSignedInAccount(this);
    }
}