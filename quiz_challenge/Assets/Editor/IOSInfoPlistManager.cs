﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public class IOSInfoPlistManager : MonoBehaviour
{
    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string plistPath = Path.Combine(path, "Info.plist");
            PlistDocument plist = new PlistDocument();
            plist.ReadFromFile(plistPath);

            PlistElementDict rootDict = plist.root;

            var arrCFBundleURLTypes = rootDict.CreateArray("CFBundleURLTypes");
            var dictCFBundleURLTypes = arrCFBundleURLTypes.AddDict();
            var arrCFBundleURLSchemes = dictCFBundleURLTypes.CreateArray("CFBundleURLSchemes");
            arrCFBundleURLSchemes.AddString("fb787468188253657");

            rootDict.SetString("FacebookAppID", "787468188253657");
            rootDict.SetString("FacebookDisplayName", "Quiz Challenge");

            var arrLSApplicationQueriesSchemes = rootDict.CreateArray("LSApplicationQueriesSchemes");
            arrLSApplicationQueriesSchemes.AddString("fbapi");
            arrLSApplicationQueriesSchemes.AddString("fb-messenger-share-api");
            arrLSApplicationQueriesSchemes.AddString("fbauth2");
            arrLSApplicationQueriesSchemes.AddString("fbshareextension");

            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}