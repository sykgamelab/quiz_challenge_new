﻿using UnityEngine.UI;

public class RefinementAnswer : Form
{
	private Text refinementAnswerText, refinementAnswerYesText, refinementAnswerNoText;
	private Game game;
    private Subscription<MessangerEvents> subscription;

    public static RefinementAnswer I { get; set; }

	private void OnRefinementAnswerYesClick()
	{
		gameObject.SetActive(false);
		game.CheckAnswer();
	}

	private void OnRefinementAnswerNoClick() => gameObject.SetActive(false);
	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);

    private void Awake()
    {
        I = this;

        subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subscription.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Start()
	{
		refinementAnswerText = objects["refinementAnswerText"].GetComponent<Text>();
		refinementAnswerYesText = objects["refinementAnswerYesText"].GetComponent<Text>();
		refinementAnswerNoText = objects["refinementAnswerNoText"].GetComponent<Text>();

		Started = true;

        game = Game.I;
    }

	private void OnEnable()
	{
		if (!Started) return;

		refinementAnswerText.text = LocalizationManager.GetString("passingLevel", "SureAnswer");
		refinementAnswerYesText.text = LocalizationManager.GetString("passingLevel", "Yes");
		refinementAnswerNoText.text = LocalizationManager.GetString("passingLevel", "No");
	}
}
