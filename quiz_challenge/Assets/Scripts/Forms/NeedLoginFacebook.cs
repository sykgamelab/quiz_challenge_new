﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class NeedLoginFacebook : Form
{
    private GameObject friendAnswer, needLoginFacebookButtonClose;
    private Text needLoginFacebookText, needLoginFacebookButtonText;
    private Subscription<EventMessages>[] subscriptionsEventMessages;
    private Subscription<MessangerEvents> subscriptionMessangerEvents;
    private Game game;
    private Button buttonOK;

    public static NeedLoginFacebook I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnCloseNeedLoginFacebookClick() => gameObject.SetActive(false);

    private void OnConnectNeedLoginFacebookClick()
    {
        Debug.Log("RUN OnConnectNeedLoginFacebookClick");

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            gameObject.SetActive(false);
            Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
        }
        else
        {
            FacebookSDK instance = FacebookSDK.GetInstance();
            if (instance.IsLoggedIn)
            {
                GraphRequest();
            }
            else
            {
                instance.Login(
                    (r) =>
                    {
                        if (r.AuthenticationSuccess)
                        {
                            GraphRequest();
                        }
                    }, "Ошибка");
            }
        }
    }

    private void GraphRequest()
    {
        Debug.Log("NeedLoginFacebook: RUN GraphRequest");
        Helper.FixButton(buttonOK);
        Helper.FixButton(needLoginFacebookButtonClose.GetComponent<Button>());
        game.FixInterface();
        Action callback = () =>
        {
            GraphResponse();
        };
        FacebookSDK.GetInstance().GraphRequest("/me/friends", callback);
    }

    private void Awake()
    {
        subscriptionsEventMessages = new Subscription<EventMessages>[]
        {
            Messanger<EventMessages>.Subscribe(EventMessages.FacebookSuccessGame, OnFacebookSuccessGame),
            Messanger<EventMessages>.Subscribe(EventMessages.UnlockGame, OnUnlockGame),
            Messanger<EventMessages>.Subscribe(EventMessages.HideFormNeedLoginFacebook, OnHideFormNeedLoginFacebook)
        };
        subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);

        I = this;
    }

    private void OnDestroy()
    {
        foreach (var s in subscriptionsEventMessages)
        {
            s.Unsubscribe();
        }
        subscriptionMessangerEvents.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void OnUnlockGame()
    {
        Helper.UnfixButton(buttonOK);
        Helper.UnfixButton(needLoginFacebookButtonClose.GetComponent<Button>());
        game.UnfixInterface();
    }

    private void OnFacebookSuccessGame()
	{
        Helper.UnfixButton(buttonOK);
        Helper.UnfixButton(needLoginFacebookButtonClose.GetComponent<Button>());
        game.UnfixInterface();
		gameObject.SetActive(false);
		friendAnswer.SetActive(true);
	}

    private void OnHideFormNeedLoginFacebook() => gameObject.SetActive(false);


	private void Start()
	{
        game = Game.I;

        buttonOK = objects["needLoginFacebookButtonOK"].GetComponent<Button>();
        needLoginFacebookText = objects["needLoginFacebookText"].GetComponent<Text>();
		needLoginFacebookButtonText = objects["needLoginFacebookButtonText"].GetComponent<Text>();
		needLoginFacebookButtonClose = objects["needLoginFacebookButtonClose"];

		needLoginFacebookText.text = LocalizationManager.GetString("hints", "UseFriendAssistance");
		needLoginFacebookButtonText.text = LocalizationManager.GetString("settings", "Login");

		friendAnswer = FriendAnswer.I.gameObject;

		Started = true;
	}

	private void OnEnable()
	{
		if (!Started) return;

		if (FacebookSDK.GetInstance().IsLoggedIn) needLoginFacebookButtonText.text = LocalizationManager.GetString("hints", "Ok");
		else needLoginFacebookButtonText.text = LocalizationManager.GetString("settings", "Login");
		needLoginFacebookText.text = LocalizationManager.GetString("additional", "AskFriend");
		//needLoginFacebookText.text = "Выберите друга, который подскажет вам ответ.";
	}

    private void GraphResponse()
    {
        Debug.Log("NeedLoginFacebook: RUN GraphResponse");
        FacebookSDK instance = FacebookSDK.GetInstance();
        string json = instance.GraphResponse();
        Debug.Log($"NeedLoginFacebook.GraphResponse: json = {json}");
        var dict = Json.Deserialize(json) as Dictionary<string, object>;
        if (dict.ContainsKey("data"))
        {
            var data = (List<object>)dict["data"];
            string idsString = Helper.GetFBFriendIds();
            Debug.Log($"NeedLoginFacebook.GraphResponse: BEFORE idsString = {idsString}");
            string[] ids = idsString.Split(';');
            string[] timestamps = Helper.GetFBFriendTimestamps().Split(';');
            StringBuilder idsForActivity = new StringBuilder();
            ArrayList idsForSave = new ArrayList();
            ArrayList timestampsForSave = new ArrayList();

            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    var obj = (Dictionary<string, object>)data[i];
                    string id = (string)obj["id"];
                    if (!idsString.Contains(id))
                    {
                        idsForActivity.Append(id + ";");
                    }
                    else
                    {
                        for (int j = 0; j < ids.Length; j++)
                        {
                            if (id == ids[j])
                            {
                                long timestamp = long.Parse(timestamps[j]);
                                long currentTimestamp = Helper.GetTimestamp();
                                if (currentTimestamp - timestamp > GameConstants.SecondsRestoreFBFriends)
                                {
                                    idsForActivity.Append(ids[j] + ";");
                                }
                                else
                                {
                                    idsForSave.Add(ids[j]);
                                    timestampsForSave.Add(timestamps[j]);
                                }
                            }
                        }
                    }
                }
                if (idsString != "")
                {
                    Helper.SetFBFriendIds(string.Join(";", idsForSave.ToArray()));
                    Helper.SetFBFriendTimestamps(string.Join(";", timestampsForSave.ToArray()));
                }

                idsString = idsForActivity.ToString();
            }
            else idsString = "0";
            Debug.Log($"NeedLoginFacebook.GraphResponse: AFTER idsString = {idsString}");
            
            string alertTitleError = LocalizationManager.GetString("additional", "FacebookError");
			string alertTitleInfo = LocalizationManager.GetString("additional", "FacebookAlert");
            string alertMessageNoFriends = LocalizationManager.GetString("additional", "FacebookNoFriends"); 
            string alertMessageNoFriendsNow = LocalizationManager.GetString("additional", "FacebookFriendsUsed");
            string alertMessageConfirmSend = LocalizationManager.GetString("additional", "FacebookFriendRefinement");
            string alertMessageConfirmClose = LocalizationManager.GetString("additional", "FacebookCancelRefinement");
            string appRequestMessage = LocalizationManager.GetString("additional", "FacebookAppmessage"); 
            string alertButtonYesText = LocalizationManager.GetString("passingLevel", "Yes");
            string alertButtonNoText = LocalizationManager.GetString("passingLevel", "No");
            string actionBarTitle = LocalizationManager.GetString("additional", "FacebookFriendlist"); 
            string buttonSendText = LocalizationManager.GetString("additional", "FacebookSend"); ;
            instance.ShowFriendList(idsString, alertTitleError, alertTitleInfo, alertMessageNoFriends, alertMessageNoFriendsNow, 
                alertMessageConfirmSend, alertMessageConfirmClose, appRequestMessage, alertButtonYesText, alertButtonNoText,
                actionBarTitle, buttonSendText);
        }
        else
        {
            Debug.Log("NeedLoginFacebook.GraphResponse: нет ключа с именем data");
            Helper.UnfixButton(buttonOK);
            Helper.UnfixButton(needLoginFacebookButtonClose.GetComponent<Button>());
            game.UnfixInterface();
        }
    }
}
