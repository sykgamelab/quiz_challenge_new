﻿using UnityEngine;
using UnityEngine.UI;

public class Congratulation : Form
{
	private GameObject categories;
	private MoveAnimation coin;
	private Game game;
	private Text congratulationHeader, congratulationText, congratulationNextText;
	private Button next;
	private Vector3 coinStartPos;
    private GameObject particleSystemObj;
    private Subscription<MessangerEvents> subcription;

    public static Congratulation I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
	private void OnCongratulationNextClick()
	{
		gameObject.SetActive(false);
		game.gameObject.SetActive(false);
		categories.SetActive(true);
		Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, true);
		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayMusic, SoundClips.Lobby);
	}

	public void ShowCategoryCongratulation()
	{
		gameObject.SetActive(true);

		Vector3 targetPos = game.CoinAnchor.position;
		coin.Target = targetPos;
		coin.T = GameConstants.CoinMoveTime;
		coin.gameObject.SetActive(true);
		coinStartPos = coin.transform.position;
		Helper.FixButton(next);
		coin.OnComplete = () =>
		{
			coin.transform.position = coinStartPos;
			int balance = Helper.GetBalance();
			balance += GameConstants.CategoryPartReward;
			Helper.SetBalance(balance);
			Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
			SocialManager.I.SetProgressToReport();
			coin.gameObject.SetActive(false);
			Helper.UnfixButton(next);
		};
		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayAnswerEffect, SoundClips.CollectCoins);
		coin.DoMove();
	}

    private void Awake()
    {
        I = this;

        subcription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subcription.Unsubscribe();
    }

    private void OnUpdateAnchors()
    {
        RectTransform flyingCoinRectTransform = objects["flyingCoin"].GetComponent<RectTransform>();
		flyingCoinRectTransform.pivot = new Vector2(0.5f, 0.5f);
        particleSystemObj.transform.localPosition = new Vector3(0f, 0f, 0f);
	
        gameObject.SetActive(false);
    }

    private void OnEnable()
	{
		if (!Started) return;

		congratulationHeader.text = LocalizationManager.GetString("passingLevel", "LevelCompleted");
		congratulationNextText.text = LocalizationManager.GetString("passingLevel", "Next");
		congratulationText.text = LocalizationManager.GetString("additional", "CategoryCongratulation");
	}

	private void Start()
	{
		categories = Categories.I.gameObject;
		game = Game.I;

		congratulationHeader = objects["congratulationHeader"].GetComponent<Text>();
		congratulationText = objects["congratulationText"].GetComponent<Text>();
		congratulationNextText = objects["congratulationNextText"].GetComponent<Text>();

		congratulationHeader.text = LocalizationManager.GetString("passingLevel", "LevelCompleted");
		/* нет локализации
        congratulationText.text = LocalizationManager.GetString("passingLevel", "LevelCompleted"); */
		congratulationNextText.text = LocalizationManager.GetString("passingLevel", "Next");

		next = objects["next"].GetComponent<Button>();
		Helper.FixButton(next);

        particleSystemObj = new GameObject();
        particleSystemObj.name = "CongratulationParticleSystem";
		particleSystemObj.transform.SetParent(objects["flyingCoin"].transform);

        ParticleSystem particleSystem = particleSystemObj.AddComponent<ParticleSystem>();
		ParticleSystem.MainModule main = particleSystem.main;
		main.startLifetime = new ParticleSystem.MinMaxCurve(4f);
		main.startSpeed = new ParticleSystem.MinMaxCurve(0f);
		main.startColor = new Color(226f / 255f, 161f / 255f, 20f / 255f);
		main.startSize = new ParticleSystem.MinMaxCurve(0.2f);
		main.gravityModifier = new ParticleSystem.MinMaxCurve(1f);
		main.simulationSpace = ParticleSystemSimulationSpace.World;
		main.maxParticles = 10000;

		ParticleSystem.EmissionModule emission = particleSystem.emission;
		emission.rateOverTime = new ParticleSystem.MinMaxCurve(100f);

		ParticleSystem.ShapeModule shape = particleSystem.shape;
		shape.shapeType = ParticleSystemShapeType.Circle;
		shape.radius = 0.2236807f;

		ParticleSystemRenderer renderer = particleSystemObj.GetComponent<ParticleSystemRenderer>();
		renderer.sortingLayerName = "Congratulation";
		renderer.sortingOrder = 3;
		renderer.material = Resources.Load<Material>("Materials/ParticleMaterial");
		coin = objects["flyingCoin"].GetComponent<MoveAnimation>();

		Started = true;
	}
}
