﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Game : Form
{
    private GameObject categories, bank, refinementAnswer, goToBankPanel, needLoginFacebook;
    private Congratulation congratulation;
    private RefinementHint refinementHint;
    private Text balanceText, categoryName, question, pgText;
    private Button bankButton, backButton;
    private Text answerAText, answerBText, answerCText, answerDText;
    private Image fakeAnswerA, fakeAnswerB, fakeAnswerC, fakeAnswerD;
    private Text fakeAnswerAText, fakeAnswerBText, fakeAnswerCText, fakeAnswerDText;
    private Text answerALetterText;
	private Text priceHintFiftyFifty, priceHintHallAssistance;
	private Image categoryImage;
    private CustomFillSlider progressBar;
    private Dictionary<string, Button> answerButtons;
    private Dictionary<string, Button> hintButtons;
    private Dictionary<string, Image> answerImages;
    private Dictionary<string, Text> answerTexts;
    private Dictionary<string, AlphaAnimation> answerAlphas;
    private Sprite unactiveState, activeState, incorrectState, correctState;
    private string btnName;
    private bool isCorrect;
    private GameObject answerObj;
    private int maxBlinkCount;
    private int blinkCount = 0;
    private TextAlphaAnimation fineAlpha;
    private MoveAnimation fineMove;
    private Vector3 fineStartPos, coinStartPos;
    private Transform fineAnchor;
    private MoveAnimation coin;
    private string[] abc = new string[] { "A", "B", "C", "D" };
    private Dictionary<string, Text> fakeAnswersText;
    private Dictionary<string, GameObject> fakeAnswersButton;
    private int minFontSize = 0;
    private bool resetMinFontSize = true;
    private Subscription<EventMessages> subscriptionEventMessages;
    private Subscription<MessangerEvents> subscriptionMessangerEvents;
	private IYandexAppMetrica appmetrica;
	Coroutine timerCoroutine;

    private void OnUpdateBalance() => balanceText.text = Helper.GetBalance().ToString();
    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnGoToBankClick() => bank.SetActive(true);

	private void OnGoToNeedLoginFacebookClick()
	{
		FriendAnswer.I.Part = Part;
		needLoginFacebook.SetActive(true);
	}

    public Question[] Questions { get; set; } = null;
    public string Category { get; set; } = "";
    public Sprite CategorySprite { get; set; } = null;
    public Transform CoinAnchor { get; private set; }
	public int Part { get; set; }
    public static Game I { get; set; }

    private void Awake()
    {
        subscriptionEventMessages = Messanger<EventMessages>.Subscribe(EventMessages.UpdateBalance, OnUpdateBalance);
        subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);

        I = this;
    }

    private void OnDestroy()
    {
        subscriptionEventMessages.Unsubscribe();
        subscriptionMessangerEvents.Unsubscribe();
    }

    private void OnUpdateAnchors()
    {
        objects["fineText"].SetActive(false);
        objects["flyingCoin"].SetActive(false);

        fakeAnswerA.gameObject.SetActive(false);
        fakeAnswerB.gameObject.SetActive(false);
        fakeAnswerC.gameObject.SetActive(false);
        fakeAnswerD.gameObject.SetActive(false);

        gameObject.SetActive(false);
    }

    private void LoadQuestion()
    {
        if (resetMinFontSize)
        {
            minFontSize = 0;
            foreach (var pair in answerTexts) pair.Value.resizeTextForBestFit = true;
            foreach (var pair in fakeAnswersText) pair.Value.resizeTextForBestFit = true;
        }
        
        Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, true);

        foreach (HintType hint in Enum.GetValues(typeof(HintType)))
        {
            if (Helper.IsHintUsed(Category, Part, hint))
            {
                hintButtons[hint.ToString()].gameObject.SetActive(false);
            }
            else
            {
                hintButtons[hint.ToString()].gameObject.SetActive(true);
            }
        }

        foreach (var pair in answerTexts)
        {
            pair.Value.color = new Color(1f, 1f, 1f);
            answerImages[pair.Key].sprite = unactiveState;
        }

        int currQuestion = Helper.GetCurrQuestion(Category, Part);

        float progress = (float)currQuestion / Questions.Length;
        progressBar.value = progress;

		//pgText.text = currQuestion != GameConstants.PerPartQuestions ? (currQuestion + 1).ToString() : currQuestion.ToString();
		pgText.text = currQuestion.ToString();

        question.text = Questions[currQuestion].Text;

        foreach (var letter in abc)
        {
            int nmb = Helper.GetAnswerNumber(Category, Part, letter);
            answerTexts[$"answer{letter}"].text = Questions[currQuestion].Answers[nmb];
            fakeAnswersText[letter].text = answerTexts[$"answer{letter}"].text;

            if (Helper.IsAnswerHidden(Category, Part, letter)) answerButtons[$"answer{letter}"].gameObject.SetActive(false);
            else answerButtons[$"answer{letter}"].gameObject.SetActive(true);
        }
    }

	private void HandleNextQuestion()
	{
		int currQuestion = Helper.GetCurrQuestion(Category, Part);

		if (isCorrect)
		{
			appmetrica.ReportEvent(MetricaConstants.CorrectAnswer,
				new Dictionary<string, object>()
				{
					{ MetricaConstants.Category, Category },
					{ MetricaConstants.Part, Part },
					{ MetricaConstants.Question, currQuestion + 1},
				});

			currQuestion++;
			Helper.SetCurrQuestion(Category, Part, currQuestion);
			UnfixInterface();
			if (currQuestion == Questions.Length)
			{
				pgText.text = (currQuestion + 1).ToString();
				progressBar.value = 1;
				congratulation.ShowCategoryCongratulation();

				appmetrica.ReportEvent(MetricaConstants.PartDone,
				new Dictionary<string, object>()
				{
					{ MetricaConstants.Category, Category },
					{ MetricaConstants.Part, Part },
					{ MetricaConstants.Time, (int)(Helper.GetCategoryPartTime(Category, Part) / 60f + 0.5f) }
				});
				StopCoroutine(timerCoroutine);
				Debug.Log("SERVER SET NEXT QUESTION PROGRESS");
				//	SocialManager.I.SetProgressToReport();
				return;
			}

			foreach (HintType hint in Enum.GetValues(typeof(HintType)))
			{
				Helper.SetHintUsed(Category, Part, hint, false);
			}

			for (int i = 0; i < abc.Length; i++)
			{
				Helper.SetAnswerHidden(Category, Part, abc[i], false);
				Helper.SetAnswerLetter(Category, Part, i, abc[i]);
				Helper.SetAnswerNumber(Category, Part, abc[i], i);
			}


			resetMinFontSize = true;
			LoadQuestion();

		}
		else if (!Helper.IsHintUsed(Category, Part, HintType.FiftyFifty))
		{
			int balance = Helper.GetBalance();
			balance -= GameConstants.WrongAnswerFine;
			if (balance < 0) balance = 0;
			Helper.SetBalance(balance);
			Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
			Dictionary<int, string> answersBefore = new Dictionary<int, string>();

			for (int i = 0; i < 4; i++)
			{
				answersBefore.Add(i, Helper.GetAnswerLetter(Category, Part, i));
			}


			int[] answersOld = new int[4];
			for (int i = 0; i < 4; i++)
			{
				answersOld[i] = Helper.GetAnswerNumber(Category, Part, abc[i]);
			}

			int[] answersNew = (int[])answersOld.Clone();

			bool same = true;
			while (same)
			{
				for (int i = 0; i < answersNew.Length; i++)
				{

					int idx = UnityEngine.Random.Range(0, answersNew.Length);
					int tmp = answersNew[i];
					answersNew[i] = answersNew[idx];
					answersNew[idx] = tmp;
				}

				for (int i = 0; i < answersOld.Length; i++)
				{
					if (answersOld[i] != answersNew[i])
					{
						same = false;
						break;
					}
				}
			}

			for (int i = 0; i < answersNew.Length; i++)
			{
				Helper.SetAnswerNumber(Category, Part, abc[i], answersNew[i]);

				int idx = 0;
				for (int j = 0; j < answersNew.Length; j++)
				{
					if (answersNew[j] == i)
					{
						idx = j;
						break;
					}
				}
				Helper.SetAnswerLetter(Category, Part, i, abc[idx]);
			}

			Dictionary<int, string> answersAfter = new Dictionary<int, string>();
			for (int i = 0; i < 4; i++)
			{
				answersAfter.Add(i, Helper.GetAnswerLetter(Category, Part, i));
			}

			Dictionary<string, string> answersChange = new Dictionary<string, string>();
			foreach (var pair in answersBefore)
			{
				string letterBefore = pair.Value;
				string letterAfter = answersAfter[pair.Key];
				answersChange.Add(letterBefore, letterAfter);
			}

			ChangeFontAnswers();
			resetMinFontSize = false;
			MoveFakeButtons(answersChange);
		}
		else
		{
			UnfixInterface();
			LoadQuestion();
		}

		Debug.Log("SERVER SET NEXT QUESTION2 PROGRESS");
		SocialManager.I.SetProgressToReport();
	}

    private void NextQuestion()
    {
		if (isCorrect)
		{
			int adsCount = Helper.GetAdsCount();
			if (adsCount % 5 == 0)
			{
				Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, false);
				if (adsCount % 3 == 0)
				{
					Debug.Log("SHOW VIDEO");
					AdsManager.ShowVideo(HandleNextQuestion);
				}
				else
				{
					Debug.Log("SHOW NO VIDEO");
					AdsManager.ShowNoVideo(HandleNextQuestion);
				}
			}
			else HandleNextQuestion();
		}
		else HandleNextQuestion();

      
    }

    public void FixInterface()
    {
        Helper.FixButton(backButton);
        Helper.FixButton(bankButton);
        foreach (var pair in answerButtons)
        {
            Helper.FixButton(pair.Value);
        }
        foreach (var pair in hintButtons)
        {
            Helper.FixButton(pair.Value);
        }
    }

    public void UnfixInterface()
    {
        Helper.UnfixButton(backButton);
        Helper.UnfixButton(bankButton);
        foreach (var pair in answerButtons)
        {
            Helper.UnfixButton(pair.Value);
        }
        foreach (var pair in hintButtons)
        {
            Helper.UnfixButton(pair.Value);
        }
    }

    private void MoveFakeButtons(Dictionary<string, string> answersChange)
    {
        GameObject answer, fakeAnswer;

        foreach (KeyValuePair<string, string> pair in answersChange)
        {
            answerButtons[$"answer{pair.Key}"].gameObject.SetActive(false);
        }

        foreach (KeyValuePair<string, string> pair in answersChange)
        {

            fakeAnswer = fakeAnswersButton[pair.Key];
            answer = answerButtons[$"answer{pair.Key}"].gameObject;

            fakeAnswer.GetComponent<RectTransform>().pivot = answer.GetComponent<RectTransform>().pivot;

            fakeAnswer.transform.position = answer.transform.position;
            fakeAnswer.SetActive(true);

            if (pair.Key != pair.Value)
            {
                MoveAnimation move = fakeAnswer.GetComponent<MoveAnimation>();
                move.Target = answerButtons[$"answer{pair.Value}"].gameObject.transform.position;
                move.T = GameConstants.FakeAnswerButtonMoveTime;
                answer = answerButtons[$"answer{pair.Value}"].gameObject;
                fakeAnswer.GetComponent<RectTransform>().pivot = answer.GetComponent<RectTransform>().pivot;
                move.DoMove();
            }
        }

        StartCoroutine(WaitMoveFakeButtons());
    }

    private IEnumerator WaitMoveFakeButtons()
    {
        yield return new WaitForSeconds(GameConstants.FakeAnswerButtonMoveTime);

        foreach (var pair in fakeAnswersButton) pair.Value.SetActive(false);

        LoadQuestion();
        UnfixInterface();
    }

    private IEnumerator WaitUnfixCoroutine()
    {
        yield return new WaitForSeconds(GameConstants.FixTime);
        if (isCorrect /*&& (Helper.GetCurrQuestion(Category, Part) + 1 != Questions.Length)*/)
        {
            coin.gameObject.SetActive(true);

            Vector3 targetPos = CoinAnchor.position;

            coin.Target = targetPos;
            coin.T = GameConstants.CoinMoveTime;
            coin.gameObject.SetActive(true);
            coinStartPos = answerObj.transform.position;

            RectTransform answerRectTransform = answerObj.GetComponent<RectTransform>();
            Vector2 answerPivot = answerRectTransform.pivot;
			//   float coinX = answerObj.transform.position.x + (answerRectTransform.rect.width * answerPivot.x + 2 * answerRectTransform.rect.width) / 1000f;
			//  float coinY = answerObj.transform.position.y - (answerRectTransform.rect.height * answerPivot.y + answerRectTransform.rect.height / 2f) / 1000f;


			//coinStartPos = new Vector3(coinX, coinY, answerObj.transform.position.z);
			coinStartPos = answerObj.transform.position;
			coin.gameObject.transform.position = coinStartPos;

			//coin.gameObject.transform.position = new Vector3(answerObj.transform.position.x, answerObj.transform.position.y, answerObj.transform.position.z);
			//coin.gameObject.transform.position = answerObj.transform.position;
			coin.OnComplete = () =>
            {
                coin.transform.position = coinStartPos;
                int balance = Helper.GetBalance();
                balance += GameConstants.CorrectAnswerReward;
                Helper.SetBalance(balance);
                Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
                coin.gameObject.SetActive(false);
            };
            Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayAnswerEffect, SoundClips.CollectCoins);
            coin.DoMove();

            yield return new WaitForSeconds(GameConstants.CoinMoveTime);
        }
        NextQuestion();
    }

    private IEnumerator FineWaitCoroutine()
    {
        yield return new WaitForSeconds(GameConstants.FixTime / 2f);
        fineAlpha.Target = 0.0f;
        fineAlpha.T = GameConstants.FixTime / 4f;
        fineAlpha.OnComplete = () =>
        {
            fineMove.gameObject.transform.position = fineStartPos;
            fineMove.gameObject.SetActive(false);
        };
        fineAlpha.DoAlpha();
    }

    private void ShowFineText()
    {
		int balance = Helper.GetBalance();
		balance -= GameConstants.WrongAnswerFine;
		if (balance < 0) balance = 0;
		Helper.SetBalance(balance);
		Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
		Dictionary<int, string> answersBefore = new Dictionary<int, string>();

		fineStartPos = fineMove.gameObject.transform.position;
        fineMove.gameObject.SetActive(true);

        fineMove.Target = fineAnchor.position;
        fineMove.T = GameConstants.FixTime / 4f;
        fineMove.DoMove();

        fineAlpha.Target = 1.0f;
        fineAlpha.T = GameConstants.FixTime / 4f;
        fineAlpha.OnComplete = () =>
        {
            StartCoroutine(FineWaitCoroutine());
        };
        fineAlpha.DoAlpha();
    }

    public void CheckAnswer()
    {
        FixInterface();

        Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayAnswerEffect, SoundClips.AnswerChoice);

		if (isCorrect)
		{
			Helper.SetScore(Helper.GetScore() + 1);
			//SocialManager.ReportScore();
			Helper.SetHaveScoreToReport(true);
		}

        AlphaAnimation alpha = answerAlphas[answerObj.name];
        alpha.Target = GameConstants.AlphaMin;
        alpha.T = GameConstants.AnswerCheckTime;
        alpha.OnComplete = () =>
        {
            alpha.Target = GameConstants.AlphaMax;
            alpha.OnComplete = () =>
            {
                answerTexts[answerObj.name].color = new Color(1.0f, 1.0f, 1.0f);
                if (isCorrect)
                {
                    answerImages[answerObj.name].sprite = correctState;
                    Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayAnswerEffect, SoundClips.Win);
                }
                else
                {
                    answerImages[answerObj.name].sprite = incorrectState;
                    Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayAnswerEffect, SoundClips.Lose);
                    ShowFineText();
                }

                alpha.Target = GameConstants.AlphaMin;
                alpha.T = isCorrect ? GameConstants.FixTime / 4f : GameConstants.FixTime / 8f;
                maxBlinkCount = isCorrect ? 2 : 4;

                Action toAlphaMax = null, toAlphaMin = null;
                blinkCount = 0;

                toAlphaMax = () =>
                {
                    alpha.Target = GameConstants.AlphaMax;
                    alpha.OnComplete = toAlphaMin;
                    alpha.DoAlpha();
                };

                toAlphaMin = () =>
                {
                    alpha.Target = GameConstants.AlphaMin;
                    alpha.OnComplete = toAlphaMax;
                    blinkCount++;
                    if (blinkCount < maxBlinkCount)
                    {
                        alpha.DoAlpha();
                    }

                };

                alpha.OnComplete = toAlphaMax;
                alpha.DoAlpha();

                StartCoroutine(WaitUnfixCoroutine());
            };
            alpha.DoAlpha();
        };
        alpha.DoAlpha();
    }

    public void UseFiftyFiftyHint()
    {
        int currQuestion = Helper.GetCurrQuestion(Category, Part);
        int correct = Questions[currQuestion].Correct;

        int answer1 = UnityEngine.Random.Range(0, 4);
        while (answer1 == correct)
        {
            answer1 = UnityEngine.Random.Range(0, 4);
        }

        int answer2 = UnityEngine.Random.Range(0, 4);
        while (answer2 == correct || answer2 == answer1)
        {
            answer2 = UnityEngine.Random.Range(0, 4);
        }

        string letter = Helper.GetAnswerLetter(Category, Part, answer1);
        answerButtons[$"answer{letter}"].gameObject.SetActive(false);
        Helper.SetAnswerHidden(Category, Part, letter, true);

        letter = Helper.GetAnswerLetter(Category, Part, answer2);
        answerButtons[$"answer{letter}"].gameObject.SetActive(false);
        Helper.SetAnswerHidden(Category, Part, letter, true);
    }

    public void FinishHint(HintType hint)
    {
		appmetrica.ReportEvent(MetricaConstants.HintUsed,
			new Dictionary<string, object>()
			{
				{ MetricaConstants.Category, Category },
				{ MetricaConstants.Part, Part },
				{ MetricaConstants.HintType, hint.ToString() }
			});
        hintButtons[hint.ToString()].gameObject.SetActive(false);
        Helper.SetHintUsed(Category, Part, hint, true);
        int balance = Helper.GetBalance();
        int price = (int)Enum.Parse(typeof(HintPrice), hint.ToString());
        balance -= price;
        Helper.SetBalance(balance);
        Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
		Debug.Log("SERVER SET FINISH HINT PROGRESS");
		SocialManager.I.SetProgressToReport();
    }

    private void OnHintButtonClick()
    {
        GameObject obj = EventSystem.current.currentSelectedGameObject;

        int price = (int)Enum.Parse(typeof(HintPrice), obj.name);
        int balance = Helper.GetBalance();
        if (balance < price)
        {
            goToBankPanel.SetActive(true);
            return;
        }

        HintType hint = (HintType)Enum.Parse(typeof(HintType), obj.name);
        refinementHint.ShowRefinement(hint, Part);
    }

    private void OnAnswerButtonClick()
    {
        GameObject obj = EventSystem.current.currentSelectedGameObject;
        answerObj = obj;

        foreach (var pair in answerImages)
        {
            if (pair.Key == answerObj.name)
            {
                pair.Value.sprite = activeState;
                answerTexts[pair.Key].color = new Color(48.0f / 255.0f, 48.0f / 255.0f, 48.0f / 255.0f);
            }
            else
            {
                pair.Value.sprite = unactiveState;
                answerTexts[pair.Key].color = new Color(1f, 1f, 1f);
            }
        }

        Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, false);

        int currQuestion = Helper.GetCurrQuestion(Category, Part);
        int answerNmb = Helper.GetAnswerNumber(Category, Part, answerObj.name.Substring(answerObj.name.Length - 1));
        isCorrect = Questions[currQuestion].Correct == answerNmb;
        btnName = obj.name;
        int val = UnityEngine.Random.Range(0, 100);
        if ((isCorrect && val < 15) || (!isCorrect && val < 15))
        {
            refinementAnswer.SetActive(true);
        }
        else
        {
            CheckAnswer();
        }
    }

	private IEnumerator GameSessionCoroutine()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			int time = Helper.GetCategoryPartTime(Category, Part) + 1;
			Helper.SetCategoryPartTime(Category, Part, time);
		}
	}

    private void OnEnable()
    {
        if (!Started) return;

		timerCoroutine = StartCoroutine(GameSessionCoroutine());
        balanceText.text = Helper.GetBalance().ToString();
        categoryName.text = LocalizationManager.GetString("categorySelection", Category).ToUpper();
        categoryImage.sprite = CategorySprite;
		objects["answerA"].GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		objects["answerC"].GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		objects["answerB"].GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		objects["answerD"].GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
		LoadQuestion();
    }

    private void OnGoToCategoriesClick()
    {
        gameObject.SetActive(false);
        categories.SetActive(true);
		StopCoroutine(timerCoroutine);
        Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, true);
        Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayMusic, SoundClips.Lobby);
    }

    private void InitAnswerButton(string name)
    {
        answerButtons.Add(name, objects[name].GetComponent<Button>());
		answerImages.Add(name, objects[name].GetComponent<Image>());
        answerAlphas.Add(name, objects[name].GetComponent<AlphaAnimation>());
        answerTexts.Add(name, objects[$"{name}Text"].GetComponent<Text>());
    }

    private void InitFakeAnswerButtons()
    {
        fakeAnswerA = objects["fakeAnswerA"].GetComponent<Image>();
        fakeAnswerB = objects["fakeAnswerB"].GetComponent<Image>();
        fakeAnswerC = objects["fakeAnswerC"].GetComponent<Image>();
        fakeAnswerD = objects["fakeAnswerD"].GetComponent<Image>();
        fakeAnswerAText = objects["fakeAnswerAText"].GetComponent<Text>();
        fakeAnswerBText = objects["fakeAnswerBText"].GetComponent<Text>();
        fakeAnswerCText = objects["fakeAnswerCText"].GetComponent<Text>();
        fakeAnswerDText = objects["fakeAnswerDText"].GetComponent<Text>();

        fakeAnswersText = new Dictionary<string, Text>
        {
            { "A",  fakeAnswerAText },
            { "B",  fakeAnswerBText },
            { "C",  fakeAnswerCText },
            { "D",  fakeAnswerDText }
        };

        fakeAnswersButton = new Dictionary<string, GameObject>
        {
            { "A", fakeAnswerA.gameObject },
            { "B", fakeAnswerB.gameObject },
            { "C", fakeAnswerC.gameObject },
            { "D", fakeAnswerD.gameObject }
        };
    }

    private void Start()
    {
		appmetrica = AppMetrica.Instance;

        bank = Bank.I.gameObject;
        categories = Categories.I.gameObject;
        refinementAnswer = RefinementAnswer.I.gameObject;
        refinementHint = RefinementHint.I;
        goToBankPanel = GoToBank.I.gameObject;
        congratulation = Congratulation.I;
        needLoginFacebook = NeedLoginFacebook.I.gameObject;

        categoryName = objects["categoryName"].GetComponent<Text>();
        categoryImage = objects["categoryImage"].GetComponent<Image>();
        question = objects["question"].GetComponent<Text>();
        backButton = objects["goToCategories"].GetComponent<Button>();
        bankButton = objects["goToBank"].GetComponent<Button>();
        balanceText = objects["balance"].GetComponent<Text>();
		priceHintFiftyFifty = objects["PriceHintFiftyFifty"].GetComponent<Text>();
		priceHintHallAssistance = objects["PriceHintHallAssistance"].GetComponent<Text>();
		priceHintFiftyFifty.text = ((int)HintPrice.FiftyFifty).ToString();
		priceHintHallAssistance.text = ((int)HintPrice.HallAssistance).ToString();


		fineAlpha = objects["fineText"].GetComponent<TextAlphaAnimation>();
        fineMove = objects["fineText"].GetComponent<MoveAnimation>();

        fineAnchor = objects["fineAnchor"].transform;
        fineAnchor.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
        CoinAnchor = objects["coinAnchor"].transform;
		CoinAnchor.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

        GameObject answerPrefab = Resources.Load<GameObject>("Prefabs/AnswerButton");
        answerTexts = new Dictionary<string, Text>();
        answerButtons = new Dictionary<string, Button>();
        answerAlphas = new Dictionary<string, AlphaAnimation>();
        answerImages = new Dictionary<string, Image>();

        InitAnswerButton("answerA");
        InitAnswerButton("answerB");
        InitAnswerButton("answerC");
        InitAnswerButton("answerD");
        InitFakeAnswerButtons();

        hintButtons = new Dictionary<string, Button>()
        {
            { "FiftyFifty" , objects["FiftyFifty"].GetComponent<Button>() },
            { "HallAssistance" , objects["HallAssistance"].GetComponent<Button>() },
            { "FriendAssistance" , objects["FriendAssistance"].GetComponent<Button>() }
        };

        unactiveState = URPNGLoader.LoadURPNG("sprite_2", 21);
        activeState = URPNGLoader.LoadURPNG("sprite_2", 22);
        correctState = URPNGLoader.LoadURPNG("sprite_2", 24);
        incorrectState = URPNGLoader.LoadURPNG("sprite_2", 25);

        GameObject progressBarHolder = objects["progressBarHolder"];
        GameObject progressBar = Instantiate(Resources.Load<GameObject>("Prefabs/QuestionsProgressBarPrefab"));
        progressBar.transform.SetParent(progressBarHolder.transform);
        progressBar.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);

        Transform pgTransform = progressBar.transform;
		Transform bgTransform = pgTransform.GetChild(0);
		bgTransform.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("grey-pb", 0);
        bgTransform.GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("green-pb", 0);
        Transform handleTransform = pgTransform.GetChild(1).GetChild(0);
        pgText = handleTransform.GetChild(0).GetComponent<Text>();
        handleTransform.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("sprite_2", 29);
        pgTransform.GetChild(2).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("finish", 0);

        this.progressBar = progressBar.GetComponent<CustomFillSlider>();

        GameObject particleSystemObj = new GameObject();
        particleSystemObj.name = "GameParticleSystem";
        particleSystemObj.transform.SetParent(objects["flyingCoin"].transform);
        RectTransform flyingCoinRectTransform = objects["flyingCoin"].GetComponent<RectTransform>();
		flyingCoinRectTransform.pivot = new Vector2(0.5f, 0.5f);
       // Vector2 flyingCoinPivot = flyingCoinRectTransform.pivot;
        //float particleSystemX = flyingCoinRectTransform.rect.width / 2f - flyingCoinPivot.x * flyingCoinRectTransform.rect.width;
        //float particleSystemY = -flyingCoinPivot.y * flyingCoinRectTransform.rect.height;
		float particleSystemX = 0f;
		float particleSystemY = 0f;
		particleSystemObj.transform.localPosition = new Vector3(particleSystemX, particleSystemY, 0f);
        ParticleSystem particleSystem = particleSystemObj.AddComponent<ParticleSystem>();

        ParticleSystem.MainModule main = particleSystem.main;
        main.startLifetime = new ParticleSystem.MinMaxCurve(4f);
        main.startSpeed = new ParticleSystem.MinMaxCurve(0f);
        main.startColor = new Color(226f / 255f, 161f / 255f, 20f / 255f);
        main.startSize = new ParticleSystem.MinMaxCurve(0.2f);
        main.gravityModifier = new ParticleSystem.MinMaxCurve(1f);
        main.simulationSpace = ParticleSystemSimulationSpace.World;
        main.maxParticles = 10000;

        ParticleSystem.EmissionModule emission = particleSystem.emission;
        emission.rateOverTime = new ParticleSystem.MinMaxCurve(100f);

        ParticleSystem.ShapeModule shape = particleSystem.shape;
        shape.shapeType = ParticleSystemShapeType.Circle;
        shape.radius = 0.2236807f;

        ParticleSystemRenderer renderer = particleSystemObj.GetComponent<ParticleSystemRenderer>();
        renderer.sortingLayerName = "Game";
        renderer.sortingOrder = 1;
        renderer.material = Resources.Load<Material>("Materials/ParticleMaterial");

        coin = objects["flyingCoin"].GetComponent<MoveAnimation>();
        answerALetterText = objects["answerALetterText"].GetComponent<Text>();

        Started = true;
    }

    private void ChangeFontAnswers()
    {
        int fontSize;

        foreach (var pair in answerTexts)
        {
            fontSize = pair.Value.cachedTextGenerator.fontSizeUsedForBestFit;
            if (fontSize > 0 && fontSize < minFontSize) minFontSize = fontSize;
        }

        foreach (var pair in answerTexts)
        {
            pair.Value.resizeTextForBestFit = false;
            pair.Value.fontSize = minFontSize;
        }

        foreach (var pair in fakeAnswersText)
        {
            pair.Value.resizeTextForBestFit = false;
            pair.Value.fontSize = minFontSize;
        }
    }

    private void Update()
    {
        if (answerALetterText != null && minFontSize == 0)
        {
            minFontSize = answerALetterText.cachedTextGenerator.fontSizeUsedForBestFit;
            if (minFontSize > 0) ChangeFontAnswers();
        }
    }
}
