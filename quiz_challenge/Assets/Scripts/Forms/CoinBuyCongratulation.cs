﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinBuyCongratulation : Form
{
	Subscription<int, EventMessages> subscription;
	Subscription<MessangerEvents> subscription2;
	Text coinCongratulationText, coinCongratulationOkText;


	private void Awake()
	{
		subscription = Messanger<int, EventMessages>.Subscribe(EventMessages.CoinsBought, OnCoinsBought);
		subscription2 = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
	}

	private void OnUpdateAnchors() => gameObject.SetActive(false);

	private void OnCoinsBought(int coins)
	{
		gameObject.SetActive(true);
		string text = LocalizationManager.GetString("bank", "CoinCongratulation").Replace("??", coins.ToString());
		coinCongratulationText.text = text;
	}

	private void OnCoinCongratulationClose() => gameObject.SetActive(false);

	private void OnDestroy()
	{
		subscription.Unsubscribe();
		subscription2.Unsubscribe();
	}
	// Start is called before the first frame update
	void Start()
    {
		coinCongratulationText = objects["coinCongratulationText"].GetComponent<Text>();
		coinCongratulationOkText = objects["coinCongratulationOkText"].GetComponent<Text>();
		Started = true;
    }

	private void OnEnable()
	{
		if (!Started) return;

		coinCongratulationOkText.text = LocalizationManager.GetString("hints", "Ok");
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
