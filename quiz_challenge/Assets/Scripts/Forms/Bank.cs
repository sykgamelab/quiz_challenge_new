﻿using UnityEngine;
using UnityEngine.UI;

public class Bank : Form
{
	private Text bankHeaderText;
	private Text pack250Text, pack690Text, pack1520Text, pack3500Text, pack7350Text;
	private GameObject bankButtonClose;
	private Button btnPurchase250, btnPurchase690, btnPurchase1520, btnPurchase3500, btnPurchase7350;
	private Text textPurchase250, textPurchase690, textPurchase1520, textPurchase3500, textPurchase7350;
	private Text textPurchase250NoInit, textPurchase690NoInit, textPurchase1520NoInit, textPurchase3500NoInit, textPurchase7350NoInit;
	private bool changedLanguage;
	private Subscription<EventMessages> subscriptionEventMessages;
	private Subscription<MessangerEvents> subscriptionMessangerEvents;
	private bool bankReady = false;

	public static Bank I { get; set; }

	private void UserAwake() { }

	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);

	private void FixButtons()
	{
		Helper.FixButton(btnPurchase250);
		Helper.FixButton(btnPurchase690);
		Helper.FixButton(btnPurchase1520);
		Helper.FixButton(btnPurchase3500);
		Helper.FixButton(btnPurchase7350);
		Helper.FixButton(bankButtonClose.GetComponent<Button>());
	}

	private void UnfixButtons()
	{
		Helper.UnfixButton(btnPurchase250);
		Helper.UnfixButton(btnPurchase690);
		Helper.UnfixButton(btnPurchase1520);
		Helper.UnfixButton(btnPurchase3500);
		Helper.UnfixButton(btnPurchase7350);
		Helper.UnfixButton(bankButtonClose.GetComponent<Button>());
	}

	private void OnPurchase250Click()
	{
		FixButtons();
		Messanger<string, EventMessages>.SendMessage(EventMessages.BuyProduct, "250coins");
	}

	private void OnPurchase690Click()
	{
		FixButtons();
		Messanger<string, EventMessages>.SendMessage(EventMessages.BuyProduct, "690coins");
	}

	private void OnPurchase1520Click()
	{
		FixButtons();
		Messanger<string, EventMessages>.SendMessage(EventMessages.BuyProduct, "1520coins");
	}

	private void OnPurchase3500Click()
	{
		FixButtons();
		Messanger<string, EventMessages>.SendMessage(EventMessages.BuyProduct, "3500coins");
	}

	private void OnPurchase7350Click()
	{
		FixButtons();
		Messanger<string, EventMessages>.SendMessage(EventMessages.BuyProduct, "7350coins");
	}

	private void OnCloseBankClick() => gameObject.SetActive(false);

	private void LoadLocalization()
	{
		bankHeaderText.text = LocalizationManager.GetString("bank", "Bank");
		pack250Text.text = LocalizationManager.GetString("bank", "pack1");
		pack690Text.text = LocalizationManager.GetString("bank", "pack2");
		pack1520Text.text = LocalizationManager.GetString("bank", "pack3");
		pack3500Text.text = LocalizationManager.GetString("bank", "pack4");
		pack7350Text.text = LocalizationManager.GetString("bank", "pack5");

		if (StoreListener.Prices != null)
		{
			textPurchase250.text = StoreListener.Prices["250coins"];
			textPurchase690.text = StoreListener.Prices["690coins"];
			textPurchase1520.text = StoreListener.Prices["1520coins"];
			textPurchase3500.text = StoreListener.Prices["3500coins"];
			textPurchase7350.text = StoreListener.Prices["7350coins"];
			textPurchase250.gameObject.SetActive(true);
			textPurchase690.gameObject.SetActive(true);
			textPurchase1520.gameObject.SetActive(true);
			textPurchase3500.gameObject.SetActive(true);
			textPurchase7350.gameObject.SetActive(true);
			textPurchase250NoInit.gameObject.SetActive(false);
			textPurchase690NoInit.gameObject.SetActive(false);
			textPurchase1520NoInit.gameObject.SetActive(false);
			textPurchase3500NoInit.gameObject.SetActive(false);
			textPurchase7350NoInit.gameObject.SetActive(false);

		}
		else
		{
			textPurchase250NoInit.text = LocalizationManager.GetString("bank", "BuyCoins");
			textPurchase690NoInit.text = LocalizationManager.GetString("bank", "BuyCoins");
			textPurchase1520NoInit.text = LocalizationManager.GetString("bank", "BuyCoins");
			textPurchase3500NoInit.text = LocalizationManager.GetString("bank", "BuyCoins");
			textPurchase7350NoInit.text = LocalizationManager.GetString("bank", "BuyCoins");
			textPurchase250.gameObject.SetActive(false);
			textPurchase690.gameObject.SetActive(false);
			textPurchase1520.gameObject.SetActive(false);
			textPurchase3500.gameObject.SetActive(false);
			textPurchase7350.gameObject.SetActive(false);
		}
	}

	private void Awake()
	{
		subscriptionEventMessages = Messanger<EventMessages>.Subscribe(EventMessages.UnlockBank, OnUnlockBank);
		subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
		I = this;
	}

	private void Update()
	{
		if (!bankReady && Started)
		{
			if (StoreListener.I.Initialized)
			{
				LoadLocalization();
				btnPurchase250.interactable = true;
				btnPurchase690.interactable = true;
				btnPurchase1520.interactable = true;
				btnPurchase3500.interactable = true;
				btnPurchase7350.interactable = true;
				bankReady = true;
			}
		}
	}

	private void OnDestroy()
	{
		subscriptionEventMessages.Unsubscribe();
		subscriptionMessangerEvents.Unsubscribe();
	}

	private void OnUpdateAnchors() => gameObject.SetActive(false);

	private void OnUnlockBank()
	{
		UnfixButtons();
	}

	private void Start()
	{
		bankHeaderText = objects["bankHeader"].GetComponent<Text>();
		pack250Text = objects["pack250"].GetComponent<Text>();
		pack690Text = objects["pack690"].GetComponent<Text>();
		pack1520Text = objects["pack1520"].GetComponent<Text>();
		pack3500Text = objects["pack3500"].GetComponent<Text>();
		pack7350Text = objects["pack7350"].GetComponent<Text>();

		btnPurchase250 = objects["btnPurchase250"].GetComponent<Button>();
		btnPurchase690 = objects["btnPurchase690"].GetComponent<Button>();
		btnPurchase1520 = objects["btnPurchase1520"].GetComponent<Button>();
		btnPurchase3500 = objects["btnPurchase3500"].GetComponent<Button>();
		btnPurchase7350 = objects["btnPurchase7350"].GetComponent<Button>();

		btnPurchase250.interactable = false;
		btnPurchase690.interactable = false;
		btnPurchase1520.interactable = false;
		btnPurchase3500.interactable = false;
		btnPurchase7350.interactable = false;

		textPurchase250 = objects["textPurchase250"].GetComponent<Text>();
		textPurchase690 = objects["textPurchase690"].GetComponent<Text>();
		textPurchase1520 = objects["textPurchase1520"].GetComponent<Text>();
		textPurchase3500 = objects["textPurchase3500"].GetComponent<Text>();
		textPurchase7350 = objects["textPurchase7350"].GetComponent<Text>();

		textPurchase250NoInit = objects["textPurchase250NoInit"].GetComponent<Text>();
		textPurchase690NoInit = objects["textPurchase690NoInit"].GetComponent<Text>();
		textPurchase1520NoInit = objects["textPurchase1520NoInit"].GetComponent<Text>();
		textPurchase3500NoInit = objects["textPurchase3500NoInit"].GetComponent<Text>();
		textPurchase7350NoInit = objects["textPurchase7350NoInit"].GetComponent<Text>();

		bankButtonClose = objects["bankButtonClose"];

		LoadLocalization();
		changedLanguage = true;

		Started = true;
	}

	private void OnEnable()
	{
		if (!Started) return;

		if (changedLanguage) LoadLocalization();

		
	}
}
