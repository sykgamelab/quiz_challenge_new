﻿using UnityEngine;
using UnityEngine.UI;

public class RefinementHint : Form {
	Game game;
	private HallAssistance hallAssistance;
    private Text refinementHintText, refinementHintYesText, refinementHintNoText;
	private HintType hint;
    private GameObject refinementHintButtonClose;
    private Subscription<MessangerEvents> subscription;
	private int part;

    public static RefinementHint I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnRefinementHintNoClick() => gameObject.SetActive(false);

	private void OnRefinementHintYesClick()
	{
		switch (hint)
		{
			case HintType.FiftyFifty:
				game.UseFiftyFiftyHint();
				break;
			case HintType.HallAssistance:
				int currQuestion = Helper.GetCurrQuestion(game.Category, part);
				int correct = game.Questions[currQuestion].Correct;
				hallAssistance.HallAssistanceHint(game.Category, part, correct, Helper.IsHintUsed(game.Category, part, HintType.FiftyFifty));
				break;
		}

		game.FinishHint(hint);
		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayAnswerEffect, SoundClips.Hints);
		gameObject.SetActive(false);
	}

	private void OnEnable()
	{
		if (!Started) return;
		
		refinementHintYesText.text = LocalizationManager.GetString("passingLevel", "Yes");
		refinementHintNoText.text = LocalizationManager.GetString("passingLevel", "No");
	}

    private void Awake()
    {
        I = this;

        subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subscription.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Start ()
    {
		game = Game.I;
		hallAssistance = HallAssistance.I;

		refinementHintText = objects["refinementHintText"].GetComponent<Text>();
		refinementHintYesText = objects["refinementHintYesText"].GetComponent<Text>();
		refinementHintNoText = objects["refinementHintNoText"].GetComponent<Text>();


        Started = true;
    }

	public void ShowRefinement(HintType hint, int part)
	{
		this.part = part;
		gameObject.SetActive(true);
		this.hint = hint;
		refinementHintText.text = LocalizationManager.GetString("hints", $"Use{hint.ToString()}");
	}
}
