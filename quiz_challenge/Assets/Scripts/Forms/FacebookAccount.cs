﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FacebookAccount : Form {
    private GameObject settings, facebookAccountButtonClose;
    private Text facebookAccountLogoutText, facebookAccountFullname;
    private Image facebookAccountAvatar;
    private Subscription<MessangerEvents> subscriptionMessangerEvents;

    public static FacebookAccount I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);

    private void OnCloseFacebookAccountClick()
    {
        gameObject.SetActive(false);
        settings.SetActive(true);
    }

    private void OnLogoutFacebookAccountClick()
    {
        FacebookSDK instance = FacebookSDK.GetInstance();
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            gameObject.SetActive(false);
            Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
        }
        else if (instance.IsLoggedIn)
        {
            instance.Logout();
            Messanger<EventMessages>.SendMessage(EventMessages.SwitchFacebookButtonText);
            gameObject.SetActive(false);
        }
    }

    private void Awake()
    {
        subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);

        I = this;
    }

    private void OnDestroy()
    {
        subscriptionMessangerEvents.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Start ()
    {
        facebookAccountAvatar = objects["facebookAccountAvatar"].GetComponent<Image>();
        facebookAccountFullname = objects["facebookAccountFullname"].GetComponent<Text>();

        facebookAccountLogoutText = objects["facebookAccountLogoutText"].GetComponent<Text>();
        facebookAccountLogoutText.text = LocalizationManager.GetString("settings", "Logout");

        settings = Settings.I.gameObject;
        
		Started = true;
    }

    private void OnEnable()
    {
		if (!Started) return;
        Messanger<EventMessages>.SendMessage(EventMessages.SwitchFacebookButtonText);
        facebookAccountLogoutText.text = LocalizationManager.GetString("settings", "Logout");
        GraphRequest("/me?fields=first_name,last_name,picture.type(large)");
    }

    private void GraphRequest(string graphPath)
    {
        FacebookSDK.GetInstance().GraphRequest(graphPath, () =>
        {
            string response = FacebookSDK.GetInstance().GraphResponse();
            LoadData(response);
        });
    }

    private void LoadData(string json)
    {
        try
        {
            var objects = Json.Deserialize(json) as Dictionary<string, object>;
            string firstName = (string)objects["first_name"];
            string lastName = (string)objects["last_name"];
            var picture = (Dictionary<string, object>)objects["picture"];
            var data = (Dictionary<string, object>)picture["data"];
            string url = (string)data["url"];
            StartCoroutine(LoadAvatar(url));
            facebookAccountFullname.text = $"{firstName} {lastName}";
        }
        catch (Exception e)
        {
            Debug.Log($"FacebookAccount.LoadData: error = {e.Message}");
        }
    }

    private IEnumerator LoadAvatar(string url)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log($"FacebookAccount.LoadAvatar: error = {www.error}");
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            facebookAccountAvatar.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
    }
}
