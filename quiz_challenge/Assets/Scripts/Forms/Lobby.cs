﻿using UnityEngine;
using UnityEngine.UI;

public class Lobby : Form {
	GameObject bank, settings, categories;
	Text balanceText, startGameText, yourProgressText, /*premiumText,*/ authenticateText;
	Subscription<EventMessages>[] subscriptions;
    public static Lobby I { get; set; }

    private void OnUpdateBalance() => balanceText.text = Helper.GetBalance().ToString();

    private void OnSwitchLanguage()
    {
        startGameText.text = LocalizationManager.GetString("lobby", "StartGame");
        yourProgressText.text = LocalizationManager.GetString("lobby", "YourProgress");
		authenticateText.text = LocalizationManager.GetString("additional", "Achievements");
		//premiumText.text = LocalizationManager.GetString("lobby", "Premium");
	}

    private void OnSwitchAuthenticateButtonText()
    {
        if (!SocialManager.IsAuthenticated())
        {
            string text = LocalizationManager.GetString("settings", "Login");
#if UNITY_EDITOR
            authenticateText.text = "UNITY EDITOR";
#elif UNITY_ANDROID
        authenticateText.text = $"{text} Google Games";
#elif UNITY_IOS
        authenticateText.text = $"{text} Game Center";
#endif
        }
        else authenticateText.text = LocalizationManager.GetString("additional", "Achievements");
    }

    private void Awake()
	{
		subscriptions = new Subscription<EventMessages>[]
		{
			Messanger<EventMessages>.Subscribe(EventMessages.UpdateBalance, OnUpdateBalance),
			Messanger<EventMessages>.Subscribe(EventMessages.SwitchLanguage, OnSwitchLanguage),
			Messanger<EventMessages>.Subscribe(EventMessages.SwitchAuthenticateButtonText, OnSwitchAuthenticateButtonText)
		};

        I = this;
	}

	private void OnDestroy()
	{
		foreach (var s in subscriptions)
		{
			s.Unsubscribe();
		}
	}

	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnGoToSettingsClick() => settings.SetActive(true);
    private void OnGoToBankClick() => bank.SetActive(true);

	private void OnStartGameClick()
	{
		categories.SetActive(true);
		gameObject.SetActive(false);
	}

    private void OnYourProgressClick() => SocialManager.ShowLeaderboard();

    private void OnPremiumClick() => SGLTools.SocialManagerInstantiator.GetInstance().ResetAchievementsForTesters();

    private void OnAuthenticateClick()
    {
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
		}
		else
		{
			if (authenticateText.text.Contains(LocalizationManager.GetString("settings", "Login")))
			{
				SocialManager.Login();
			}
			else
			{
				SocialManager.ShowAchievements();
			}
		}
    }

    private void Start()
	{
        balanceText = objects["balance"].GetComponent<Text>();
		balanceText.text = Helper.GetBalance().ToString();

		startGameText = objects["startGame"].GetComponent<Text>();
        yourProgressText = objects["yourProgress"].GetComponent<Text>();
    //    premiumText = objects["premium"].GetComponent<Text>();
        authenticateText = objects["authenticate"].GetComponent<Text>();

        OnSwitchAuthenticateButtonText();

        bank = Bank.I.gameObject;
		settings = Settings.I.gameObject;
		categories = Categories.I.gameObject;

        OnSwitchLanguage();

		Started = true;
    }
}
