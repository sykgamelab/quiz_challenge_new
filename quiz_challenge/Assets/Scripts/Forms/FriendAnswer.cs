﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FriendAnswer : Form
{
	private Game game;
	private GameObject friendAnswerButtonClose;
    private Text friendAnswerText, friendAnswerLetterText;
    private Image friendAnswerAvatar;
    private Subscription<string, EventMessages> subscriptionEventMessages;
    private Subscription<MessangerEvents> subscriptionMessangerEvents;

    public static FriendAnswer I { get; set; }
	public int Part { get; set; }

	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);

	private void OnCloseFriendAnswerClick()
	{
		game.FinishHint(HintType.FriendAssistance);
		gameObject.SetActive(false);
	}

	private void Awake()
	{
        subscriptionEventMessages = Messanger<string, EventMessages>.Subscribe(EventMessages.LoadFacebookFriendAvatar, OnLoadFacebookFriendAvatar);
        subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);

        I = this;
	}

    private void OnDestroy()
    {
        subscriptionEventMessages.Unsubscribe();
        subscriptionMessangerEvents.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void OnEnable()
	{
		if (!Started) return;

		int currQuestion = Helper.GetCurrQuestion(game.Category, Part);
		int correct = game.Questions[currQuestion].Correct;
		string letter = Helper.GetAnswerLetter(game.Category, Part, correct);
		friendAnswerLetterText.text = letter;
		friendAnswerText.text = LocalizationManager.GetString("additional", "FriendAnswer");

	}

	private void OnLoadFacebookFriendAvatar(string id)
	{
        string graphPath = $"/{id}?fields=picture.type(large)";
        FacebookSDK fb = FacebookSDK.GetInstance();
        fb.GraphRequest(graphPath, () =>
        {
            string json = fb.GraphResponse();
            var objects = Json.Deserialize(json) as Dictionary<string, object>;
            var picture = (Dictionary<string, object>)objects["picture"];
            var data = (Dictionary<string, object>)picture["data"];
            string url = (string)data["url"];
            StartCoroutine(LoadAvatar(url));
        });
	}

	private void Start()
	{
		friendAnswerText = objects["friendAnswerText"].GetComponent<Text>();
		friendAnswerLetterText = objects["friendAnswerLetterText"].GetComponent<Text>();
		friendAnswerAvatar = objects["friendAnswerAvatar"].GetComponent<Image>();

		game = Game.I;

		Started = true;
	}

	private IEnumerator LoadAvatar(string url)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log($"FriendAnswer.LoadAvatar: error = {www.error}");
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            friendAnswerAvatar.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
    }
}
