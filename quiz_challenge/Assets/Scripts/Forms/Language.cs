﻿using UnityEngine;
using UnityEngine.UI;

public class Language : Form {
    private Text languageHeaderText;
    private Image enImg, ruImg, frImg, deImg, esImg;
    private GameObject settings, languageButtonClose;
    private Subscription<MessangerEvents> subscription;

    public static Language I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnCloseLanguageClick() => gameObject.SetActive(false);

    private void OnEnClick() => ChangeStates(SupportedLanguages.En );
    private void OnRuClick() => ChangeStates(SupportedLanguages.Ru);
    private void OnFrClick() => ChangeStates(SupportedLanguages.Fr);
    private void OnDeClick() => ChangeStates(SupportedLanguages.De);
    private void OnEsClick() => ChangeStates(SupportedLanguages.Es);

    private void Awake()
    {
        I = this;

        subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subscription.Unsubscribe();
    }

    private void OnUpdateAnchors()
    {
        enImg.gameObject.SetActive(false);
        ruImg.gameObject.SetActive(false);
        frImg.gameObject.SetActive(false);
        deImg.gameObject.SetActive(false);
        esImg.gameObject.SetActive(false);

        ChangeStates(LocalizationManager.GetCurrent());

        gameObject.SetActive(false);
        settings.SetActive(true);
    }

    private void Start ()
    {
        languageHeaderText = objects["languageHeader"].GetComponent<Text>();
        languageHeaderText.text = LocalizationManager.GetString("settings", "Settings");

        enImg = objects["spriteEn"].GetComponent<Image>();
        ruImg = objects["spriteRu"].GetComponent<Image>();
        frImg = objects["spriteFr"].GetComponent<Image>();
        deImg = objects["spriteDe"].GetComponent<Image>();
        esImg = objects["spriteEs"].GetComponent<Image>();

        settings = Settings.I.gameObject;
        
        Started = true;
    }

    private void ChangeCheckbox(SupportedLanguages lang)
    {
        switch (lang)
        {
            case SupportedLanguages.En:
                enImg.gameObject.SetActive(!enImg.gameObject.activeSelf);
                break;
            case SupportedLanguages.Ru:
                ruImg.gameObject.SetActive(!ruImg.gameObject.activeSelf);
                break;
            case SupportedLanguages.Fr:
                frImg.gameObject.SetActive(!frImg.gameObject.activeSelf);
                break;
            case SupportedLanguages.De:
                deImg.gameObject.SetActive(!deImg.gameObject.activeSelf);
                break;
            case SupportedLanguages.Es:
                esImg.gameObject.SetActive(!esImg.gameObject.activeSelf);
                break;
        }
       
        gameObject.SetActive(false);
    }

    private void ChangeStates(SupportedLanguages lang)
    {
        SupportedLanguages currentLang = LocalizationManager.GetCurrent();

        ChangeCheckbox(lang);

        if (lang != currentLang)
        {
            ChangeCheckbox(currentLang);
            LocalizationManager.SetCurrent(lang);
            Messanger<EventMessages>.SendMessage(EventMessages.SwitchLanguage);
        }
    }

    private void OnEnable()
    {
		if (!Started) return;

        languageHeaderText.text = LocalizationManager.GetString("settings", "Settings");
    }
}
