﻿using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class Settings : Form
{
	private Text settingsHeaderText, soundText, musicText, languageText, languageShortText, facebookText, rateUsText, restorePurchasesText;
	private Sprite stateOff, stateOn;
	private Image toggleSoundImg, toggleMusicImg, languageFlagImg;
	private GameObject language, facebookAccount, settingsButtonClose;
	private bool changedLanguage;
	private Subscription<EventMessages>[] subscriptionsEventMessages;
	private Subscription<MessangerEvents> subscriptionMessangerEvents;
    private FacebookSDK instance;
	public static Settings I { get; set; }

	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
	private void OnCloseSettingsClick() => gameObject.SetActive(false);

	private void OnFacebookClick()
	{
        instance = FacebookSDK.GetInstance();
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			gameObject.SetActive(false);
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
		}
		else if (!instance.IsLoggedIn)
		{
			instance.Login(
				(r) => 
				{
					Debug.Log($"SETTINGS LOGGED IN: success = {r.AuthenticationSuccess}, cancel = {r.AuthenticationCancel}, error = {r.LastError}");
                    if (r.AuthenticationSuccess)
                    {
                        facebookAccount.SetActive(true);
                        gameObject.SetActive(false);
                    }
                }, "Ошибка");
		}
		else
		{
            facebookAccount.SetActive(true);
            gameObject.SetActive(false);
		}
	}

	private void OnRateUsClick()
	{
		if (Application.internetReachability != NetworkReachability.NotReachable)
		{
			RateUsRefinement.I.Show();
		}
		else
		{
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
		}
	}

    private void OnRestorePurchases()
    {
        StoreListener.RestorePurchases();
    }

    private void OnLanguageClick()
	{
		language.SetActive(true);
		gameObject.SetActive(false);
	}

	private void OnSoundClick()
	{
		bool active = Helper.IsSoundActive();
		active = !active;

		if (active) toggleSoundImg.sprite = stateOn;
		else toggleSoundImg.sprite = stateOff;

		Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetSoundActive, active);
		Helper.SetSoundActive(active);
	}

	private void OnMusicClick()
	{
		bool active = Helper.IsMusicActive();
		active = !active;

		if (active) toggleMusicImg.sprite = stateOn;
		else toggleMusicImg.sprite = stateOff;

		Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, active);
		Helper.SetMusicActive(active);
		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayMusic, SoundClips.Lobby);
	}

	private void LoadLocalization()
	{
		SupportedLanguages lang = LocalizationManager.GetCurrent();

		settingsHeaderText.text = LocalizationManager.GetString("settings", "Settings");
		soundText.text = LocalizationManager.GetString("settings", "Sound");
		musicText.text = LocalizationManager.GetString("settings", "Music");
		languageText.text = LocalizationManager.GetString("settings", "Language");
		languageShortText.text = LocalizationManager.GetLanguageShort(lang);
		languageFlagImg.sprite = LocalizationManager.GetFlag(lang);
		OnSwitchFacebookButtonText();
		rateUsText.text = LocalizationManager.GetString("settings", "RateGame");
		restorePurchasesText.text = LocalizationManager.GetString("additional", "RestorePurchases");
	}

	private void Awake()
	{
		subscriptionsEventMessages = new Subscription<EventMessages>[]
		{
			Messanger<EventMessages>.Subscribe(EventMessages.SwitchFacebookButtonText, OnSwitchFacebookButtonText),
			Messanger<EventMessages>.Subscribe(EventMessages.FacebookSuccessSettings, OnFacebookSuccessSettings)
		};

		subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);

		I = this;
	}

	private void OnDestroy()
	{
		foreach (var s in subscriptionsEventMessages)
		{
			s.Unsubscribe();
		}

		subscriptionMessangerEvents.Unsubscribe();
	}

	private void OnUpdateAnchors() => gameObject.SetActive(false);

	private void Start()
	{
		settingsHeaderText = objects["settingsHeader"].GetComponent<Text>();
		soundText = objects["sound"].GetComponent<Text>();
		musicText = objects["music"].GetComponent<Text>();
		languageText = objects["language"].GetComponent<Text>();
		languageShortText = objects["languageShort"].GetComponent<Text>();
		languageFlagImg = objects["languageFlag"].GetComponent<Image>();
		facebookText = objects["facebook"].GetComponent<Text>();
		rateUsText = objects["rateUs"].GetComponent<Text>();
		restorePurchasesText = objects["restorePurchasesText"].GetComponent<Text>();

		stateOff = URPNGLoader.LoadURPNG("sprite_2", 27);
		stateOn = URPNGLoader.LoadURPNG("sprite_2", 30);

		toggleSoundImg = objects["soundToggle"].GetComponent<Image>();
		toggleMusicImg = objects["musicToggle"].GetComponent<Image>();

		if (Helper.IsSoundActive()) toggleSoundImg.sprite = stateOn;
		else toggleSoundImg.sprite = stateOff;

		if (Helper.IsMusicActive()) toggleMusicImg.sprite = stateOn;
		else toggleMusicImg.sprite = stateOff;

		language = Language.I.gameObject;
		facebookAccount = FacebookAccount.I.gameObject;

		LoadLocalization();
		changedLanguage = true;
		Started = true;
	}

	private void OnSwitchFacebookButtonText()
	{
		if (facebookText != null)
		{
			if (FacebookSDK.GetInstance().IsLoggedIn)
			{
				facebookText.text = LocalizationManager.GetString("settings", "Logout");
			}
			else
			{
				facebookText.text = LocalizationManager.GetString("settings", "Login");
			}
		}
	}

	private void OnFacebookSuccessSettings()
	{
		gameObject.SetActive(false);
		facebookAccount.SetActive(true);
	}

	private void OnEnable()
	{
		if (!Started) return;

		if (changedLanguage) LoadLocalization();

		OnSwitchFacebookButtonText();
	}
}
