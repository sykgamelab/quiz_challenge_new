﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class RateUsRefinement : Form
{
	private Text rateUsRefinementText, rateUsRefinementYesText, rateUsRefinementNoText;
	private Subscription<MessangerEvents> subscription;

	public static RateUsRefinement I { get; private set;  }

	private void Awake()
	{
		I = this;
		subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
	}

	private void OnUpdateAnchors() => gameObject.SetActive(false);

	private void OnDestroy()
	{
		subscription.Unsubscribe();
	}
	// Start is called before the first frame update
	void Start()
    {
		rateUsRefinementText = objects["rateUsRefinementText"].GetComponent<Text>();
		rateUsRefinementYesText = objects["rateUsRefinementYesText"].GetComponent<Text>();
		rateUsRefinementNoText = objects["rateUsRefinementNoText"].GetComponent<Text>();
	
		Started = true;
    }

	public void Show() => gameObject.SetActive(true);
	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);

	private void OnRefinementRateUsYesClick()
	{
#if UNITY_IOS
			if (!Device.RequestStoreReview())
			{
				Application.OpenURL("itms-apps://itunes.apple.com/app/1435724970");
			}
#elif UNITY_ANDROID
		Application.OpenURL("market://details?id=urmobi.games.quizchallenge");
#endif
		gameObject.SetActive(false);
	}

	private void OnRefinementRateUsNoClick() => gameObject.SetActive(false);

	private void OnEnable()
	{
		if (!Started) return;

		rateUsRefinementText.text = LocalizationManager.GetString("additional", "RateUs");
		rateUsRefinementYesText.text = LocalizationManager.GetString("passingLevel", "Yes");
		rateUsRefinementNoText.text = LocalizationManager.GetString("passingLevel", "No");
	}

	// Update is called once per frame
	void Update()
    {
	}
}
