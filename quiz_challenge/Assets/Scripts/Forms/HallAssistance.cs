using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HallAssistance : Form {
    private Text hallAssistanceTextOk;
    private GameObject hallAssistanceButtonClose;
    private Subscription<MessangerEvents> subscription;
	private int part;

    public static HallAssistance I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnCloseHallAssistanceClick() => gameObject.SetActive(false);
    private void HallAssistanceOkClick() => gameObject.SetActive(false);

    private void Awake()
    {
        I = this;

        subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subscription.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Start ()
    {
        hallAssistanceTextOk = objects["HallAssistanceTextOk"].GetComponent<Text>();
        hallAssistanceTextOk.text = LocalizationManager.GetString("hints", "Ok");

        Dictionary<string, string> dict = new Dictionary<string, string>()
        {
            { "HallAssistanceAnswerA", "A" },
            { "HallAssistanceAnswerB", "B" },
            { "HallAssistanceAnswerC", "C" },
            { "HallAssistanceAnswerD", "D" },
        };

        foreach (KeyValuePair<string, string> keyValuePair in dict)
        {
            GameObject answerLetter = objects[keyValuePair.Key];
            GameObject answer = Instantiate(Resources.Load<GameObject>("Prefabs/HallAssistanceAnswerPrefab"));
            answer.transform.SetParent(answerLetter.transform);

            RectTransform answerRectTransform = answer.GetComponent<RectTransform>();
            answerRectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            answerRectTransform.anchorMax = new Vector2(0.5f, 0.5f);
            answerRectTransform.anchoredPosition = new Vector2(0f, 0f);

            answerRectTransform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("hall-assisstance-fill", 0);

            Transform backgroundTransform = answerRectTransform.GetChild(1);
            backgroundTransform.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("sprite_2", 13);
            backgroundTransform.GetChild(0).GetComponent<Text>().text = keyValuePair.Value;
        }

		Started = true;
    }

	private void OnEnable()
	{
		if (!Started) return;

		hallAssistanceTextOk.text = LocalizationManager.GetString("hints", "Ok");
	}

    private void ChangePercentages(int[] result)
    {
        Transform[] transforms = new Transform[4]
        {
            objects["HallAssistanceAnswerA"].transform.GetChild(0),
            objects["HallAssistanceAnswerB"].transform.GetChild(0),
            objects["HallAssistanceAnswerC"].transform.GetChild(0),
            objects["HallAssistanceAnswerD"].transform.GetChild(0)
        };

        CustomSlider sliderA = transforms[0].GetComponent<CustomSlider>();
        CustomSlider sliderB = transforms[1].GetComponent<CustomSlider>();
        CustomSlider sliderC = transforms[2].GetComponent<CustomSlider>();
        CustomSlider sliderD = transforms[3].GetComponent<CustomSlider>();

        Text textA = transforms[0].GetChild(2).GetChild(0).GetComponent<Text>();
        Text textB = transforms[1].GetChild(2).GetChild(0).GetComponent<Text>();
        Text textC = transforms[2].GetChild(2).GetChild(0).GetComponent<Text>();
        Text textD = transforms[3].GetChild(2).GetChild(0).GetComponent<Text>();

        sliderA.value = result[0] / 100f;
        sliderB.value = result[1] / 100f;
        sliderC.value = result[2] / 100f;
        sliderD.value = result[3] / 100f;

        textA.text = result[0].ToString() + "%";
        textB.text = result[1].ToString() + "%";
        textC.text = result[2].ToString() + "%";
        textD.text = result[3].ToString() + "%";

        for (int i = 0; i < 4; i++)
        {
            if (result[i] == 0) transforms[i].GetChild(0).gameObject.SetActive(false);
            else transforms[i].GetChild(0).gameObject.SetActive(true);
        }
    }

	private int LetterToInt(string letter)
	{
		int nmb = 0;

		switch (letter)
		{
			case "A":
				nmb = 0;
				break;
			case "B":
				nmb = 1;
				break;
			case "C":
				nmb = 2;
				break;
			case "D":
				nmb = 3;
				break;
		}

		return nmb;
	}

    public void HallAssistanceHint(string category, int part, int correct, bool fiftyFiftyUsed)
    {
		this.part = part;

        int[] result = new int[4];
        int incorrect = Random.Range(0, 4);
        while (incorrect == correct)
        {
            incorrect = Random.Range(0, 4);
        }

        int left = fiftyFiftyUsed ? 100 : 80;
        int seed = Random.Range(0, 80);

        int correctPercentage = 0, incorrectPercentage = 0;
        if (seed < 80)
        {
            correctPercentage = Random.Range(fiftyFiftyUsed ? 51 : 41, left);
            incorrectPercentage = left - correctPercentage;
        }
        else
        {
            incorrectPercentage = Random.Range(fiftyFiftyUsed ? 51 : 41, 80);
            correctPercentage = left - incorrectPercentage;
        }
        left = 100 - left;

        int incorrect1 = -1, incorrect2 = -1;
        for (int i = 0; i < 4; i++)
        {
            if (i != incorrect && i != correct)
            {
                if (incorrect1 == -1)
                {
                    incorrect1 = i;
                }
                else
                {
                    incorrect2 = i;
                }
            }
        }

        int incorrect1Percentage = Random.Range(0, left);
        left -= incorrect1Percentage;
        int incorrect2Percentage = left;

		string letter = Helper.GetAnswerLetter(category, part, correct);
		correct = LetterToInt(letter);

		letter = Helper.GetAnswerLetter(category, part, incorrect);
		incorrect = LetterToInt(letter);

		letter = Helper.GetAnswerLetter(category, part, incorrect1);
		incorrect1 = LetterToInt(letter);

		letter = Helper.GetAnswerLetter(category, part, incorrect2);
		incorrect2 = LetterToInt(letter);

		result[correct] = correctPercentage;
        result[incorrect] = incorrectPercentage;
        result[incorrect1] = incorrect1Percentage;
        result[incorrect2] = incorrect2Percentage;

        ChangePercentages(result);

		gameObject.SetActive(true);
    }
}
