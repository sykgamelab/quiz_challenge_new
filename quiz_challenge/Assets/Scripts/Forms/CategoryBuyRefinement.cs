﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryBuyRefinement : Form
{
	private Subscription<MessangerEvents> subscription;
	private Text categoryBuyRefinement;
	private CategoryButton cb;
	private IYandexAppMetrica appmetrica;
	private Text refinementCategoryBuyText;

	public static CategoryBuyRefinement I { get; set; }
	
	private void OnRefinementCategoryBuyYesClick()
	{
		if (cb.Part < 0)
		{
			Categories.I.BlockInterface();
			Messanger<string, EventMessages>.SendMessage(EventMessages.BuyProduct, cb.Prodid);
		}
		else
		{
			int balance = Helper.GetBalance();
			if (balance >= GameConstants.PartPrice)
			{
				appmetrica.ReportEvent(MetricaConstants.CategoryPartBought,
					new Dictionary<string, object>()
					{
						{ MetricaConstants.Category, cb.Category },
						{ MetricaConstants.Part, cb.Part }
					});
				balance -= GameConstants.PartPrice;
				Helper.SetBalance(balance);
				Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
				//Helper.SetCategoryLocked(cb.Category, cb.Part, false);
				Helper.SetCategoryState(cb.Category, cb.Part, CategoryButton.State.Unlocked);
				cb.ButtonState = CategoryButton.State.Unlocked;

				Debug.Log("SERVER SET SUBCATEGORY BUY PROGRESS");
				SocialManager.I.SetProgressToReport();
			}
			else GoToBank.I.gameObject.SetActive(true);
		}
		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
		gameObject.SetActive(false);
	}

	private void OnRefinementCategoryBuyCloseClick()
	{
		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
		gameObject.SetActive(false);
	}

	public void Show(CategoryButton cb)
	{
		this.cb = cb;
		string text;

		if (cb.Part < 0)
		{
			string replacement = "1$";
			if (StoreListener.Prices != null) replacement = StoreListener.Prices[cb.Prodid];
			text = LocalizationManager.GetString("additional", "BuyCategory").Replace("??", replacement);
		}
		else text = LocalizationManager.GetString("additional", "BuySubcategory");

		refinementCategoryBuyText.text = LocalizationManager.GetString("additional", "Buy");

		categoryBuyRefinement.text = text;
		gameObject.SetActive(true);
	}

	private void OnUpdateAnchors()
	{
		gameObject.SetActive(false);
	}

	private void Awake()
	{
		I = this;
	}

	// Start is called before the first frame update
	void Start()
    {
		appmetrica = AppMetrica.Instance;
		subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
		categoryBuyRefinement = objects["categoryBuyRefinement"].GetComponent<Text>();
		refinementCategoryBuyText = objects["refinementCategoryBuyText"].GetComponent<Text>();
		Started = true;
	}

	private void OnDestroy()
	{
		subscription.Unsubscribe();
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
