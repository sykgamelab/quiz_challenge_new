﻿using UnityEngine;
using UnityEngine.UI;

public class GoToBank : Form
{
    private GameObject bank;
    private Text goToBankText, goToBankOkText, goToBankCancelText;
    private Subscription<MessangerEvents> subscription;

    public static GoToBank I { get; set; }

	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
	private void OnGoToBankCancelClick() => gameObject.SetActive(false);

	private void OnGoToBankOkClick()
	{
		bank.SetActive(true);
		gameObject.SetActive(false);
	}

    private void Awake()
    {
        I = this;

        subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subscription.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Start()
	{
		bank = Bank.I.gameObject;

		goToBankText = objects["goToBankText"].GetComponent<Text>();
		goToBankOkText = objects["goToBankOkText"].GetComponent<Text>();
		goToBankCancelText = objects["goToBankCancelText"].GetComponent<Text>();

		goToBankText.text = LocalizationManager.GetString("notEnoughCoins", "NotEnoughCoins");
		goToBankOkText.text = LocalizationManager.GetString("passingLevel", "Yes"); ;
		goToBankCancelText.text = LocalizationManager.GetString("passingLevel", "No"); 

		Started = true;
	}

    private void OnEnable()
    {
		if (!Started) return;

		goToBankText.text = LocalizationManager.GetString("notEnoughCoins", "NotEnoughCoins");
		//goToBankText.text = "Недостаточно монет! Желаете перейти в банк?";
		goToBankOkText.text = LocalizationManager.GetString("passingLevel", "Yes"); ;
		goToBankCancelText.text = LocalizationManager.GetString("passingLevel", "No");
	}
}
