﻿using System;
using UnityEngine.UI;

public class RefinementProgress : Form {
    private Text refinementProgressText, refinementProgressYesText, refinementProgressNoText;
    private Subscription<MessangerEvents> subscription;
	Action<bool> callback;

    public static RefinementProgress I { get; set; }

    private void OnRefinementProgressYesClick()
    {
		callback?.Invoke(true);
        gameObject.SetActive(false);
    }

	private void OnRefinementProgressNoClick()
	{
		callback?.Invoke(false);
		gameObject.SetActive(false);
	}
    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);

	public void Show(Action<bool> callback)
	{
		this.callback = callback;
		gameObject.SetActive(true);
	}

    private void Awake()
    {
        I = this;

        subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        subscription.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Start()
    {
        refinementProgressText = objects["refinementProgressText"].GetComponent<Text>();
        refinementProgressYesText = objects["refinementProgressYesText"].GetComponent<Text>();
        refinementProgressNoText = objects["refinementProgressNoText"].GetComponent<Text>();

        Started = true;
    }

    private void OnEnable()
    {
        if (!Started) return;

        refinementProgressText.text = LocalizationManager.GetString("additional", "ServerSynchronization");
        refinementProgressYesText.text = LocalizationManager.GetString("passingLevel", "Yes");
        refinementProgressNoText.text = LocalizationManager.GetString("passingLevel", "No");
    }
}
