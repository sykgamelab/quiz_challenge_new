﻿using UnityEngine;
using UnityEngine.UI;

public class Error : Form {
    Text errorText, errorOkText;
    Subscription<string, EventMessages> subscriptionEventMessages;
    Subscription<MessangerEvents> subscriptionMessangerEvents;
    private GameObject errorButtonClose;

    public static Error I { get; set; }

    private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
    private void OnErrorClick() => gameObject.SetActive(false);
    private void OnCloseErrorClick() => gameObject.SetActive(false);

    private void Awake()
    {
        subscriptionEventMessages = Messanger<string, EventMessages>.Subscribe(EventMessages.ErrorOccured, OnErrorOccured);
        subscriptionMessangerEvents = Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors);

        I = this;
    }

    private void OnDestroy()
    {
        subscriptionEventMessages.Unsubscribe();
        subscriptionMessangerEvents.Unsubscribe();
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void OnErrorOccured(string text)
    {
        errorText.text = text;
        gameObject.SetActive(true);
    }

    private void Start ()
    {
        errorText = objects["errorText"].GetComponent<Text>();
        errorOkText = objects["errorOkText"].GetComponent<Text>();

        errorOkText.text = errorOkText.text = LocalizationManager.GetString("hints", "Ok");


		Started = true;
    }

    private void OnEnable()
    {
		if (!Started) return;
		errorOkText.text = LocalizationManager.GetString("hints", "Ok");
    }
}
