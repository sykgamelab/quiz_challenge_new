﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Categories : Form
{
	private GameObject bank, lobby, categoriesScrollHolder, game, categoriesPartsHolder, blocker;
	private Text balanceText, categoriesHeader;
	private Subscription<EventMessages>[] subscriptions;
	private Subscription<MessangerEvents>[] subscriptions2;
	private Subscription<string, EventMessages>[] subscriptions3;
	private CategoryButton[] categoryButtons;
	private CategoryButton[] categoryPartsButtons = new CategoryButton[GameConstants.CategoryParts];
	private bool choosePart = false;
	private float[] yPositions;
	private IYandexAppMetrica appmetrica;
	private Dictionary<string, Question[]> categories;
	private Dictionary<string, (string, int)> descriptions;
	private Game gameForm;
	private CategoryBuyRefinement categoryBuyRefinement;

	public static Categories I { get; set; }

	private void OnUpdateBalance() => balanceText.text = Helper.GetBalance().ToString();
	private void OnUITapClick() => Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
	private void OnGoToBankClick() => bank.SetActive(true);

	private void OnSwitchLanguage()
	{
		Tuple<Dictionary<string, Question[]>, Dictionary<string, (string, int)>> tuple = Helper.LoadCategories();
		categories = tuple.Item1;
		descriptions = tuple.Item2;
	}

	private void OnCategoryBought(string prodid)
	{
		if (prodid != null)
		{
			foreach (var cb in categoryButtons)
			{
				if (cb.Prodid == prodid)
				{
					//cb.Locked = false;
					cb.ButtonState = CategoryButton.State.ReadyToGet;
					//Helper.SetCategoryLocked(cb.Category, -1, false);
					Helper.SetCategoryState(cb.Category, -1, CategoryButton.State.ReadyToGet);
					SortCategories();
					break;
				}
			}
			Debug.Log("SERVER SET CATEGORIES PROGRESS");
			if (ServerManager.ProgressSynchronized)
			{
				SocialManager.I.SetProgressToReport();
			}
		}

		UnlockInterface();
	}

	private void OnUpdateAnchors()
	{
		foreach (var cb in categoryButtons)
		{
			Rect parentRect = cb.gameObject.transform.parent.GetComponent<RectTransform>().rect;
			RectTransform rectTransform = cb.gameObject.GetComponent<RectTransform>();
			Rect rect = rectTransform.rect;
			float offsetMinX, offsetMinY, offsetMaxX, offsetMaxY;
			offsetMinX = offsetMaxX = (parentRect.width - rectTransform.rect.width) / 2f;
			offsetMinY = offsetMaxY = (parentRect.height - rectTransform.rect.height) / 2f;

			AspectRatioFitter arf = cb.gameObject.GetComponent<AspectRatioFitter>();
			arf.aspectMode = AspectRatioFitter.AspectMode.None;

			rectTransform.offsetMin = new Vector2(offsetMinX, offsetMinY);
			rectTransform.offsetMax = new Vector2(-offsetMaxX, -offsetMaxY);
		}

		float y = 0f;
		float spacing = Screen.height / 100f;
		foreach (var categoryButton in categoryButtons)
		{
			GameObject currParent = categoryButton.transform.parent.gameObject;
			categoryButton.transform.SetParent(currParent.transform.parent, true);
			RectTransform rectTransform = categoryButton.GetComponent<RectTransform>();
			Rect rect = rectTransform.rect;
			rectTransform.offsetMin = rectTransform.offsetMax = new Vector2(0f, 0f);
			rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.5f, 1f);
			rectTransform.sizeDelta = new Vector2(rect.width, rect.height);
			rectTransform.pivot = new Vector2(0.5f, 1f);
			rectTransform.anchoredPosition = new Vector2(0, -y);
			y += rect.height + spacing;

			Destroy(currParent);
		}

		RectTransform contentRectTransform = categoryButtons[0].transform.parent.GetComponent<RectTransform>();
		Rect contentRect = contentRectTransform.rect;
		contentRectTransform.anchorMin = contentRectTransform.anchorMax = new Vector2(0f, 1f);
		contentRectTransform.offsetMin = contentRectTransform.offsetMax = new Vector2(0f, 0f);
		contentRectTransform.sizeDelta = new Vector2(contentRect.width, y);
		contentRectTransform.rect.Set(0, 0, contentRect.width, y);


		foreach (var cb in categoryPartsButtons)
		{
			Rect parentRect = cb.gameObject.transform.parent.GetComponent<RectTransform>().rect;
			RectTransform rectTransform = cb.gameObject.GetComponent<RectTransform>();
			Rect rect = rectTransform.rect;
			float offsetMinX, offsetMinY, offsetMaxX, offsetMaxY;
			offsetMinX = offsetMaxX = (parentRect.width - rectTransform.rect.width) / 2f;
			offsetMinY = offsetMaxY = (parentRect.height - rectTransform.rect.height) / 2f;

			AspectRatioFitter arf = cb.gameObject.GetComponent<AspectRatioFitter>();
			arf.aspectMode = AspectRatioFitter.AspectMode.None;

			rectTransform.offsetMin = new Vector2(offsetMinX, offsetMinY);
			rectTransform.offsetMax = new Vector2(-offsetMaxX, -offsetMaxY);
		}

		y = 0f;
		foreach (var categoryButton in categoryPartsButtons)
		{
			GameObject currParent = categoryButton.transform.parent.gameObject;
			categoryButton.transform.SetParent(currParent.transform.parent, true);
			RectTransform rectTransform = categoryButton.GetComponent<RectTransform>();
			Rect rect = rectTransform.rect;
			rectTransform.offsetMin = rectTransform.offsetMax = new Vector2(0f, 0f);
			rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.5f, 1f);
			rectTransform.sizeDelta = new Vector2(rect.width, rect.height);
			rectTransform.pivot = new Vector2(0.5f, 1f);
			rectTransform.anchoredPosition = new Vector2(0, -y);
			y += rect.height + spacing;

			Destroy(currParent);
		}

		RectTransform categoriesPartsRectTransform = categoriesPartsHolder.GetComponent<RectTransform>();
		Rect categoriesPartsRect = categoriesPartsRectTransform.rect;
		categoriesPartsRectTransform.anchorMin = categoriesPartsRectTransform.anchorMax = new Vector2(0f, 1f);
		categoriesPartsRectTransform.offsetMin = categoriesPartsRectTransform.offsetMax = new Vector2(0f, 0f);
		categoriesPartsRectTransform.sizeDelta = new Vector2(contentRect.width, y);
		categoriesPartsRectTransform.rect.Set(0, 0, contentRect.width, y);

		for (int i = 0; i < categoryButtons.Length; i++) yPositions[i] = categoryButtons[i].transform.localPosition.y;

		blocker.transform.SetAsLastSibling();
		categoriesPartsHolder.SetActive(false);
		gameObject.SetActive(false);
	}

	private void Awake()
	{
		subscriptions = new Subscription<EventMessages>[]
		{
			Messanger<EventMessages>.Subscribe(EventMessages.UpdateBalance, OnUpdateBalance),
			Messanger<EventMessages>.Subscribe(EventMessages.SwitchLanguage, OnSwitchLanguage),
		};

		subscriptions2 = new Subscription<MessangerEvents>[]
		{
			Messanger<MessangerEvents>.Subscribe(MessangerEvents.UpdateAnchors, OnUpdateAnchors)
		};

		subscriptions3 = new Subscription<string, EventMessages>[]
		{
			Messanger<string, EventMessages>.Subscribe(EventMessages.CategoryBought, OnCategoryBought)
		};

		I = this;
	}

	private void OnDestroy()
	{
		foreach (var s in subscriptions)
		{
			s.Unsubscribe();
		}
		foreach (var s in subscriptions2)
		{
			s.Unsubscribe();
		}
		foreach (var s in subscriptions3)
		{
			s.Unsubscribe();
		}
	}

	private void OnGoToLobbyClick()
	{
		if (!choosePart)
		{
			gameObject.SetActive(false);
			lobby.SetActive(true);
		}
		else
		{
			choosePart = false;
			categoriesScrollHolder.SetActive(true);
			categoriesPartsHolder.SetActive(false);
		}
	}

	public void SortCategories()
	{
		int k = 0;
		foreach (var cb in categoryButtons)
		{
			int total = 0;
			for (int i = 0; i < GameConstants.CategoryParts; i++) total += Helper.GetCurrQuestion(cb.Category, i);
			if (total == GameConstants.PerCategoryQuestions)
			{
				//cb.Locked = false;
				cb.ButtonState = CategoryButton.State.Unlocked;
				Vector3 pos = cb.transform.localPosition;
				cb.transform.localPosition = new Vector3(pos.x, yPositions[k], pos.z);
				k++;
			}
		}

		foreach (var cb in categoryButtons)
		{
			//if (Helper.IsCategoryLocked(cb.Category, -1)) continue;

			//cb.Locked = false;

			CategoryButton.State state = Helper.GetCategoryState(cb.Category, -1);
			if (state != CategoryButton.State.Unlocked) continue;

			cb.ButtonState = state;
			int total = 0;
			for (int i = 0; i < GameConstants.CategoryParts; i++) total += Helper.GetCurrQuestion(cb.Category, i);
			if (total != GameConstants.PerCategoryQuestions)
			{
				Vector3 pos = cb.transform.localPosition;
				cb.transform.localPosition = new Vector3(pos.x, yPositions[k], pos.z);
				k++;
			}
		}

		foreach (var cb in categoryButtons)
		{
			//if (Helper.IsCategoryLocked(cb.Category, -1)) continue;

			//cb.Locked = false;

			CategoryButton.State state = Helper.GetCategoryState(cb.Category, -1);
			if (state == CategoryButton.State.ReadyToGet)
			{
				cb.ButtonState = state;
				Vector3 pos = cb.transform.localPosition;
				cb.transform.localPosition = new Vector3(pos.x, yPositions[k], pos.z);
				k++;
			}
		}

		foreach (var cb in categoryButtons)
		{
			CategoryButton.State state = Helper.GetCategoryState(cb.Category, -1);
			if (state == CategoryButton.State.Locked)
			{
				Vector3 pos = cb.transform.localPosition;
				cb.transform.localPosition = new Vector3(pos.x, yPositions[k], pos.z);
				k++;
			}

		}
	}

	private void OnEnable()
	{
		if (!Started) return;

		categoriesHeader.text = LocalizationManager.GetString("categorySelection", "CategorySelection");
		balanceText.text = Helper.GetBalance().ToString();

		int done = Helper.GetCategoriesDone();
		int doneReal = 0;
		foreach (var cb in categoryButtons)
		{
			int total = 0;
			for (int i = 0; i < GameConstants.CategoryParts; i++) total += Helper.GetCurrQuestion(cb.Category, i);
			cb.SetProgress(total, GameConstants.PerCategoryQuestions);
			cb.transform.GetChild(1).GetComponent<Text>().text = LocalizationManager.GetString("categorySelection", cb.Category);
			cb.UpdatePartsDone();

			if (Helper.GetAchievementProgress(cb.Category) != total)
			{
				Helper.SetAchievementProgress(cb.Category, total);
				Helper.SetAchievementToReport(cb.Category, true);
			}

			if (total == GameConstants.PerCategoryQuestions) doneReal++;
		}

		if (done != doneReal)
		{
			Helper.SetCategoriesDone(done + 1);
			for (int i = 0; i < categoryButtons.Length; i++)
			{
				if (categoryButtons[i].ButtonState == CategoryButton.State.Locked)
				{
					categoryButtons[i].ButtonState = CategoryButton.State.ReadyToGet;
					//Helper.SetCategoryLocked(categoryButtons[i].Category, -1, false);
					Helper.SetCategoryState(categoryButtons[i].Category, -1, CategoryButton.State.ReadyToGet);
					SocialManager.I.SetProgressToReport();
					break;
				}
			}

			SortCategories();
		}

		if (choosePart)
		{
			int doneParts = 0, time = 0;
			foreach (var cb in categoryPartsButtons)
			{
				cb.SetProgress(Helper.GetCurrQuestion(cb.Category, cb.Part), cb.Questions.Length);
				string part = LocalizationManager.GetString("additional", "Part");
				cb.transform.GetChild(1).GetComponent<Text>().text =
					$"{LocalizationManager.GetString("categorySelection", cb.Category)} - {part} {cb.Part + 1}";

				if (Helper.GetCurrQuestion(cb.Category, cb.Part) == cb.Questions.Length)
				{
					if (cb.Part + 1 < GameConstants.CategoryParts)
					{
						Helper.SetCategoryState(cb.Category, cb.Part + 1, CategoryButton.State.Unlocked);
						categoryPartsButtons[cb.Part + 1].ButtonState = CategoryButton.State.Unlocked;
					}
					time += Helper.GetCategoryPartTime(cb.Category, cb.Part);
					doneParts++;
				}
			}

			if (doneParts == GameConstants.CategoryParts)
			{
				appmetrica.ReportEvent(MetricaConstants.CategoryDone,
				new Dictionary<string, object>()
				{
					{ MetricaConstants.Category, categoryPartsButtons[0].Category },
					{ MetricaConstants.Time, (int)(time / 60f + 0.5f)  }
				});

				choosePart = false;
				categoriesScrollHolder.SetActive(true);
				categoriesPartsHolder.SetActive(false);
			}
		}

		if (doneReal == categoryButtons.Length)
		{
			int time = 0;
			foreach (var cb in categoryButtons)
			{
				for (int i = 0; i < GameConstants.CategoryParts; i++)
				{
					time += Helper.GetCategoryPartTime(cb.Category, i);
				}
			}

			appmetrica.ReportEvent(MetricaConstants.GameDone, new Dictionary<string, object>()
			{
				{ MetricaConstants.Time, (int)(time / 60f + 0.5f ) }
			});
		}

		for (int i = 0; i < categoryPartsButtons.Length; i++)
		{
			RectTransform categoryRectTransform = categoryButtons[i].GetComponent<RectTransform>();
			RectTransform categoryPartRectTransform = categoryPartsButtons[i].GetComponent<RectTransform>();

			categoryPartRectTransform.sizeDelta = categoryRectTransform.sizeDelta;
			float y = yPositions[i];
			float x = categoryButtons[i].gameObject.transform.localPosition.x;
			float z = categoryButtons[i].gameObject.transform.localPosition.z;
			categoryPartsButtons[i].gameObject.transform.localPosition = new Vector3(x, y, z);
		}
	}

	private void OnCategoryClick(CategoryButton button)
	{
		if (button.ButtonState == CategoryButton.State.Unlocked)
		{
			choosePart = true;
			for (int i = 0; i < categoryPartsButtons.Length; i++)
			{
				CategoryButton cb = categoryPartsButtons[i];
				cb.Category = button.Category;
				cb.Part = i;
				cb.Questions = categories[$"{button.Category}{i}"];
				cb.gameObject.GetComponent<Image>().sprite = button.gameObject.GetComponent<Image>().sprite;
				cb.SetProgress(Helper.GetCurrQuestion(button.Category, i), cb.Questions.Length);
				string part = LocalizationManager.GetString("additional", "Part");
				cb.transform.GetChild(1).GetComponent<Text>().text =
					$"{LocalizationManager.GetString("categorySelection", button.Category)} - {part} {i + 1}";
				cb.CategorySprite = button.CategorySprite;
				cb.Number = button.Number;
			}
			categoriesScrollHolder.SetActive(false);
			categoriesPartsHolder.SetActive(true);

			int opened = 0;
			for (int i = 0; i < categoryPartsButtons.Length; i++)
			{
				if (Helper.GetCategoryState(button.Category, i) == CategoryButton.State.Unlocked)
				{
					categoryPartsButtons[i].ButtonState = CategoryButton.State.Unlocked;
					opened++;
				}
				else
				{
					categoryPartsButtons[i].ButtonState = CategoryButton.State.Locked;
				}
			}

			for (int i = 0; i < GameConstants.CategoryParts; i++)
			{
				if (i + 1 < GameConstants.CategoryParts)
				{
					if ((Helper.GetCurrQuestion(categoryPartsButtons[i].Category, i) == GameConstants.PerPartQuestions) &&
						(Helper.GetCategoryState(categoryPartsButtons[i].Category, i + 1) == CategoryButton.State.Locked))
					{
						//Helper.SetCategoryLocked(categoryPartsButtons[i + 1].Category, i, false);
						Helper.SetCategoryState(categoryPartsButtons[i + 1].Category, i, CategoryButton.State.Unlocked);
						SocialManager.I.SetProgressToReport();
						categoryPartsButtons[i + 1].ButtonState = CategoryButton.State.Unlocked;
						break;
					}

					
				}
			}

			if (opened == 0)
			{
				categoryPartsButtons[0].ButtonState = CategoryButton.State.Unlocked;
				Helper.SetCategoryState(button.Category, 0, CategoryButton.State.Unlocked);
			}
		}
		else if (button.ButtonState == CategoryButton.State.ReadyToGet)
		{
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				//gameObject.SetActive(false);
				Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
			}
			else
			{
				button.ButtonState = CategoryButton.State.Unlocked;
				Helper.SetCategoryState(button.Category, -1, CategoryButton.State.Unlocked);
				SocialManager.I.SetProgressToReport();
				SortCategories();
			}
		}
		else
		{
			categoryBuyRefinement.Show(button);
		}

		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
	}

	public void BlockInterface() => blocker.SetActive(true);
	public void UnlockInterface() => blocker.SetActive(false);

	private void OnCategoryPartClick(CategoryButton button)
	{
		if (button.ButtonState == CategoryButton.State.Unlocked)
		{
			gameForm.Questions = button.Questions;
			gameForm.Category = button.Category;
			gameForm.CategorySprite = button.CategorySprite;
			gameForm.Part = button.Part;

			gameObject.SetActive(false);
			game.SetActive(true);

			Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, true);
			Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayMusic, $"music{button.Number}");
		}
		else
		{
			categoryBuyRefinement.Show(button);
		}

		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
	}

	private void Start()
	{
		float y = 0;
		float spacing = 20f;

		appmetrica = AppMetrica.Instance;

		categoriesHeader = objects["categoriesHeader"].GetComponent<Text>();
		balanceText = objects["balance"].GetComponent<Text>();
		blocker = objects["blocker"];
		blocker.SetActive(false);

		bank = Bank.I.gameObject;
		lobby = Lobby.I.gameObject;
		game = Game.I.gameObject;
		gameForm = Game.I;
		categoryBuyRefinement = CategoryBuyRefinement.I;

		if (categoriesScrollHolder == null) categoriesScrollHolder = objects["categoriesScrollHolder"];

		GameObject scrollView = new GameObject();
		scrollView.name = "categoriesScrollView";
		scrollView.transform.SetParent(categoriesScrollHolder.transform);
		RectTransform scrollRectTransform = scrollView.AddComponent<RectTransform>();
		Rect holderRect = categoriesScrollHolder.GetComponent<RectTransform>().rect;
		scrollRectTransform.sizeDelta = new Vector2(holderRect.width, holderRect.height);
		scrollRectTransform.pivot = new Vector2(0f, 1f);
		scrollRectTransform.anchorMax = scrollRectTransform.anchorMin = new Vector2(0, 1f);
		scrollRectTransform.anchoredPosition = new Vector2(0f, 0f);
		ScrollRect scrollRect = scrollView.AddComponent<ScrollRect>();
		scrollRect.movementType = ScrollRect.MovementType.Clamped;
		scrollRectTransform.anchorMin = new Vector2(0f, 0f);
		scrollRectTransform.anchorMax = new Vector2(1f, 1f);
		scrollRectTransform.offsetMin = scrollRectTransform.offsetMax = new Vector2(0f, 0f);

		GameObject viewport = new GameObject();
		viewport.name = "viewport";
		viewport.transform.SetParent(scrollView.transform);
		RectTransform viewportRectTransform = viewport.AddComponent<RectTransform>();
		Rect scrollRectangle = scrollRectTransform.rect;
		viewportRectTransform.sizeDelta = new Vector2(scrollRectangle.width, scrollRectangle.height);
		viewportRectTransform.pivot = new Vector2(0f, 1f);
		viewportRectTransform.anchorMax = viewportRectTransform.anchorMin = new Vector2(0, 1f);
		viewportRectTransform.anchoredPosition = new Vector2(0f, 0f);
		viewportRectTransform.anchorMin = new Vector2(0f, 0f);
		viewportRectTransform.anchorMax = new Vector2(1f, 1f);
		viewportRectTransform.offsetMin = viewportRectTransform.offsetMax = new Vector2(0f, 0f);

		Image maskGraphics = scrollView.AddComponent<Image>();
		Mask mask = scrollView.AddComponent<Mask>();
		maskGraphics.sprite = Resources.Load<Sprite>("UnpackedSprites/mask");
		mask.showMaskGraphic = false;

		Sprite sprite = URPNGLoader.LoadURPNG("categories-sprite", 0);
		GameObject content = new GameObject();
		content.name = "content";
		content.transform.SetParent(viewport.transform);
		RectTransform contentRectTransform = content.AddComponent<RectTransform>();
		contentRectTransform.pivot = new Vector2(0f, 1f);

		Tuple<Dictionary<string, Question[]>, Dictionary<string, (string, int)>> tuple = Helper.LoadCategories();
		categories = tuple.Item1;
		descriptions = tuple.Item2;

		Sprite[] atlas = new Sprite[24];
		for (int i = 0; i < atlas.Length; i++)
		{
			atlas[i] = URPNGLoader.LoadURPNG("categories-sprite", i);
		}

		contentRectTransform.sizeDelta = new Vector2(viewportRectTransform.rect.width, (sprite.rect.height + spacing) * categories.Count);
		contentRectTransform.pivot = new Vector2(0f, 1f);
		contentRectTransform.anchorMax = contentRectTransform.anchorMin = new Vector2(0, 1f);
		contentRectTransform.anchoredPosition = new Vector2(0f, 0f);
		contentRectTransform.anchorMin = new Vector2(0f, 1f);
		contentRectTransform.anchorMax = new Vector2(1f, 1f);


		contentRectTransform.offsetMin = contentRectTransform.offsetMax = new Vector2(0f, 0f);
		//contentRectTransform.rect.Set(0f, 0f, viewportRectTransform.rect.width, (sprite.rect.height + spacing) * categories.Count);
		contentRectTransform.sizeDelta = new Vector2(0f, (sprite.rect.height + spacing) * categories.Count);

		Rect contentRect = contentRectTransform.rect;
		Vector2 contentSizeDelta = new Vector2(contentRectTransform.rect.width, contentRectTransform.rect.height);

		float xp0, yp0;

		xp0 = contentRectTransform.pivot.x * contentSizeDelta.x;
		yp0 = contentRectTransform.pivot.y * contentSizeDelta.y;

		float margin = (contentRectTransform.rect.width - sprite.rect.width) / 2f;
		float step = sprite.rect.width;
		GameObject categoryButtonPrefab = Resources.Load<GameObject>("Prefabs/CategoryButtonPrefab");
		categoryButtons = new CategoryButton[descriptions.Count];
		yPositions = new float[descriptions.Count];
		int count = 0;

		List<(string, string, int)> sortedDescrs = new List<(string, string, int)>();
		foreach (KeyValuePair<string, (string, int)> pair in descriptions)
		{
			int total = 0;
			for (int i = 0; i < GameConstants.CategoryParts; i++) total += Helper.GetCurrQuestion(pair.Key, i);
			if (total == GameConstants.PerCategoryQuestions)
			{
				sortedDescrs.Add((pair.Key, pair.Value.Item1, pair.Value.Item2));
			}
		}

		foreach (KeyValuePair<string, (string, int)> pair in descriptions)
		{
			if (Helper.GetCategoryState(pair.Key, -1) != CategoryButton.State.Unlocked) continue;
			var catTuple = (pair.Key, pair.Value.Item1, pair.Value.Item2);
			if (!sortedDescrs.Contains(catTuple))
				sortedDescrs.Add(catTuple);
		}

		foreach (KeyValuePair<string, (string, int)> pair in descriptions)
		{
			if (Helper.GetCategoryState(pair.Key, -1) != CategoryButton.State.ReadyToGet) continue;
			var catTuple = (pair.Key, pair.Value.Item1, pair.Value.Item2);
			if (!sortedDescrs.Contains(catTuple))
				sortedDescrs.Add(catTuple);
		}

		foreach (KeyValuePair<string, (string, int)> pair in descriptions)
		{
			if (Helper.GetCategoryState(pair.Key, -1) == CategoryButton.State.Locked)
				sortedDescrs.Add((pair.Key, pair.Value.Item1, pair.Value.Item2));
		}

		foreach ((string, string, int) catTuple in sortedDescrs)
		{
			sprite = atlas[catTuple.Item3];
			GameObject holder = new GameObject();
			holder.name = $"Holder_{catTuple.Item1}";
			holder.transform.SetParent(content.transform);
			RectTransform holderRectTransform = holder.AddComponent<RectTransform>();
			holderRectTransform.sizeDelta = new Vector2(sprite.rect.width, sprite.rect.height);
			holderRectTransform.pivot = new Vector2(0f, 1f);
			holderRectTransform.anchorMax = holderRectTransform.anchorMin = new Vector2(0f, 1f);
			holderRectTransform.anchoredPosition = new Vector2(margin, -y);

			GameObject category = Instantiate(categoryButtonPrefab, holder.transform);
			category.name = catTuple.Item1;
			category.transform.SetParent(holder.transform);
			category.GetComponent<Image>().sprite = sprite;
			RectTransform categoryRectTransform = category.GetComponent<RectTransform>();
			categoryRectTransform.pivot = new Vector2(0f, 1f);
			categoryRectTransform.anchorMin = new Vector2(0f, 0f);
			categoryRectTransform.anchorMax = new Vector2(1f, 1f);
			categoryRectTransform.offsetMin = categoryRectTransform.offsetMax = new Vector2(0f, 0f);


			category.transform.GetChild(1).GetComponent<Text>().text = LocalizationManager.GetString("categorySelection", catTuple.Item1);

			Transform pgTransform = category.transform.GetChild(0);
			Transform bgTransform = pgTransform.transform.GetChild(0);
			bgTransform.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("progress-bar", 0);
			bgTransform.GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("progress-bar", 1);
			pgTransform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("sprite_1", 9);
			pgTransform.GetChild(2).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("finish", 0);
			y += sprite.rect.height + spacing;



			CategoryButton categoryButton = category.GetComponent<CategoryButton>();
			categoryButton.Prodid = catTuple.Item2;
			categoryButton.Category = catTuple.Item1;
			categoryButton.Part = -1;
			categoryButton.UpdatePartsDone();

			categoryButton.OnClickHandler = OnCategoryClick;
			int idx = atlas.Length / 2 + catTuple.Item3;
			categoryButton.CategorySprite = atlas[idx];

			int total = 0;
			for (int i = 0; i < GameConstants.CategoryParts; i++) total += Helper.GetCurrQuestion(catTuple.Item1, i);
			categoryButton.SetProgress(total, GameConstants.PerCategoryQuestions);


			Vector3 localPosition = holder.transform.localPosition;
			Vector2 sizeDelta = holderRectTransform.sizeDelta;
			float ymin = (localPosition.y - holderRectTransform.pivot.y * sizeDelta.y + yp0) / contentSizeDelta.y;
			float ymax = (localPosition.y - holderRectTransform.pivot.y * sizeDelta.y + yp0 + sizeDelta.y) / contentSizeDelta.y;
			float xmin = (localPosition.x - holderRectTransform.pivot.x * sizeDelta.x + xp0) / contentSizeDelta.x;
			float xmax = (localPosition.x - holderRectTransform.pivot.x * sizeDelta.x + xp0 + sizeDelta.x) / contentSizeDelta.x;

			holderRectTransform.offsetMin = holderRectTransform.offsetMax = new Vector2(0f, 0f);
			holderRectTransform.anchorMin = new Vector2(xmin, ymin);
			holderRectTransform.anchorMax = new Vector2(xmax, ymax);


			AspectRatioFitter arf = category.AddComponent<AspectRatioFitter>();
			arf.aspectRatio = sizeDelta.x / sizeDelta.y;
			arf.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
			categoryButton.Number = count % 4 + 1;
			categoryButtons[count++] = categoryButton;
		}

		int opened = 0;
		foreach (var cb in categoryButtons)
		{
			if (cb.Prodid == "")
			{
				cb.ButtonState = CategoryButton.State.Unlocked;
				Helper.SetCategoryState(cb.Category, -1, CategoryButton.State.Unlocked);
			}
			else
			{
				cb.ButtonState = Helper.GetCategoryState(cb.Category, -1);
			}
			/*if (!Helper.IsCategoryLocked(cb.Category, -1))
			{
				cb.Locked = false;
				opened++;
			}
			else
			{
				cb.Locked = true;
			}*/
		}

		/*if (opened == 0)
		{
			for (int i = 0; i < GameConstants.OpenedCategoriesByDefault; i++)
			{
				Helper.SetCategoryLocked(categoryButtons[i].Category, -1, false);
				categoryButtons[i].Locked = false;
			}
		}*/

		scrollRect.content = contentRectTransform;

		categoriesPartsHolder = objects["categoriesPartsHolder"];
		RectTransform categoriesPartsRectTransform = categoriesPartsHolder.GetComponent<RectTransform>();
		Rect categoriesPartsRect = categoriesPartsRectTransform.rect;
		Vector2 categoriesPartsSizeDelta = new Vector2(categoriesPartsRect.width, categoriesPartsRect.height);

		y = 0;
		xp0 = categoriesPartsRectTransform.pivot.x * categoriesPartsSizeDelta.x;
		yp0 = categoriesPartsRectTransform.pivot.y * categoriesPartsSizeDelta.y;
		for (int i = 0; i < 4; i++)
		{
			sprite = atlas[0];
			GameObject holder = new GameObject();
			holder.name = $"Part {i}";
			holder.transform.SetParent(categoriesPartsHolder.transform);
			RectTransform holderRectTransform = holder.AddComponent<RectTransform>();
			holderRectTransform.sizeDelta = new Vector2(sprite.rect.width, sprite.rect.height);
			holderRectTransform.pivot = new Vector2(0f, 1f);
			holderRectTransform.anchorMax = holderRectTransform.anchorMin = new Vector2(0f, 1f);
			holderRectTransform.anchoredPosition = new Vector2(margin, -y);

			GameObject category = Instantiate(categoryButtonPrefab, holder.transform);
			category.name = "PARTS";
			category.transform.SetParent(holder.transform);
			category.GetComponent<Image>().sprite = sprite;
			RectTransform categoryRectTransform = category.GetComponent<RectTransform>();
			categoryRectTransform.pivot = new Vector2(0f, 1f);
			categoryRectTransform.anchorMin = new Vector2(0f, 0f);
			categoryRectTransform.anchorMax = new Vector2(1f, 1f);
			categoryRectTransform.offsetMin = categoryRectTransform.offsetMax = new Vector2(0f, 0f);


			//	category.transform.GetChild(1).GetComponent<Text>().text = LocalizationManager.GetString("categorySelection", pair.Key);

			Transform pgTransform = category.transform.GetChild(0);
			Transform bgTransform = pgTransform.transform.GetChild(0);
			bgTransform.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("progress-bar", 0);
			bgTransform.GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("progress-bar", 1);
			pgTransform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("sprite_1", 9);
			pgTransform.GetChild(2).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("finish", 0);
			y += sprite.rect.height + spacing;



			CategoryButton categoryButton = category.GetComponent<CategoryButton>();
			categoryButton.OnClickHandler = OnCategoryPartClick;
			categoryButton.IsPart = true;
			//	categoryButton.Category = pair.Key;
			//categoryButton.Questions = pair.Value;
			//int idx = atlas.Length / 2 + sprites[pair.Key];
			//			categoryButton.CategorySprite = atlas[idx];

			//int currQuestion = Helper.GetCurrQuestion(pair.Key);
			//	categoryButton.SetProgress(currQuestion, pair.Value.Length);


			Vector3 localPosition = holder.transform.localPosition;
			Vector2 sizeDelta = holderRectTransform.sizeDelta;
			float ymin = (localPosition.y - holderRectTransform.pivot.y * sizeDelta.y + yp0) / categoriesPartsSizeDelta.y;
			float ymax = (localPosition.y - holderRectTransform.pivot.y * sizeDelta.y + yp0 + sizeDelta.y) / categoriesPartsSizeDelta.y;
			float xmin = (localPosition.x - holderRectTransform.pivot.x * sizeDelta.x + xp0) / categoriesPartsSizeDelta.x;
			float xmax = (localPosition.x - holderRectTransform.pivot.x * sizeDelta.x + xp0 + sizeDelta.x) / categoriesPartsSizeDelta.x;

			holderRectTransform.offsetMin = holderRectTransform.offsetMax = new Vector2(0f, 0f);
			holderRectTransform.anchorMin = new Vector2(xmin, ymin);
			holderRectTransform.anchorMax = new Vector2(xmax, ymax);


			AspectRatioFitter arf = category.AddComponent<AspectRatioFitter>();
			arf.aspectRatio = sizeDelta.x / sizeDelta.y;
			arf.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
			categoryButton.Number = count % 4 + 1;
			categoryPartsButtons[i] = categoryButton;
		}

		Started = true;
	}
}
