using UnityEngine;

public class UIAudioServer : MonoBehaviour
{
	private AudioSource source;
	private bool active;
	private Subscription<bool, MessangerEvents>[] boolSubsctiptions;
	private Subscription<string, MessangerEvents>[] stringSubsctiptions;

	private void OnPlayUI(string clip)
	{
		if (!Helper.IsSoundActive() || !active) return;

        AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{clip}");
		source.clip = audioClip;
		source.Play();
	}

	private void OnSetSoundActive(bool active) => this.active = active;

	private void Awake()
	{
		stringSubsctiptions = new Subscription<string, MessangerEvents>[]
		{
			Messanger<string, MessangerEvents>.Subscribe(MessangerEvents.PlayUI, OnPlayUI)
		};
		boolSubsctiptions = new Subscription<bool, MessangerEvents>[]
		{
			Messanger<bool, MessangerEvents>.Subscribe(MessangerEvents.SetSoundActive, OnSetSoundActive)
		};
	}

	private void OnDestroy()
	{
		foreach (var s in boolSubsctiptions)
		{
			s.Unsubscribe();
		}
		foreach (var s in stringSubsctiptions)
		{
			s.Unsubscribe();
		}
	}

	private void Start()
	{
		source = GetComponent<AudioSource>();
        active = Helper.IsSoundActive();
	}
}
