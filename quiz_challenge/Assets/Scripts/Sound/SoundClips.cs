﻿public static class SoundClips
{
	public const string UITap = "ui_tap";
	public const string AnswerChoice = "answer_choice";
	public const string Hints = "hints";
	public const string Win = "win";
	public const string Lose = "lose";
	public const string CollectCoins = "coins";
    public const string Lobby = "lobby";
}