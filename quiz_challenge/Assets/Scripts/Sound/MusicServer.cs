﻿using UnityEngine;

public class MusicServer : MonoBehaviour
{
    AudioSource source;
    bool active;
    Subscription<bool, MessangerEvents> boolSubscription;
    Subscription<string, MessangerEvents> stringSubscription;

    private void OnSetMusicActive(bool active)
    {
        this.active = active;

        if (Helper.IsMusicActive() && active) source.Play();
        else source.Stop();
    }

    private void OnPlayMusic(string clip)
    {
        AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{clip}");
        source.clip = audioClip;

        if (!Helper.IsMusicActive()) return;

        source.Play();
    }

    private void Awake()
    {
        boolSubscription = Messanger<bool, MessangerEvents>.Subscribe(MessangerEvents.SetMusicActive, OnSetMusicActive);
        stringSubscription = Messanger<string, MessangerEvents>.Subscribe(MessangerEvents.PlayMusic, OnPlayMusic);
    }

    private void OnDestroy()
    {
        boolSubscription.Unsubscribe();
        stringSubscription.Unsubscribe();
    }

    void Start()
    {
        source = GetComponent<AudioSource>();
        source.clip = Resources.Load<AudioClip>($"Audio/music");
        active = Helper.IsMusicActive();
    }
}