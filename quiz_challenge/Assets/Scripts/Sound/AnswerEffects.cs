﻿using UnityEngine;

public class AnswerEffects : MonoBehaviour
{
	private AudioSource source;
	private bool active;
	private Subscription<string, MessangerEvents>[] stringSubscriptions;
	private Subscription<bool, MessangerEvents>[] boolSubscriptions;

	private void OnPlayAnswerEffect(string clip)
	{
		if (!Helper.IsSoundActive() || !active) return;

		AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{clip}");
		source.clip = audioClip;
		source.Play();
	}

	private void OnSetSoundActive(bool active) => this.active = active;

	private void Awake()
	{
		stringSubscriptions = new Subscription<string, MessangerEvents>[]
		{
			Messanger<string, MessangerEvents>.Subscribe(MessangerEvents.PlayAnswerEffect, OnPlayAnswerEffect)
		};
		boolSubscriptions = new Subscription<bool, MessangerEvents>[] 
		{
			Messanger<bool, MessangerEvents>.Subscribe(MessangerEvents.SetSoundActive, OnSetSoundActive)
		};
	}

	private void OnDestroy()
	{
		foreach (var s in stringSubscriptions)
		{
			s.Unsubscribe();
		}
		foreach (var s in boolSubscriptions)
		{
			s.Unsubscribe();
		}
	}

	private void Start()
	{
		source = GetComponent<AudioSource>();
        active = Helper.IsSoundActive();
	}
}
