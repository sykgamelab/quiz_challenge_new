﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[ExecuteInEditMode]
public class URPNGSprite : MonoBehaviour
{
	public string layout = "";
	public int index = 0;

	// Use this for initialization
	private void Start()
	{
		if (layout != "")
		{
			Sprite sprite = URPNGLoader.LoadURPNG(layout, index);
			GetComponent<Image>().sprite = sprite;
		}
	}

#if UNITY_EDITOR
	private void Update()
	{
		if (!Application.isPlaying)
		{
			Sprite sprite = URPNGLoader.LoadURPNG(layout, index);
			GetComponent<Image>().sprite = sprite;
			if (GetComponent<Image>().sprite == null)
			{
				URPNGLoader.Clear();
			}
		}
	}
#endif

	public void SetSprite(string layout, int index)
	{
		this.layout = layout;
		this.index = index;
		Sprite sprite = URPNGLoader.LoadURPNG(layout, index);
		GetComponent<Image>().sprite = sprite;
	}
}
