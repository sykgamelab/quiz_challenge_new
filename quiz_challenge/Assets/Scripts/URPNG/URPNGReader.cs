﻿using System;
using System.IO;
using UnityEngine;



public class UrmobiPNGReadException : Exception
{
	public UrmobiPNGReadException(string message) : base(message) { }
}

internal static class URPNGReader
{
	public struct Selection
	{
		public int num, x, y, w, h, bx, by, bz, bw;
	}

	public static Tuple<Selection[], Sprite[]> ReadURPNG(Stream stream)
	{
		using (BinaryReader br = new BinaryReader(stream))
		{
			int count = br.ReadInt32();
			Selection[] selections = new Selection[count];
			for (int i = 0; i < count; i++)
			{
				selections[i] = new Selection
				{
					num = br.ReadInt32(),
					x = br.ReadInt32(),
					y = br.ReadInt32(),
					w = br.ReadInt32(),
					h = br.ReadInt32(),
					bx = br.ReadInt32(),
					by = br.ReadInt32(),
					bz = br.ReadInt32(),
					bw = br.ReadInt32()
				};
				int crc = br.ReadInt32();
			}

			int chunkSize = 32768;
			byte[] data = new byte[chunkSize];


			count = 0;
			int read = 0;


			while ((read = br.Read(data, count, chunkSize)) > 0)
			{
				count += read;
				if (chunkSize + count > data.Length)
				{
					Array.Resize(ref data, data.Length * 2);
				}
			}

			Array.Resize(ref data, count);
			Texture2D mainTex = new Texture2D(1, 1);
			mainTex.LoadImage(data);

			Sprite[] sprites = new Sprite[count];
			if (selections.Length > 0)
			{
				sprites = new Sprite[selections.Length];
				for (int i = 0; i < selections.Length; i++)
				{
					Selection s = selections[i];

					Vector4 border;
					if (s.bx != 0 || s.by != 0 || s.bz != 0 || s.bw != 0)
					{
						border = new Vector4(s.bx, s.by, s.bz, s.bw);
					}
					else
					{
						border = Vector4.zero;
					}

					sprites[i] = Sprite.Create(mainTex, new Rect(s.x, mainTex.height - s.y - s.h, s.w, s.h), new Vector2(0.5f, 0.5f), 1000.0f, 1, SpriteMeshType.FullRect, border);
				}
			}
			else
			{
				sprites = new Sprite[1];
				sprites[0] = Sprite.Create(mainTex, new Rect(0.0f, 0.0f, mainTex.width, mainTex.height), new Vector2(0.5f, 0.5f), 1000.0f, 1, SpriteMeshType.FullRect);
			}

			return new Tuple<Selection[], Sprite[]>(selections,sprites);
		}
	}
}