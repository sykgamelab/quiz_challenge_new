﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question
{
	public Question()
	{
		Answers = new List<string>();
	}

	public int Correct { set; get; }
	public string Text { set; get; }
	public string Category { get; set; }

	public void AddAnswer(string answer)
	{
		Answers.Add(answer);
	}

	public List<string> Answers { get; private set; }
}