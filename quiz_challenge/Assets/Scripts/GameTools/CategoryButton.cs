﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CategoryButton : MonoBehaviour {
	public enum State { Locked, ReadyToGet, Unlocked }

	GameObject categories;
	Game gameForm;
	CustomFillSlider slider;
	private bool isPart/*, locked = true*/;

	[SerializeField] private Text partsDone;
	[SerializeField] private Image lockImg;
	[SerializeField] private GameObject progressBar;
	[SerializeField] private Image getBullet;
	[SerializeField] private Text getText;

	public Question[] Questions { get; set; } = null;
	public string Category { get; set; } = null;
	public Sprite CategorySprite { get; set; } = null;
    public int Number;
	public Action<CategoryButton> OnClickHandler { get; set; } = null;
	public int Part { get; set; }
	public string Prodid { get; set; }
	private State state = State.Locked;

	public State ButtonState
	{
		get => state;
		set
		{
			state = value;
			if (state == State.Locked)
			{
				progressBar.SetActive(false);
				partsDone.gameObject.SetActive(false);
				getBullet.gameObject.SetActive(false);
				lockImg.gameObject.SetActive(true);
			}
			else if (state == State.ReadyToGet)
			{
				progressBar.SetActive(false);
				partsDone.gameObject.SetActive(false);
				getBullet.gameObject.SetActive(true);
				lockImg.gameObject.SetActive(false);
			}
			else
			{
				progressBar.SetActive(true);
				if (!isPart) partsDone.gameObject.SetActive(true);
				else partsDone.gameObject.SetActive(false);
				getBullet.gameObject.SetActive(false);
				lockImg.gameObject.SetActive(false);
			}
		}
	}

	/*public bool Locked
	{
		get => locked;
		set
		{
			locked = value;
			progressBar.SetActive(!locked);
			if (!isPart) partsDone.gameObject.SetActive(!locked);
			lockImg.gameObject.SetActive(locked);
		}
	}*/

	public bool IsPart
	{
		get => isPart;
		set
		{
			isPart = value;
			if (!isPart || state != State.Unlocked) partsDone.gameObject.SetActive(false);
			else if (isPart) partsDone.gameObject.SetActive(true);
		}
	}

	

	private void OnClick()
	{
		OnClickHandler?.Invoke(this);
	/*	gameForm.Questions = Questions;
		gameForm.Category = Category;
		gameForm.CategorySprite = CategorySprite;

		Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayUI, SoundClips.UITap);
		categories.SetActive(false);
		gameForm.gameObject.SetActive(true);

        Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, true);
        Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayMusic, $"music{Number}");*/
    }

    private void Start()
	{
		Transform root = transform.root;
		gameForm = Game.I;
		categories = Categories.I.gameObject;
		gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
		lockImg.sprite = URPNGLoader.LoadURPNG("lock", 0);
		getBullet.sprite = URPNGLoader.LoadURPNG("download-button", 0);
		//progressBar.SetActive(false);
		//	partsDone.gameObject.SetActive(false);		
	}

	public void SetProgress(int currQuestion, int totalQuestions)
	{
		Transform pgTransform = transform.GetChild(0);
		Transform handleTrabsform = pgTransform.GetChild(1).GetChild(0);

		if (slider == null) slider = gameObject.transform.GetChild(0).GetComponent<CustomFillSlider>();

		Transform imgTransform = pgTransform.GetChild(2);
		Image finishImage = imgTransform.GetComponent<Image>();
		Button button = gameObject.GetComponent<Button>();
		if (currQuestion == totalQuestions)
		{
			slider.value = 1f;
			finishImage.sprite = URPNGLoader.LoadURPNG("sprite_1", 16);
			button.transition = Selectable.Transition.None;
			button.interactable = false;
			handleTrabsform.gameObject.SetActive(false);
			return;
		}
		else
		{
			handleTrabsform.gameObject.SetActive(true);
			finishImage.sprite = URPNGLoader.LoadURPNG("finish", 0);
			button.interactable = true;
			button.transition = Selectable.Transition.ColorTint;
		}

		float progress = (float)currQuestion / totalQuestions;
		slider.value = progress;
		handleTrabsform.GetChild(1).GetComponent<Text>().text = (int)(progress * 100.0f) + "%";
	}

	public void UpdatePartsDone()
	{
		int done = 0;
		if (Category != null && !isPart)
		{
			for (int i = 0; i < GameConstants.CategoryParts; i++)
			{
				if (Helper.GetCurrQuestion(Category, i) == GameConstants.PerPartQuestions)
				{
					done++;
				}
			}
		}

		partsDone.text = $"{done.ToString()}/4";
	}

	private void OnEnable()
	{
		getText.text = LocalizationManager.GetString("additional", "CategoryGet").ToUpper();
	}
}
