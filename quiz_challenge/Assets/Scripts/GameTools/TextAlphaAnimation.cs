﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextAlphaAnimation : MonoBehaviour
{
	private bool doAlpha = false;
	private float speed, elapsed;
	private Text text;

	public float Target { get; set; }
	public float T { get; set; }
	public Action OnComplete { get; set; }
	

	public void DoAlpha()
	{
		if (!doAlpha)
		{
			text = GetComponent<Text>();
			Color cl = text.color;
			doAlpha = true;
			speed = (Target - cl.a) / T;
			elapsed = 0.0f;
		}
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (doAlpha)
		{
			float d = Time.deltaTime;
			elapsed += d;
			Color cl = text.color;
			float a = cl.a + d * speed;
			text.color = new Color(cl.r, cl.g, cl.b, a);
			if (elapsed >= T)
			{
				doAlpha = false;
				OnComplete?.Invoke();
			}
		}
	}
}
