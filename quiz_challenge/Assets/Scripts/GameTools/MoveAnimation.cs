﻿using System;
using UnityEngine;

public class MoveAnimation : MonoBehaviour {
	private bool doMove  = false;
	private float speedX, speedY, elapsed;

	public void DoMove()
	{
		if (!doMove)
		{
			doMove = true;
			speedX = (Target.x - transform.position.x) / T;
			speedY = (Target.y - transform.position.y) / T;
			elapsed = 0.0f;
		}
	}

	public Vector3 Target { get; set; }
	public float T{ get; set; }
	public Action OnComplete { get; set; }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (doMove)
		{
			float d = Time.deltaTime;
			elapsed += d;
			float x = transform.position.x + d * speedX;
			float y = transform.position.y + d * speedY;
			transform.position = new Vector3(x, y, transform.position.z);
			if (elapsed >= T)
			{
				doMove = false;
				OnComplete?.Invoke();
			}
		}
	}
}
