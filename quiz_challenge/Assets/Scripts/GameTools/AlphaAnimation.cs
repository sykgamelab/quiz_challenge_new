﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AlphaAnimation : MonoBehaviour {
	private Image image;
	private bool doAlpha = false;
	private float speed, elapsed;

	public float Target { get; set; }
	public float T { get; set; }
	public Action OnComplete { get; set; }

	public void DoAlpha()
	{
		if (!doAlpha)
		{
			image = GetComponent<Image>();
			Color cl = image.color;
			doAlpha = true;
			speed = (Target - cl.a) / T;
			elapsed = 0.0f;
		}
	}
	
	// Use this for initialization
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
		if (doAlpha)
		{
			float d = Time.deltaTime;
			elapsed += d;
			Color cl = image.color;
			float a =cl.a + d * speed;
			image.color = new Color(cl.r, cl.g, cl.b, a);
			if (elapsed >= T)
			{
				doAlpha = false;
				OnComplete?.Invoke();
			}
		}
	}
}
