﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

public enum HintType
{
	FiftyFifty,
	HallAssistance,
	FriendAssistance
}

public enum HintPrice
{
	FiftyFifty = 4,
	HallAssistance = 5,
	FriendAssistance = 0
}

public static class GameConstants
{
	public const int DefaultBalance = 0;
	//public const int DefaultBalance = 5;
	public const int CorrectAnswerReward = 1;
	public const int WrongAnswerFine = 10;
	public const int CategoryPartReward = 25;
	public const float AlphaMax = 1.0f;
	public const float AlphaMin = 0.7f;

	public const float CoinMoveTime = 0.815f;
	public const float AnswerCheckTime = 1.25f;
	public const float FixTime = 3.2f;
    public const float FakeAnswerButtonMoveTime = 0.5f;

	/*public const float CoinMoveTime = 0.01f;
	public const float AnswerCheckTime = 0.1f;
	public const float FixTime = 0.1f;
	public const float FakeAnswerButtonMoveTime = 0.1f;*/

	public const int SecondsRestoreFBFriends = 86400;
	public const int CategoryParts = 4;
	public const int PerCategoryQuestions = 100;
	public const int PerPartQuestions = 25;
	public const int OpenedCategoriesByDefault = 3;
	public const int PartPrice = 10;
}

public static class ServerConstants
{
    public const string UserID = "userID";
    public const string Progress = "progress";
    public const string Timestamp = "timestamp";
	public const string Url = "https://37.140.198.128:8080/";
	//public const string Url = "https://192.168.1.60:8080/";
	//  public const string Url = "https://127.0.0.1:8080/";
	//public const string Url = "https://192.168.0.105:8080/";
	public const string UriSetProgress = "QuizChallenge/SetProgress";
    public const string UriGetProgress = "QuizChallenge/GetProgress";
    public const string UriFirebaseAuth = "QuizChallenge/Auth";
    public const string UriAuthIOS = "QuizChallenge/AuthIOS";
    public const string FirebaseToken = "firebaseToken";
    public const string Ticket = "ticket";
    public const string ExpirationTime = "expirationTime";
    public const int RequestRepeatFirebaseAuthMilliseconds = 30000;
    public const int RequestRepeatGetProgressMilliseconds = 30000;
	public const string Token = "token";
	public const string UriAuthAndroid = "QuizChallenge/AuthAndroid";
	public const string UriAuthTest = "QuizChallenge/AuthTest";
	public const int Timeout = 10000;
}

public static class AdsConstants
{
#if UNITY_ANDROID
	public const string AppID = "ca-app-pub-4848661661926858~4965027936";
	public const string InterstitialNoVideo = "ca-app-pub-4848661661926858/2898101948";
	public const string InterstitialVideo = "ca-app-pub-4848661661926858/4331259650";
#elif UNITY_IOS
	public const string AppID = "ca-app-pub-4848661661926858~9144602639";
	public const string InterstitialNoVideo = "ca-app-pub-4848661661926858/4822214245";
	public const string InterstitialVideo = "ca-app-pub-4848661661926858/7065234205";
#endif
}

public static class MetricaConstants
{
	public const string CorrectAnswer = "correct_answer";
	public const string HintUsed = "hint_used";
	public const string Category = "category";
	public const string Part = "part";
	public const string Question = "question";
	public const string PartDone = "part_done";
	public const string CategoryDone = "category_done";
	public const string CategoryPartBought = "category_part_bought";
	public const string CategoryBought = "category_bought";
	public const string CoinsBought = "coins_bought";
	public const string Amount = "amount";
	public const string HintType = "hint_type";
	public const string Session = "session";
	public const string Duration = "duration";
	public const string Time = "time";
	public const string GameDone = "game_done";
}


public static class SocialConstants
{
#if UNITY_IOS
    public const string CoinsTable = "coins_table";
	public static readonly ReadOnlyDictionary<string, string> CategoriesAchievementsIDs = new ReadOnlyDictionary<string, string>
	(
		new Dictionary<string, string>()
		{
				{ "Sport", "sport_achievement" },
				{ "Games", "games_achievement" },
				{ "History", "history_achievement" },
				{ "Movie&Cartoon", "movie_cartoon_achievement" },
				{ "Personality", "personality_achievement" },
				{ "Nature", "nature_achievement" },
				{ "Countries", "countries_achievement" },
				{ "Hi-tech", "hi_tech_achievement" },
				{ "Religion", "religion_achievement" },
				{ "Literature", "literature_achievement" },
				{ "Music", "music_achievement" },
				{ "Cities", "cities_achievement" }
		}
	);
#elif UNITY_ANDROID
    public const string CoinsTable = "CgkIzNfdrYsUEAIQAQ";
    public static readonly ReadOnlyDictionary<string, string> CategoriesAchievementsIDs = new ReadOnlyDictionary<string, string>
    (
        new Dictionary<string, string>()
        {
            { "Sport", "CgkIzNfdrYsUEAIQBA" },
            { "Games", "CgkIzNfdrYsUEAIQBg" },
            { "History", "CgkIzNfdrYsUEAIQBw" },
            { "Movie&Cartoon", "CgkIzNfdrYsUEAIQCA" },
            { "Personality", "CgkIzNfdrYsUEAIQCQ" },
            { "Nature", "CgkIzNfdrYsUEAIQCg" },
            { "Countries", "CgkIzNfdrYsUEAIQCw" },
            { "Hi-tech", "CgkIzNfdrYsUEAIQDA" },
            { "Religion", "CgkIzNfdrYsUEAIQDQ" },
            { "Literature", "CgkIzNfdrYsUEAIQDg" },
            { "Music", "CgkIzNfdrYsUEAIQDw" },
            { "Cities", "CgkIzNfdrYsUEAIQEA" }
        }
    );
#endif
}