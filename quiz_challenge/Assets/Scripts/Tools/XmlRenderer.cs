﻿using System;
using System.Xml;
using System.Globalization;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using System.IO;
#endif

//[ExecuteInEditMode]
public class XmlRenderer : MonoBehaviour
{
	[SerializeField] private GameObject canvas;
	[SerializeField] private Camera camera;
	private bool firstUpdate = true, firstLateUpdate = true, allStarted = false;
	private static float referenceWidth, referenceHeight;
	private static List<GameObject> forms = new List<GameObject>();
	private static List<GameObject[]> formTrees = new List<GameObject[]>();
	private static List<KeyValuePair<Canvas, string>> canvasLayers = new List<KeyValuePair<Canvas, string>>();
	private static List<KeyValuePair<string, GameObject>> aligments = new List<KeyValuePair<string, GameObject>>();
	private string dirForms = "Forms/5625";
	private string formName;

#if UNITY_EDITOR
	private FileSystemWatcher[] watchers = new FileSystemWatcher[2];
	private bool reloadScene = false;
	private GameObject formObj;
#endif

	private class FormNode
	{
		public FormNode()
		{
			Children = new List<FormNode>();
		}

		public FormNode Parent { get; set; }
		public GameObject Node { get; set; }
		public List<FormNode> Children { get; set; }
	}

	private class InterfaceElement
	{
		public InterfaceElement Parent { get; set; }
		public string Tag { get; set; }

		private Dictionary<string, object> attrs;

		public InterfaceElement(string tag) : this(null, tag) { }

		public InterfaceElement(InterfaceElement parent, string tag)
		{
			Parent = parent;
			attrs = new Dictionary<string, object>();
			Children = new List<InterfaceElement>();
			Tag = tag;
		}

		public void SetAttribute(string key, object val)
		{
			if (!attrs.ContainsKey(key)) attrs.Add(key, val);
			else attrs[key] = val;
		}

		public bool ContainsAttribute(string key) => attrs.ContainsKey(key);
		public object GetAttribute(string key) => attrs[key];

		public void AddChild(InterfaceElement child) => Children.Add(child);

		public List<InterfaceElement> Children { get; set; }
	}

	private class InterfaceParser
	{
		private XmlDocument doc;
		private List<InterfaceElement> treeInList;
		private GameObject[] treeInArr;
		private List<KeyValuePair<AspectRatioFitter, AspectRatioFitter.AspectMode>> arfs
			= new List<KeyValuePair<AspectRatioFitter, AspectRatioFitter.AspectMode>>();
		//private Dictionary<string, SelectionReader.WH[]> selections;

		private Dictionary<string, string> tagToType = new Dictionary<string, string>()
				{
					{ "Button", "Button" },
					{ "Image", "Image" },
					{ "Text", "Text" }
				};

		private void ParseChildren(XmlNode node, InterfaceElement parent, float width, float height)
		{
			foreach (XmlNode child in node.ChildNodes)
			{
				ParseNode(child, parent, width, height);
			}
		}

		private void ParseAttribute(XmlAttribute attr, XmlNode node, InterfaceElement elem)
		{
			string val;
			float denumerator = 1.0f;

			switch (attr.Name)
			{
				case "name":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "width":
					val = attr.Value;
					denumerator = 1.0f;
					if (val[val.Length - 1] == '%')
					{
						val = val.Substring(0, val.Length - 1);
						elem.SetAttribute("relativeWidth", true);
						denumerator = 100f;
					}

					elem.SetAttribute(attr.Name, float.Parse(val, CultureInfo.InvariantCulture) / denumerator);
					break;
				case "height":
					val = attr.Value;
					denumerator = 1.0f;
					if (val[val.Length - 1] == '%')
					{
						val = val.Substring(0, val.Length - 1);
						elem.SetAttribute("relativeHeight", true);
						denumerator = 100f;
					}
					//elem.SetAttribute("height", Convert.ToSingle(val) / denumerator);
					elem.SetAttribute(attr.Name, float.Parse(val, CultureInfo.InvariantCulture) / denumerator);
					break;
				case "font":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "font-size":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "font-min":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "font-max":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "best-fit":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "aligment":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "sprite":
					elem.SetAttribute(attr.Name, attr.Value);
					string[] stringsInSprite = attr.Value.Split(new char[] { '/' });
					bool useNativeSize = true;
					foreach (XmlAttribute a in node.Attributes)
					{
						if (a.Name == "useNativeSize" && a.Value == "false")
						{
							useNativeSize = false;
						}
					}

					if (useNativeSize)
					{
						Sprite sp = URPNGLoader.LoadURPNG(stringsInSprite[0], Convert.ToInt32(stringsInSprite[1]));
#if UNITY_EDITOR
						if (sp == null)
						{
							URPNGLoader.Clear();
							sp = URPNGLoader.LoadURPNG(stringsInSprite[0], Convert.ToInt32(stringsInSprite[1]));
						}
#endif
						elem.SetAttribute("width", sp.rect.width);
						elem.SetAttribute("height", sp.rect.height);
					}
					break;
				case "image-type":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "components":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "onClick":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "color":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "text-aligment":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "text":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "margin-top":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "aspectRatioFitter":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "sortingOrder":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
				case "material":
					elem.SetAttribute(attr.Name, attr.Value);
					break;
			}
		}

		private void ParseNode(XmlNode node, InterfaceElement parent, float width, float height)
		{
			InterfaceElement elem = new InterfaceElement(parent, node.Name);

			if (node.NodeType == XmlNodeType.Comment) return;

			foreach (XmlAttribute attr in node.Attributes)
			{
				ParseAttribute(attr, node, elem);
			}

			if (elem.Tag == "Row")
			{
				if (!elem.ContainsAttribute("height"))
				{
					elem.SetAttribute("height", 1f / node.ParentNode.ChildNodes.Count);
					elem.SetAttribute("relativeHeight", true);
				}

				elem.SetAttribute("relativeWidth", true);
				elem.SetAttribute("width", 1.0f);
			}
			else if (elem.Tag == "Col")
			{
				if (!elem.ContainsAttribute("width"))
				{
					elem.SetAttribute("width", 1f / node.ParentNode.ChildNodes.Count);
					elem.SetAttribute("relativeWidth", true);
				}

				elem.SetAttribute("relativeHeight", true);
				elem.SetAttribute("height", 1.0f);
			}
			else if (elem.Tag == "Layer")
			{
				elem.SetAttribute("relativeWidth", true);
				elem.SetAttribute("width", 1.0f);
				elem.SetAttribute("relativeHeight", true);
				elem.SetAttribute("height", 1.0f);
			}

			//parsers[node.Name](node, elem);
			treeInList.Add(elem);
			parent.AddChild(elem);
			ParseChildren(node, elem, width, height);
		}

		public void Load(string xml)
		{
			doc = new XmlDocument();
			//doc.Load(path);
			doc.LoadXml(xml);
		}

		private void GenerateElements(Form form, InterfaceElement curr, ref int count, float width, float height, GameObject canvas, string className)
		{
			int currCount = count;
			float x = 0f, y = 0f;
			string[] stringsInSprite;
			Image img = null;
			Button btn = null;
			Text text = null;
			RectTransform rectTransform;
			RectTransform parentRectTransform;

			foreach (var elem in curr.Children)
			{
				bool anchored = false;
				GameObject parent = treeInArr[currCount - 1];
				string varName = elem.ContainsAttribute("name") ? (string)elem.GetAttribute("name") : null;

				treeInArr[count] = new GameObject();
				treeInArr[count].transform.SetParent(parent.transform);
				rectTransform = treeInArr[count].AddComponent<RectTransform>();
				parentRectTransform = parent.GetComponent<RectTransform>();

				string type = elem.Tag;
				if (tagToType.ContainsKey(elem.Tag))
				{
					switch (type)
					{
						case "Image":
							img = treeInArr[count].AddComponent<Image>();
							break;
						case "Button":
							btn = treeInArr[count].AddComponent<Button>();
							break;
						case "Text":
							text = treeInArr[count].AddComponent<Text>();
							break;
					}
				}

				if (type == "Button")
				{
					img = treeInArr[count].AddComponent<Image>();
				}

				if (varName != null) treeInArr[count].name = varName;
				else treeInArr[count].name = elem.Tag;


				float elemWidth = width;
				float elemHeight = height;

				if (elem.ContainsAttribute("width"))
				{
					bool relative = elem.ContainsAttribute("relativeWidth");
					float val = (float)elem.GetAttribute("width");
					if (relative) elemWidth *= val;
					else elemWidth = val;
				}

				if (elem.ContainsAttribute("height"))
				{
					bool relative = elem.ContainsAttribute("relativeHeight");
					float val = (float)elem.GetAttribute("height");
					if (relative) elemHeight *= val;
					else elemHeight = val;
				}

				if (elem.ContainsAttribute("sortingOrder"))
				{
					treeInArr[count].AddComponent<GraphicRaycaster>();
					Canvas childCanvas = treeInArr[count].GetComponent<Canvas>();
					childCanvas.overrideSorting = true;
					childCanvas.sortingOrder = int.Parse((string)elem.GetAttribute("sortingOrder"));
					canvasLayers.Add(new KeyValuePair<Canvas, string>(childCanvas, className));
				}

				Vector2 sizeDelta = new Vector2(elemWidth, elemHeight);
				rectTransform.sizeDelta = sizeDelta;

				if (elem.Tag == "Button" || elem.Tag == "Image")
				{
					rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
					rectTransform.anchorMax = new Vector2(0.5f, 0.5f); ;
					rectTransform.pivot = new Vector2(0f, 1f);

					if (elem.ContainsAttribute("sprite"))
					{
						stringsInSprite = elem.GetAttribute("sprite").ToString().Split('/');
						img.sprite = URPNGLoader.LoadURPNG(stringsInSprite[0], Convert.ToInt32(stringsInSprite[1]));

						if (elem.ContainsAttribute("image-type") && elem.GetAttribute("image-type").ToString() == "Tiled")
						{

						}

						rectTransform = treeInArr[count].GetComponent<RectTransform>();
						rectTransform.sizeDelta = new Vector2(elemWidth, elemHeight);
						rectTransform.anchoredPosition = new Vector2(-elemWidth / 2f, elemHeight / 2f);
						anchored = true;
						if (elem.ContainsAttribute("image-type"))
						{
							img.type = (Image.Type)Enum.Parse(typeof(Image.Type), (string)elem.GetAttribute("image-type"));
						}
					}

				}

				if (type == "Button")
				{
					if (elem.ContainsAttribute("onClick"))
					{
						string[] onClickHandlers = ((string)elem.GetAttribute("onClick")).Split(new char[] { '/' },
							StringSplitOptions.RemoveEmptyEntries);

						Type formType = form.GetType();
						MethodInfo[] methods = formType.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);

						foreach (var handler in onClickHandlers)
						{
							foreach (var m in methods)
							{
								if (m.Name == handler)
								{
									UnityEngine.Events.UnityAction action = (UnityEngine.Events.UnityAction)Delegate.CreateDelegate(typeof(UnityEngine.Events.UnityAction), form, m.Name);
									btn.onClick.AddListener(action);
								}
							}
						}
					}

					btn.targetGraphic = img;
				}

				if (elem.ContainsAttribute("components"))
				{
					string[] componentNames = ((string)elem.GetAttribute("components")).Split(new char[] { '/' },
						StringSplitOptions.RemoveEmptyEntries);
					foreach (var name in componentNames)
					{
						int idx = name.LastIndexOf(".");
						Type componentType = null;
						if (idx < 0)
						{
							componentType = Type.GetType(name);
						}
						else
						{
							string asmName = name.Substring(0, idx);
							Assembly assem = Assembly.Load(asmName);
							componentType = assem.GetType(name);
						}

						if (componentType != null) treeInArr[count].AddComponent(componentType);
					}
				}

				if ((elem.Tag == "Layer" || elem.Tag == "Row" || elem.Tag == "Col") && elem.ContainsAttribute("color"))
				{
					img = treeInArr[count].AddComponent<Image>();
				}

				if (elem.Tag == "Text")
				{
					rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
					rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
					rectTransform.pivot = new Vector2(0.5f, 0.5f);
					treeInArr[count].GetComponent<RectTransform>().localPosition =
						new Vector2(parentRectTransform.rect.width * 0.5f,
						-parentRectTransform.rect.height * 0.5f);

					text.alignment = TextAnchor.MiddleCenter;
					if (elem.ContainsAttribute("text")) text.text = (string)elem.GetAttribute("text");
					if (elem.ContainsAttribute("font")) text.font = Resources.Load<Font>($"Fonts/{(string)elem.GetAttribute("font")}");
					if (elem.ContainsAttribute("font-size")) text.fontSize = Convert.ToInt32((string)elem.GetAttribute("font-size"));
					if (elem.ContainsAttribute("text-aligment")) text.alignment = (TextAnchor)Enum.Parse(typeof(TextAnchor),
						(string)elem.GetAttribute("text-aligment"));
					if (elem.ContainsAttribute("font-min")) text.resizeTextMinSize = Convert.ToInt32((string)elem.GetAttribute("font-min"));
					if (elem.ContainsAttribute("font-max")) text.resizeTextMaxSize = Convert.ToInt32(elem.GetAttribute("font-max"));
					if (elem.ContainsAttribute("best-fit")) text.resizeTextForBestFit = Convert.ToBoolean((string)elem.GetAttribute("best-fit"));
				}

				if (elem.Tag == "Text" || elem.Tag == "Image" || elem.Tag == "Button")
				{
					if (elem.ContainsAttribute("color"))
					{
						string[] rgba = elem.GetAttribute("color").ToString().Split('/');
						float r = float.Parse(rgba[0], CultureInfo.InvariantCulture) / 255f;
						float g = float.Parse(rgba[1], CultureInfo.InvariantCulture) / 255f;
						float b = float.Parse(rgba[2], CultureInfo.InvariantCulture) / 255f;
						float a = float.Parse(rgba[3], CultureInfo.InvariantCulture) / 255f;
						if (elem.Tag == "Image" || elem.Tag == "Button") img.color = new Color(r, g, b, a);
						else text.color = new Color(r, g, b, a);

					}
				}

				if (elem.Tag == "Image" || elem.Tag == "Button")
				{
					if (elem.ContainsAttribute("material"))
					{
						treeInArr[count].GetComponent<Image>().material = Resources.Load<Material>("Materials/" + elem.GetAttribute("material").ToString());
					}
				}

				if (elem.Tag == "Row" || elem.Tag == "Col" || elem.Tag == "Layer")
				{
					rectTransform.anchorMin = new Vector2(0, 1);
					rectTransform.anchorMax = new Vector2(0, 1);
					rectTransform.pivot = new Vector2(0, 1);
				}

				if (elem.Tag == "Layer")
				{
					rectTransform.anchoredPosition = new Vector2(0, 0);
				}
				else
				{
					if (elem.Tag != "Text" && !anchored)
					{
						treeInArr[count].transform.localPosition = new Vector2(x, y); ;
					}
					else
					{
						anchored = false;
					}
				}

				if (elem.ContainsAttribute("margin-top"))
				{
					rectTransform.anchorMin = new Vector2(0f, 1f);
					rectTransform.anchorMax = new Vector2(0f, 1f);
					rectTransform.pivot = new Vector2(0f, 1f);
					rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, -30f);
				}

				if (elem.Tag == "Row") y -= elemHeight;
				else if (elem.Tag == "Col") x += elemWidth;

				Vector2 parentSizeDelta = new Vector2(parentRectTransform.rect.width, parentRectTransform.rect.height);
				Vector3 localPosition = treeInArr[count].transform.localPosition;

				float xp0, yp0;

				xp0 = parentRectTransform.pivot.x * parentSizeDelta.x;
				yp0 = parentRectTransform.pivot.y * parentSizeDelta.y;

				if (elem.Tag == "Row")
				{
					float ymin = (localPosition.y - rectTransform.pivot.y * sizeDelta.y + yp0) / parentSizeDelta.y;
					float ymax = (localPosition.y - rectTransform.pivot.y * sizeDelta.y + yp0 + sizeDelta.y) / parentSizeDelta.y;

					rectTransform.offsetMin = new Vector2(0f, 0f);
					rectTransform.offsetMax = new Vector2(0f, 0f);
					rectTransform.anchorMin = new Vector2(0f, ymin);
					rectTransform.anchorMax = new Vector2(1f, ymax);
				}
				else if (elem.Tag == "Col")
				{
					float xmin = (localPosition.x - rectTransform.pivot.x * sizeDelta.x + xp0) / parentSizeDelta.x;
					float xmax = (localPosition.x - rectTransform.pivot.x * sizeDelta.x + xp0 + sizeDelta.x) / parentSizeDelta.x;

					rectTransform.offsetMin = new Vector2(0f, 0f);
					rectTransform.offsetMax = new Vector2(0f, 0f);
					rectTransform.anchorMin = new Vector2(xmin, 0f);
					rectTransform.anchorMax = new Vector2(xmax, 1f);
				}
				else
				{

					float left, right, top, bottom;
					left = localPosition.x - rectTransform.pivot.x * sizeDelta.x + xp0;
					right = parentSizeDelta.x - left - sizeDelta.x;
					bottom = localPosition.y - rectTransform.pivot.y * sizeDelta.y + yp0;
					top = parentSizeDelta.y - bottom - sizeDelta.y;
					Vector2 offsetMin = new Vector2(left, bottom);
					Vector2 offsetMax = new Vector2(right, top);

					rectTransform.offsetMin = offsetMin;
					rectTransform.offsetMax = -offsetMax;
					rectTransform.anchorMin = new Vector2(0f, 0f);
					rectTransform.anchorMax = new Vector2(1f, 1f);
				}

				if (elem.ContainsAttribute("aspectRatioFitter"))
				{
					AspectRatioFitter aspectRatioFitter = treeInArr[count].AddComponent<AspectRatioFitter>();
					aspectRatioFitter.aspectRatio = rectTransform.rect.width / rectTransform.rect.height;
					AspectRatioFitter.AspectMode mode = (AspectRatioFitter.AspectMode)Enum.Parse(typeof(AspectRatioFitter.AspectMode),
						(string)elem.GetAttribute("aspectRatioFitter"));
					arfs.Add(new KeyValuePair<AspectRatioFitter, AspectRatioFitter.AspectMode>(aspectRatioFitter, mode));
				}

				if (elem.ContainsAttribute("aligment"))
				{
					aligments.Add(new KeyValuePair<string, GameObject>((string)elem.GetAttribute("aligment"), treeInArr[count]));
				}

				if (varName != null)
				{
					form.AddObject(varName, treeInArr[count]);
				}
				count++;
				GenerateElements(form, elem, ref count, elemWidth, elemHeight, canvas, className);
			}
		}

		private void GenerateForms(string className, InterfaceElement root, float width, float height, GameObject canvas)
		{
			treeInArr = new GameObject[treeInList.Count];
			treeInArr[0] = new GameObject();
			treeInArr[0].name = className;
			treeInArr[0].transform.SetParent(canvas.transform);
			treeInArr[0].transform.SetAsFirstSibling();
			treeInArr[0].AddComponent<GraphicRaycaster>();

			RectTransform rectTransform = treeInArr[0].GetComponent<RectTransform>();
			rectTransform.sizeDelta = new Vector2(width, height);
			rectTransform.anchorMin = new Vector2(0, 1);
			rectTransform.anchorMax = new Vector2(0, 1);
			rectTransform.pivot = new Vector2(0f, 1f);
			rectTransform.anchoredPosition = new Vector2(0, 0);

			Form form = (Form)treeInArr[0].AddComponent(Type.GetType(className));
			int count = 1;
			GenerateElements(form, root, ref count, width, height, canvas, className);

			formTrees.Add(treeInArr);
			forms.Add(treeInArr[0]);

			foreach (var pair in arfs)
			{
				pair.Key.aspectMode = pair.Value;
			}
		}

		public void Parse(GameObject canvas)
		{
			treeInList = new List<InterfaceElement>();
			XmlNode root = doc.GetElementsByTagName("Form")[0];
			string className = root.Attributes["name"].Value;

			float width = 0f, height = 0f;
			foreach (XmlAttribute attr in root.Attributes)
			{
				if (attr.Name == "width")
				{
					width = float.Parse(attr.Value, CultureInfo.InvariantCulture);
					referenceWidth = width;
				}

				if (attr.Name == "height")
				{
					height = float.Parse(attr.Value, CultureInfo.InvariantCulture);
					referenceHeight = height;
				}
			}

			InterfaceElement rootElement = new InterfaceElement(root.Name);
			treeInList.Add(rootElement);
			ParseChildren(root, rootElement, width, height);
			GenerateForms(className, rootElement, width, height, canvas);
		}

	}

#if UNITY_EDITOR
	private void OnXMLChanged(object source, FileSystemEventArgs e)
	{
		string name = e.Name;
		name = name.Substring(name.LastIndexOf("\\") + 1).Replace(".xml", "");

		reloadScene = true;
		formName = name;
	}

	private void LoadForm()
	{
		InterfaceParser parser = new InterfaceParser();
		TextAsset form = Resources.Load<TextAsset>($"{dirForms}/{formName}");

		GameObject canvas = GameObject.Find("Canvas");
		Transform transform;

		for (int i = 0; i < canvas.transform.childCount; i++)
		{
			transform = canvas.transform.GetChild(i);

			if (transform.name == formName) Destroy(transform.gameObject);

			transform.gameObject.SetActive(false);
		}

		parser.Load(form.text);
		parser.Parse(canvas);
	}
#endif

	private void OptimizeElements(FormNode parent, GameObject curr, List<GameObject> forRemove)
	{
		bool remove = true;
		FormNode newParent = parent;
		if (curr.name == "settingsButtonClose")
		{
		}
		if (curr.GetComponent<GraphicRaycaster>() == null && (curr.name == "Layer" || curr.name == "Row" || curr.name == "Col"))
		{
			for (int i = 0; i < curr.transform.childCount; i++)
			{
				GameObject child = curr.transform.GetChild(i).gameObject;
				if (child.GetComponent<GraphicRaycaster>() != null || (child.name != "Layer" && child.name != "Row" && child.name != "Col"))
				{
					remove = false;
				}
			}
		}
		else
		{
			remove = false;
		}

		if (remove)
		{
			forRemove.Add(curr);
		}
		else
		{
			newParent = new FormNode();
			newParent.Parent = parent;
			newParent.Node = curr;
			parent.Children.Add(newParent);
		}

		NoOptimization noOptFlag = curr.GetComponent<NoOptimization>();
		if (noOptFlag == null)
		{
			for (int i = 0; i < curr.transform.childCount; i++)
			{
				OptimizeElements(newParent, curr.transform.GetChild(i).gameObject, forRemove);
			}
		}
		else
		{
			Destroy(noOptFlag);
		}
	}

	private void SetFormTree(FormNode node, FormNode parent)
	{
		node.Node.transform.SetParent(parent.Node.transform, true);

		RectTransform parentRectTransform = parent.Node.GetComponent<RectTransform>();
		RectTransform rectTransform = node.Node.GetComponent<RectTransform>();
		if (rectTransform == null)
		{
			return;
		}
		Vector2 parentSizeDelta = new Vector2(parentRectTransform.rect.width, parentRectTransform.rect.height);
		Vector2 sizeDelta = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
		Vector3 localPosition = node.Node.transform.localPosition;

		float xp0, yp0;

		xp0 = parentRectTransform.pivot.x * parentSizeDelta.x;
		yp0 = parentRectTransform.pivot.y * parentSizeDelta.y;

		float ymin = (localPosition.y - rectTransform.pivot.y * sizeDelta.y + yp0) / parentSizeDelta.y;
		float ymax = (localPosition.y - rectTransform.pivot.y * sizeDelta.y + yp0 + sizeDelta.y) / parentSizeDelta.y;
		float xmin = (localPosition.x - rectTransform.pivot.x * sizeDelta.x + xp0) / parentSizeDelta.x;
		float xmax = (localPosition.x - rectTransform.pivot.x * sizeDelta.x + xp0 + sizeDelta.x) / parentSizeDelta.x;

		rectTransform.offsetMin = new Vector2(0f, 0f);
		rectTransform.offsetMax = new Vector2(0f, 0f);
		rectTransform.anchorMin = new Vector2(xmin, ymin);
		rectTransform.anchorMax = new Vector2(xmax, ymax);

		foreach (var child in node.Children)
		{
			SetFormTree(child, node);
		}
	}

	private void OptimizeForms(GameObject root)
	{
		FormNode formTree = new FormNode();
		formTree.Node = root;
		List<GameObject> forRemove = new List<GameObject>();
		for (int i = 0; i < root.transform.childCount; i++)
		{
			OptimizeElements(formTree, root.transform.GetChild(i).gameObject, forRemove);
		}

		foreach (var child in formTree.Children)
		{
			SetFormTree(child, formTree);
		}

		foreach (var obj in forRemove)
		{
			Destroy(obj);
		}
	}

	private void Start()
	{
		InterfaceParser parser = new InterfaceParser();
		TextAsset[] commonXmlFiles = Resources.LoadAll<TextAsset>(dirForms);
		int[] screenRatios = new int[] { 5625, 4615, 7500 };
		int screenRatio = (int)((float)Screen.width / Screen.height * 10000f);
		int nearestRatioIdx = 0;

		for (int i = 0; i < screenRatios.Length; i++)
		{
			if (Mathf.Abs(screenRatios[i] - screenRatio) < Mathf.Abs(screenRatios[nearestRatioIdx] - screenRatio)) nearestRatioIdx = i;
		}

		dirForms = $"Forms/{screenRatios[nearestRatioIdx]}";

#if UNITY_EDITOR
		PlayerPrefs.DeleteAll();
		for (int i = 0; i < 2; i++)
		{
			watchers[i] = new FileSystemWatcher();
			watchers[i].NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
						   | NotifyFilters.FileName | NotifyFilters.DirectoryName;

			if (i == 0) watchers[i].Path = $"Assets/Resources/Forms/5625";
			else watchers[i].Path = $"Assets/Resources/{dirForms}";

			watchers[i].Changed += new FileSystemEventHandler(OnXMLChanged);
			watchers[i].EnableRaisingEvents = true;
		}
#endif

		for (int i = 0; i < commonXmlFiles.Length; i++)
		{
			formName = commonXmlFiles[i].name;
			TextAsset customXmlFile = Resources.Load<TextAsset>($"{dirForms}/{formName}");

			if (customXmlFile != null) Debug.Log($"xml = {dirForms}/{formName}");
			else Debug.Log($"xml = Forms/5625/{formName}");

			if (customXmlFile != null) parser.Load(customXmlFile.text);
			else parser.Load(commonXmlFiles[i].text);
			parser.Parse(canvas);
		}
	}

	private void LateUpdate()
	{
		if (!allStarted)
		{
			int count = canvas.transform.childCount;
			allStarted = true;
			for (int i = 0; i < count; i++)
			{
				Form form = canvas.transform.GetChild(i).GetComponent<Form>();
				if (form != null && !form.Started)
				{
					allStarted = false;
					break;
				}
			}

			if (!allStarted) return;
		}

		if (firstLateUpdate)
		{
			foreach (var pair in canvasLayers)
			{
				pair.Key.sortingLayerName = pair.Value;
			}
			canvasLayers.Clear();

			foreach (GameObject form in forms)
			{
				RectTransform rectTransform = form.GetComponent<RectTransform>();
				rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
			}

			referenceWidth = Screen.width;
			referenceHeight = Screen.height;

			foreach (var treeInArr in formTrees)
			{
				foreach (var node in treeInArr)
				{
					if (node.GetComponent<Form>() != null)
					{
						Canvas formCanvas = treeInArr[0].GetComponent<Canvas>();
						formCanvas.overrideSorting = true;
						formCanvas.sortingLayerName = treeInArr[0].name;
						continue;
					}

					AspectRatioFitter arf = node.GetComponent<AspectRatioFitter>();
					if (arf != null && arf.aspectMode == AspectRatioFitter.AspectMode.FitInParent)
					{
						arf.aspectMode = AspectRatioFitter.AspectMode.None;
						Rect parentRect = node.transform.parent.GetComponent<RectTransform>().rect;
						RectTransform rectTransform = node.GetComponent<RectTransform>();
						float offsetMinX, offsetMinY, offsetMaxX, offsetMaxY;
						offsetMinX = offsetMaxX = (parentRect.width - rectTransform.rect.width) / 2f;
						offsetMinY = offsetMaxY = (parentRect.height - rectTransform.rect.height) / 2f;
						rectTransform.offsetMin = new Vector2(offsetMinX, offsetMinY);
						rectTransform.offsetMax = new Vector2(-offsetMaxX, -offsetMaxY);
					}

				}
			}

			foreach (var pair in aligments)
			{
				GameObject obj = pair.Value;
				RectTransform rectTransform = obj.GetComponent<RectTransform>();
				Rect rect = rectTransform.rect;
				rectTransform.offsetMin = rectTransform.offsetMax = new Vector2(0f, 0f);
				rectTransform.sizeDelta = new Vector2(rect.width, rect.height);
				switch (pair.Key)
				{
					case "LowerCenter":
						rectTransform.pivot = new Vector2(0.5f, 0f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.5f, 0f);
						break;
					case "LowerLeft":
						rectTransform.pivot = new Vector2(0f, 0f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0f, 0f);
						break;
					case "LowerRight":
						rectTransform.pivot = new Vector2(1f, 0f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(1f, 0f);
						break;
					case "MiddleCenter":
						rectTransform.pivot = new Vector2(0.5f, 0.5f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
						break;
					case "MiddleLeft":
						rectTransform.pivot = new Vector2(0f, 0.5f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0f, 0.5f);
						break;
					case "MiddleRight":
						rectTransform.pivot = new Vector2(1f, 0.5f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(1f, 0.5f);
						break;
					case "UpperCenter":
						rectTransform.pivot = new Vector2(0.5f, 1f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.5f, 1f);
						break;
					case "UpperLeft":
						rectTransform.pivot = new Vector2(0f, 1f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0f, 1f);
						break;
					case "UpperRight":
						rectTransform.pivot = new Vector2(1f, 1f);
						rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(1f, 1f);
						break;
				}

				rectTransform.anchoredPosition = new Vector2(0f, 0f);
			}

			aligments.Clear();

			foreach (GameObject form in forms)
			{
				OptimizeForms(form);
			}

			forms.Clear();
			formTrees.Clear();

			Messanger<MessangerEvents>.SendMessage(MessangerEvents.UpdateAnchors);
			Messanger<MessangerEvents>.SendMessage(MessangerEvents.GameLoadingDone);

			firstLateUpdate = false;
		}
	}

	private void Update()
	{
#if UNITY_EDITOR
		if (reloadScene)
		{
			LoadForm();
			reloadScene = false;
			formObj = GameObject.Find(formName);
		}

		if (formObj != null && !formObj.activeSelf)
		{
			formObj.SetActive(true);
			formObj = null;
		}
#endif

		if (firstUpdate)
		{
			canvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
			canvas.GetComponent<Canvas>().worldCamera = camera;
			Input.multiTouchEnabled = false;
			firstUpdate = false;
		}
	}
}
