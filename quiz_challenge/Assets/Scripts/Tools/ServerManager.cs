﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using UnityEngine;

public static class ServerManager
{
	//private Thread thread;
	private static SGLTools.SocialManagerInstantiator.SocialManager social;
	private static Dictionary<string, object> progressToReport = null, progressToRetry = null, progressToSync;
	public static Mutex progressMutex = new Mutex();
	public static Mutex syncMutex = new Mutex();
	private static bool run = true;
	private static string ticket = null;
	private static int expirationTime = 0;

	public static bool ProgressReady { get; private set; } = false;
	public static bool ProgressSynchronized { get; set; } = false;
	private static int syncTime = 0;

	private static Dictionary<int, string> statusCodes = new Dictionary<int, string>
		{
			{ 200, "Ok" },
			{ 400, "BadRequest" },
			{ 404, "NotFound" },
			{ 472, "NoAuthorization" },
			{ 503, "ServerError" }
		};


	public static void SetProgressToReport(Dictionary<string, object> progressToReport)
	{
		progressMutex.WaitOne();

		Debug.Log("SERVER SET PROGRESS IN MANAGER");
		if (ProgressSynchronized) ServerManager.progressToReport = progressToReport;

		progressMutex.ReleaseMutex();
	}

	public static Dictionary<string, object> GetProgressToSync()
	{
		Dictionary<string, object> res = null;
		if (ProgressReady) res = progressToSync;
		return res;
	}

	private static void ServerClientLoop()
	{
		SGLTools.SocialManagerInstantiator.SocialManager sm = SGLTools.SocialManagerInstantiator.GetInstance();
		Action<int, string> authCallback = (int code, string response) =>
		{
			Debug.Log("IN AUTH CALLBACK");
			if (code == (int)HttpStatusCode.OK)
			{
				object responseObject = Json.Deserialize(response);
				if (responseObject is Dictionary<string, object> ticketDict)
				{
					syncMutex.WaitOne();
					if (ticketDict.ContainsKey("ticket")) ticket = ticketDict["ticket"] as string;

					if (ticketDict.ContainsKey("expirationTime")) expirationTime =
						Convert.ToInt32(ticketDict["expirationTime"]);
					syncMutex.ReleaseMutex();

					Debug.Log($"SERVER AUTHENTICATE TICKET {ticket}");
				}
			}
			Debug.Log($"SERVER AUTHENTICATE CODE {code}");
			Debug.Log($"SERVER AUTHENTICATE RESPONSE {response}");
		};

		Action<int, string> getProgressCallback = (int code, string response) =>
		{
			if (code != (int)HttpStatusCode.OK)
			{
				if (code == 472)
				{
					ticket = null;
					expirationTime = 0;
				}
				else if (code == (int)HttpStatusCode.NotFound)
				{
					progressToSync = null;
					ProgressReady = true;
				}
			}
			else if (code == (int)HttpStatusCode.OK)
			{
				progressToSync = (Dictionary<string, object>)Json.Deserialize(response);
				ProgressReady = true;
			}

			Debug.Log($"SERVER GET PROGRESS CODE {code}");
			Debug.Log($"SERVER GET PROGRESS RESPONSE {response}");
		};

		Action<int, string> setProgressCallback = (int code, string response) =>
		{
			if (code == 472)
			{
				ticket = null;
				expirationTime = 0;
				progressMutex.WaitOne();
				if (progressToReport == null) progressToReport = progressToRetry;
				progressMutex.ReleaseMutex();
			}
			else if (code == (int)HttpStatusCode.OK)
			{
				progressToRetry = null;
			}

			Debug.Log($"SERVER SET PROGRESS CODE {code}");
		};

		while (run)
		{
			try
			{
				if (sm.Authenticated)
				{
					if (ticket == null)
					{
						Authenticate(authCallback);
						//	continue;
					}

					if (ticket != null)
					{
						if (!ProgressReady && !ProgressSynchronized)
						{
							GetProgress(ticket, getProgressCallback);
						}
						else if (ProgressSynchronized)
						{
							progressMutex.WaitOne();
							if (progressToReport == null) progressToReport = progressToRetry;
							if (ticket != null && progressToReport != null)
							{
								string progressJSON = Json.Serialize(progressToReport);

								progressToRetry = progressToReport;
								progressToReport = null;
								progressMutex.ReleaseMutex();

								SetProgress(ticket, progressJSON, setProgressCallback);
							}
							else progressMutex.ReleaseMutex();
						}
					}
				}

				//Thread.Sleep(10000);
				if (ticket == null) Thread.Sleep(10000);
				else Thread.Sleep(1000);
			}
			catch (Exception e)
			{
				Debug.Log($"SERVER THREAD EXCEPTION: {e.Message}");
				Debug.LogError($"SERVER THREAD EXCEPTION: {e.StackTrace}");
			}
		}
	}

	public static void StopServerClient()
	{
		run = false;
	}

	public static string GetTicket()
	{
		string ticketCopy = null;
		syncMutex.WaitOne();
		ticketCopy = ticket;
		syncMutex.ReleaseMutex();

		return ticketCopy;
	}

	public static int GetSyncTimestamp()
	{
		int syncTimeCopy = 0;
		syncMutex.WaitOne();
		syncTimeCopy = syncTime;
		syncMutex.ReleaseMutex();

		return syncTime;
	}

	public static int GetTicketExpirationTime()
	{
		int expirationTimeCopy = 0;
		syncMutex.WaitOne();
		expirationTimeCopy = expirationTime;
		syncMutex.ReleaseMutex();

		return expirationTimeCopy;
	}
	public static void StartServerClient(string ticket, int syncTime)
	{
		ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
		social = SGLTools.SocialManagerInstantiator.GetInstance();
		ServerManager.ticket = ticket;
		Debug.Log($"SERVER START WITH TICKET {ticket}");
		ServerManager.syncTime = syncTime;
		//	StartCoroutine(ServerCoroutine());
		ticket = Helper.GetTicket();
		new Thread(ServerClientLoop).Start();
	}


	private static void GetProgress(string ticket, Action<int, string> callback)
	{
		StringBuilder builder = new StringBuilder();
		builder.Append(ServerConstants.Url);
		builder.Append(ServerConstants.UriGetProgress);
		builder.Append($"?{ServerConstants.Ticket}={HttpUtility.UrlEncode(ticket)}");
		GetRequest(builder.ToString(), callback);
	}

	private static void SetProgress(string ticket, string progressJSON, Action<int, string> callback)
	{
		StringBuilder builder = new StringBuilder();
		builder.Append(ServerConstants.Url);
		builder.Append(ServerConstants.UriSetProgress);
		string postData =
			$"{ServerConstants.Ticket}={HttpUtility.UrlEncode(ticket)}&" +
   $"{ServerConstants.Progress}={HttpUtility.UrlEncode(progressJSON)}";
		PostRequest(builder.ToString(), postData, callback);
	}

	private static void Authenticate(Action<int, string> callback)
	{
		Debug.Log("DO AUTHENTICATE");
		StringBuilder builder = new StringBuilder();
		builder.Append(ServerConstants.Url);
#if UNITY_EDITOR
		builder.Append(ServerConstants.UriAuthTest);
		builder.Append($"?userID={HttpUtility.UrlEncode("test_user")}");
		Debug.Log($"builderAuthUrl = {builder.ToString()}");
		GetRequest(builder.ToString(), callback);
#else

		Action<SGLTools.SocialManagerInstantiator.SocialManager.VerificationSignature> callbackSig = (signature) =>
		{
			if (signature != null)
			{
#if UNITY_IOS
				if (signature.Url != null)
				{

					Debug.Log($"SplashScreen: GenerateIdentityVerificationSignature SUCCESS. Url = {signature.Url}");
					StringBuilder builderAuthUrl = new StringBuilder();
					builderAuthUrl.Append(ServerConstants.Url);
					builderAuthUrl.Append(ServerConstants.UriAuthIOS);
					builderAuthUrl.Append($"?certificateURL={HttpUtility.UrlEncode(signature.Url)}&");
					builderAuthUrl.Append($"signature={HttpUtility.UrlEncode(signature.Signature)}&");
					builderAuthUrl.Append($"salt={HttpUtility.UrlEncode(signature.Salt)}&");
					builderAuthUrl.Append($"timestamp={HttpUtility.UrlEncode(Helper.EncodeTimestamp(signature.Timestamp))}&");
					builderAuthUrl.Append($"userID={HttpUtility.UrlEncode(signature.UserId)}");
					Debug.Log($"builderAuthUrl = {builderAuthUrl.ToString()}");
					GetRequest(builderAuthUrl.ToString(), callback);
				} 
				else Debug.Log($"SplashScreen: GenerateIdentityVerificationSignature SUCCESS. Url = NULL");
#elif UNITY_ANDROID
				if (signature.Token != null)
				{
					StringBuilder builderAuthUrl = new StringBuilder();
					builderAuthUrl.Append(ServerConstants.Url);
					builderAuthUrl.Append(ServerConstants.UriAuthAndroid);
					builderAuthUrl.Append($"?token={HttpUtility.UrlEncode(signature.Token)}&");
					//Debug.Log($"builderAuthUrl = {builderAuthUrl.ToString()}");
					Debug.Log($"SERVER AUTHENTICATE TOKEN={signature.Token}");
					GetRequest(builderAuthUrl.ToString(), callback);
				}		
#endif
			}
			else Debug.Log("ServerManager: GenerateIdentityVerificationSignature FAIL");

		};
		social.GenerateIdentityVerificationSignature(callbackSig);
#endif


	}

	private static void GetRequest(string url, Action<int, string> action)
	{
		HttpWebResponse response = null;
		StreamReader reader = null;

		Debug.Log("SERVER GET REQUEST");
		try
		{

			HttpWebRequest request = WebRequest.CreateHttp(url);
			request.Timeout = ServerConstants.Timeout;
			Debug.Log("SERVER TRY CATCH");
			response = (HttpWebResponse)request.GetResponse();
			Debug.Log("SERVER GOT RESPONSE");
			string status = GetCorrectStatusCode((int)response.StatusCode);

			reader = new StreamReader(response.GetResponseStream());
			string result = reader.ReadToEnd();
			Debug.Log($"SERVER GetRequest: url = {url}, status = {status}, result = {result}");
			action((int)response.StatusCode, result);
		}
		catch (WebException e)
		{
			Debug.Log("SERVER WEB EXCEPTION");
			response = e.Response as HttpWebResponse;
			Debug.Log($"SERVER GetRequest: url = {url}, error = {e.Message}");
			if (response != null)
			{
				action((int)response.StatusCode, e.Message);
			}
			else
			{
				action(-1, e.Message);
			}
		}
		catch (Exception e)
		{
			Debug.Log("SERVER CAUGHT EXCEPTION");
			action(-1, e.Message);
		}
	}

	private static void PostRequest(string url, string postData, Action<int, string> action)
	{
		Stream stream = null;
		HttpWebResponse response = null;
		StreamReader reader = null;

		try
		{
			HttpWebRequest request = WebRequest.CreateHttp(url);

			request.Timeout = ServerConstants.Timeout;
			var data = Encoding.UTF8.GetBytes(postData);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = data.Length;

			stream = request.GetRequestStream();
			stream.Write(data, 0, data.Length);
			stream.Flush();
			stream.Close();

			response = (HttpWebResponse)request.GetResponse();

			reader = new StreamReader(response.GetResponseStream());
			string result = reader.ReadToEnd();
			Debug.Log($"SERVER PostRequest: url = {url}, data = {postData}, result = {result}");
			action((int)response.StatusCode, result);
		}
		catch (WebException e)
		{
			response = e.Response as HttpWebResponse;
			Debug.Log($"SERVER PostRequest: url = {url}, data = {postData}, error = {e.Message}");
			if (response != null)
			{
				action((int)response.StatusCode, e.Message);
			}
			else
			{
				action(-1, e.Message);
			}
		}
		catch (Exception e)
		{
			action(-1, e.Message);
		}
		finally
		{
			if (stream != null) stream.Close();
			if (response != null) response.Close();
			if (reader != null) reader.Close();
		}
	}

	private static string GetCorrectStatusCode(int statusCode)
	{
		if (statusCodes.ContainsKey(statusCode))
		{
			return statusCodes[statusCode];
		}
		else
		{
			return statusCode.ToString();
		}
	}
}
