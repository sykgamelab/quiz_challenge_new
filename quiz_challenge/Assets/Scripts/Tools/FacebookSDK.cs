﻿using System;
#if UNITY_IOS
using System.Threading.Tasks;
using System.Runtime.InteropServices;
#endif
using System.Threading;
using UnityEditor;
using UnityEngine;

public class FacebookSDK
{
    public class Result
    {
        public Result(bool success = false, bool cancel = false, string error = null)
        {
            AuthenticationSuccess = success;
            AuthenticationCancel = cancel;
            LastError = error;
        }

        public bool AuthenticationSuccess { get; }
        public bool AuthenticationCancel { get; }
        public string LastError { get; }
    }

    private static FacebookSDK instance = null;
#if UNITY_EDITOR
    private bool isLoggedIn = false;
#elif UNITY_ANDROID
    private const string ActivityClassName = "com.sgl.facebooksdknative.views.FacebookSDK";
    private const string ServiceClassName = "com.sgl.facebooksdknative.services.FacebookSDKService";
    private AndroidJavaObject unityPlayerActivity;
    private AndroidJavaObject fb = null;
    private AndroidJavaObject fbService = null;

    private class TaskData
    {
        public AndroidJavaObject JavaObject { get; set; }
        public Action<Result> Callback { get; set; }
        public TaskData(AndroidJavaObject javaObject, Action<Result> callback)
        {
            JavaObject = javaObject;
            Callback = callback;
        }
    }
#elif UNITY_IOS
    [DllImport("__Internal")]
    private static extern void loginNative(string[] permissions, int length, string alertTitleError);

    [DllImport("__Internal")]
    private static extern void logoutNative();

    [DllImport("__Internal")]
    private static extern bool isLoggedInNative();

    [DllImport("__Internal")]
    private static extern bool isLoginDoneNative();

    [DllImport("__Internal")]
    private static extern bool isAuthenticationSuccessNative();

    [DllImport("__Internal")]
    private static extern bool isAuthenticationCancelNative();

    [DllImport("__Internal")]
    private static extern string getFBLastErrorNative();

    [DllImport("__Internal")]
    private static extern void graphRequestNative(string graphPath, string alertTitleError);

    [DllImport("__Internal")]
    private static extern bool isGraphRequestDoneNative();

    [DllImport("__Internal")]
    private static extern string graphResponseNative();

    [DllImport("__Internal")]
    private static extern void showFriendListNative(string graphPath, string ids, string alertTitleError, string alertTitleInfo,
        string alertMessageNoFriends, string alertMessageNoFriendsNow, string alertMessageConfirmSend,
        string appRequestMessage, string alertButtonYesText, string alertButtonNoText, string buttonSendText);

    [DllImport("__Internal")]
    private static extern string sendedIDFriendNative();
#endif

    private FacebookSDK()
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        var unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        unityPlayerActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        fb = new AndroidJavaObject(ActivityClassName);
        fbService = new AndroidJavaObject(ServiceClassName).CallStatic<AndroidJavaObject>("getInstance");
#endif
    }

    public static FacebookSDK GetInstance()
    {
        if (instance == null) instance = new FacebookSDK();

        return instance;
    }

    public bool IsLoggedIn
    {
        get
        {
#if UNITY_EDITOR
            return isLoggedIn;
#elif UNITY_ANDROID
            return fb.Call<bool>("isLoggedIn");
#elif UNITY_IOS
            return isLoggedInNative();
#endif
        }
#if UNITY_EDITOR
        set
        {
            isLoggedIn = value;
        }
#endif
    }

    public void Login(Action<Result> callback = null, string errorTitle = null)
    {
#if UNITY_EDITOR
        IsLoggedIn = true;
        callback?.Invoke(new Result());
#elif UNITY_ANDROID
        string[] permissions = { "public_profile", "user_friends" };
        fb.Call("loginCommand", unityPlayerActivity, permissions, errorTitle);

        Action<object> action = (arg) =>
        {
            TaskData data = (TaskData)arg;
            AndroidJavaObject javaObject = data.JavaObject;

            while (!javaObject.Call<bool>("isLoginDone"))
            {
                Thread.Sleep(1000);
            }

            bool success = javaObject.Call<bool>("isAuthenticationSuccess");
            bool cancel = javaObject.Call<bool>("isAuthenticationCancel");
            string error = javaObject.Call<string>("getLastError");

            Action<Result> cb = data.Callback;
            cb?.Invoke(new Result(success, cancel, error));
        };

        //Action<object> start = AndroidThread.GetAction(action);
        new AndroidThread(action, new TaskData(fb, callback)).Start();
#elif UNITY_IOS
        LoginAsync(errorTitle, callback);
#endif
	}

#if UNITY_EDITOR
#elif UNITY_IOS
    private async void LoginAsync(string alertTitleError, Action<Result> callback = null)
    {
        string[] permissions = { "public_profile", "user_friends" };
        loginNative(permissions, permissions.Length, alertTitleError);

        Debug.Log("FacebookSDK.LoginAsync: BEFORE AWAIT");
        await Task.Run(() =>
        {
            while (!isLoginDoneNative()) ;
        });
        Debug.Log("FacebookSDK.LoginAsync: AFTER AWAIT");

        bool success = isAuthenticationSuccessNative();
        bool cancel = isAuthenticationCancelNative();
        string error = getFBLastErrorNative();
        callback?.Invoke(new Result(success, cancel, error));
    }

    private void GraphRequestAsync(string graphPath, string alertTitleError, Action callback)
    {
        Debug.Log("FacebookSDK.GraphRequestAsync: BEFORE TASK.RUN");
        Task.Run(() =>
        {
            graphRequestNative(graphPath, alertTitleError);
            while (!isGraphRequestDoneNative()) ;
            callback.Invoke();
        });
        Debug.Log("FacebookSDK.GraphRequestAsync: AFTER TASK.RUN");
    }

    private void ShowFriendListAsync(string graphPath, string ids, string alertTitleError, string alertTitleInfo,
        string alertMessageNoFriends, string alertMessageNoFriendsNow, string alertMessageConfirmSend,
        string appRequestMessage, string alertButtonYesText, string alertButtonNoText, string buttonSendText, 
        Action<string> callback)
    {
        showFriendListNative(graphPath, ids, alertTitleError, alertTitleInfo, alertMessageNoFriends, alertMessageNoFriendsNow,
            alertMessageConfirmSend, appRequestMessage, alertButtonYesText, alertButtonNoText, buttonSendText);

        Task.Run(() => 
        {
            while (sendedIDFriendNative() == null) ;
            string id = sendedIDFriendNative();
            Debug.Log($"FacebookSDK.ShowFriendListAsync: id = {id}");
            callback.Invoke(id);
        });
    }
#endif

	public void Logout()
    {
#if UNITY_EDITOR
        IsLoggedIn = false;
#elif UNITY_ANDROID
		fb.Call("logout");
#elif UNITY_IOS
        logoutNative();
#endif
    }

    public string GetLastError()
    {
        string error = null;

#if UNITY_EDITOR
#elif UNITY_ANDROID
        error = fb.Call<string>("getLastError");
#elif UNITY_IOS
        error = getFBLastErrorNative();
#endif

        return error;
    }

    public void GraphRequest(string graphPath, Action callback, string alertTitleError = null)
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        fbService.Call("graphRequest", graphPath);

        Action<object> action = (arg) =>
        {
            while (!fbService.Call<bool>("isGraphRequestDone"))
            {
                Thread.Sleep(100);
            }

            Action cb = (Action)arg;
            cb?.Invoke();
        };

        //Action<object> start = AndroidThread.GetAction(action);
        new AndroidThread(action, callback).Start();
#elif UNITY_IOS
        GraphRequestAsync(graphPath, alertTitleError, callback);
#endif
    }

    public string GraphResponse()
    {
        string response = null;

#if UNITY_EDITOR
#elif UNITY_ANDROID
        response = fbService.Call<string>("getGraphResponse");
#elif UNITY_IOS
        response = graphResponseNative();
#endif

        return response;
    }

    private void SaveSendedIDAndUnlockGame(string id)
    {
        string savedIds = Helper.GetFBFriendIds();

        Debug.Log($"FacebookSDK.SaveSendedIDAndUnlockGame: id = {id}");
        if (id == "error_connection")
        {
            Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
            Messanger<EventMessages>.SendMessage(EventMessages.UnlockGame);
        }
        else if (id == "")
        {
            Messanger<EventMessages>.SendMessage(EventMessages.UnlockGame);
        }
        else
        {
            if (!savedIds.Contains(id))
            {
                long timestamp = Helper.GetTimestamp();
                if (savedIds == "")
                {
                    Helper.SetFBFriendIds(id);
                    Helper.SetFBFriendTimestamps(timestamp.ToString());
                }
                else
                {
                    Helper.SetFBFriendIds($"{savedIds};{id}");
                    Helper.SetFBFriendTimestamps($"{Helper.GetFBFriendTimestamps()};{timestamp}");
                }
            }
            Messanger<string, EventMessages>.SendMessage(EventMessages.LoadFacebookFriendAvatar, id);
            Messanger<EventMessages>.SendMessage(EventMessages.FacebookSuccessGame);
        }
    }

    public void ShowFriendList(string ids, string alertTitleError, string alertTitleInfo, 
        string alertMessageNoFriends, string alertMessageNoFriendsNow, string alertMessageConfirmSend, 
        string alertMessageConfirmClose, string appRequestMessage, string alertButtonYesText,string alertButtonNoText,
        string actionBarTitle, string buttonSendText)
    {
        string graphPath = "/me/friends?fields=first_name,last_name,picture.type(large)";
        Messanger<EventMessages>.SendMessage(EventMessages.HideFormNeedLoginFacebook);
#if UNITY_EDITOR
#elif UNITY_ANDROID
        fb.Call("showFriendListCommand", unityPlayerActivity, graphPath, ids, alertTitleError, alertTitleInfo, 
            alertMessageNoFriends, alertMessageNoFriendsNow, alertMessageConfirmSend, alertMessageConfirmClose,
            appRequestMessage, alertButtonYesText, alertButtonNoText, actionBarTitle, buttonSendText);

        string id = null;
        Action action = () =>
        {
            Debug.Log("FacebookSDK.ShowFriendList: BEFORE SLEEP");
            while (fbService.Call<string>("getSendedIdFriend") == null)
            {
                Thread.Sleep(1000);
            }
            id = fbService.Call<string>("getSendedIdFriend");
            Debug.Log($"FacebookSDK.ShowFriendList: AFTER SLEEP -> id = {id}");
        };

        //Action<object> start = AndroidThread.GetAction(action);
        AndroidThread thread = new AndroidThread(action);
        thread.Start();
        thread.Join();

        SaveSendedIDAndUnlockGame(id);
#elif UNITY_IOS
        Action<string> callback = (id) => SaveSendedIDAndUnlockGame(id);

        ShowFriendListAsync(graphPath, ids, alertTitleError, alertTitleInfo, alertMessageNoFriends, alertMessageNoFriendsNow, 
            alertMessageConfirmSend, appRequestMessage, alertButtonYesText, alertButtonNoText, buttonSendText, callback);
#endif
    }
}
