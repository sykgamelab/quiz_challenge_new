﻿using UnityEngine;
using UnityEngine.UI;

public class CustomFillSlider : MonoBehaviour {
	public enum SliderType { Horizontal, Vertical };

	public SliderType type = SliderType.Horizontal;

	[Range(0.0f, 1.0f)] public float value;

	[SerializeField] private GameObject handle;
	[SerializeField] private Image fill;
	[SerializeField] private Canvas canvas;

	RectTransform handleRectTransform, handleParentRectTransform;
	float handlePosition, handleSide, handleParentSide;

	private void Start()
	{
		canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
		handleParentRectTransform = handle.transform.parent.GetComponent<RectTransform>();
		handleRectTransform = handle.GetComponent<RectTransform>();

		if (type == SliderType.Horizontal)
		{
			handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).width;
			handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).width;
		}
		else if (type == SliderType.Vertical)
		{
			handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).height;
			handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).height;
		}
	}

	private void Update()
	{
		handleParentRectTransform = handle.transform.parent.GetComponent<RectTransform>();
		handleRectTransform = handle.GetComponent<RectTransform>();
		float newHandlePosition;

		if (type == SliderType.Horizontal)
		{
			handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).width;
			handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).width;

			Vector3 handlePosition = handle.transform.localPosition;
			newHandlePosition = Mathf.Floor((handleSide - handleParentSide) / 2f + value * (handleParentSide - handleSide) + 0.5f);
			handle.transform.localPosition = new Vector3(newHandlePosition, handlePosition.y, handlePosition.z);
		}
		else if (type == SliderType.Vertical)
		{
			handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).height;
			handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).height;
			Vector3 handlePosition = handle.transform.localPosition;
			newHandlePosition = Mathf.Floor((handleSide - handleParentSide) / 2f + value * (handleParentSide - handleSide) + 0.5f);
			handle.transform.localPosition = new Vector3(handlePosition.x, newHandlePosition, handlePosition.z);
		}

		fill.fillAmount = value;
	}
}
