﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class Helper
{
	[Serializable]
	private class InternalQuestion
	{
		public int correct;
		public string text;
		public string[] answers;
	}

	[Serializable]
	private class InternalQuestionsList
	{
		public InternalQuestion[] items;
	}

	[Serializable]
	private class InternalCategory
	{
		public string category;
		public int sprite;
		public string prodid;
	}

	[Serializable]
	private class InternalCategoriesList
	{
		public InternalCategory[] items;
	}

	public static string GetLangCode()
	{
		SystemLanguage lang = Application.systemLanguage;
		//SystemLanguage lang = SystemLanguage.Russian;
		switch (lang)
		{
			/*case SystemLanguage.Arabic:
				return "ar";*/
			case SystemLanguage.German:
				return "de";
			case SystemLanguage.English:
				return "en";
			case SystemLanguage.Spanish:
				return "es";
			case SystemLanguage.French:
				return "fr";
			/*case SystemLanguage.Indonesian:
				return "ind";
			case SystemLanguage.Italian:
				return "it";
			case SystemLanguage.Portuguese:
				return "pt";*/
			case SystemLanguage.Russian:
			case SystemLanguage.Ukrainian:
				return "ru";
		/*	case SystemLanguage.Thai:
				return "th";
			case SystemLanguage.Turkish:
				return "tr";
			case SystemLanguage.Vietnamese:
				return "vi";*/
			default:
				return "en";
		}
	}

	public static Tuple<Dictionary<string, Question[]>, Dictionary<string, (string, int)>> LoadCategories()
	{
		Dictionary<string, Question[]> dictQuestions = new Dictionary<string, Question[]>();
		Dictionary<string, (string, int)> dictSprites = new Dictionary<string, (string, int)>();

		string langCode = GetLangCode();
		string path = "Questions/categories";
		TextAsset categoriesText = Resources.Load<TextAsset>(path);
		InternalCategoriesList categoriesJSON = JsonUtility.FromJson<InternalCategoriesList>(categoriesText.text);

		foreach (InternalCategory category in categoriesJSON.items)
		{
			for (int i = 0; i < 4; i++)
			{
				path = $"Questions/{LocalizationManager.GetCurrent().ToString().ToLower()}/{category.category}{i}";
				TextAsset questionsText = Resources.Load<TextAsset>(path);
				InternalQuestionsList questionsJSON = JsonUtility.FromJson<InternalQuestionsList>(questionsText.text);
				List<Question> questions = new List<Question>();
				foreach (InternalQuestion questionJSON in questionsJSON.items)
				{
					Question question = new Question();
					question.Correct = questionJSON.correct;
					question.Text = questionJSON.text;
					foreach (string answer in questionJSON.answers)
						question.AddAnswer(answer);
					question.Category = category.category;
					questions.Add(question);
				}

				dictQuestions.Add(category.category + i, questions.ToArray());
			}
			dictSprites.Add(category.category, (category.prodid, category.sprite));
		}

		return new Tuple<Dictionary<string, Question[]>, Dictionary<string, (string, int)>>(dictQuestions, dictSprites);
	}

	public static bool IsHintUsed(string category, int part, HintType hint) =>
		PlayerPrefs.GetInt($"{category}{part}_{hint}_used", 0) != 0;

	public static void SetHintUsed(string category, int part, HintType hint, bool used) =>
		PlayerPrefs.SetInt($"{category}{part}_{hint}_used", used ? 1 : 0);

	public static bool IsAnswerHidden(string category, int part, string letter) =>
		PlayerPrefs.GetInt($"{category}{part}_{letter}_hidden", 0) != 0;

	public static void SetAnswerHidden(string category, int part, string letter, bool hidden) =>
		PlayerPrefs.SetInt($"{category}{part}_{letter}_hidden", hidden ? 1 : 0);

	public static int GetCurrQuestion(string category, int part) =>
		PlayerPrefs.GetInt($"curr_question_{category}{part}", 0);
	public static void SetCurrQuestion(string category, int part, int question) =>
		PlayerPrefs.SetInt($"curr_question_{category}{part}", question);
	public static bool IsMusicActive() => PlayerPrefs.GetInt("music_active", 1) != 0;
	public static void SetMusicActive(bool active) => PlayerPrefs.SetInt("music_active", active ? 1 : 0);
	public static bool IsSoundActive() => PlayerPrefs.GetInt("sound_active", 1) != 0;
	public static void SetSoundActive(bool active) => PlayerPrefs.SetInt("sound_active", active ? 1 : 0);

	public static int GetBalance() => PlayerPrefs.GetInt("balance", GameConstants.DefaultBalance);
	public static void SetBalance(int balance) => PlayerPrefs.SetInt("balance", balance);

	public static int GetAnswerNumber(string category, int part, string letter)
	{
		int nmb = PlayerPrefs.GetInt($"{category}{part}_answer_{letter}", -1);
		if (nmb < 0)
		{
			switch (letter)
			{
				case "A":
					nmb = 0;
					break;
				case "B":
					nmb = 1;
					break;
				case "C":
					nmb = 2;
					break;
				case "D":
					nmb = 3;
					break;
			}
		}
		return nmb;
	}

	public static string GetAnswerLetter(string category, int part, int number)
	{
		string letter = PlayerPrefs.GetString($"{category}{part}_answer_{number}", "");
		if (letter == "")
		{
			switch (number)
			{
				case 0:
					letter = "A";
					break;
				case 1:
					letter = "B";
					break;
				case 2:
					letter = "C";
					break;
				case 3:
					letter = "D";
					break;
			}
		}
		return letter;
	}

	public static void SetAnswerNumber(string category, int part, string letter, int number) =>
		PlayerPrefs.SetInt($"{category}{part}_answer_{letter}", number);
	public static void SetAnswerLetter(string category, int part, int number, string letter) =>
		PlayerPrefs.SetString($"{category}{part}_answer_{number}", letter);

	public static void FixButton(Button button)
	{
		button.transition = Selectable.Transition.None;
		button.interactable = false;
	}

	public static void UnfixButton(Button button)
	{
		button.transition = Selectable.Transition.ColorTint;
		button.interactable = true;
	}

	public static long GetTimestamp()
	{
		DateTime now = DateTime.UtcNow;
		long unixTime = ((DateTimeOffset)now).ToUnixTimeSeconds();
		return unixTime;
	}

	public static int GetScore() => PlayerPrefs.GetInt("score", 0);
	public static void SetScore(int score) => PlayerPrefs.SetInt("score", score);

	public static Dictionary<string, object> GetProgress()
	{
		string[] abc = new string[] { "A", "B", "C", "D" };
		int[] answerNumbers = new int[] { 0, 1, 2, 3 };

		Dictionary<string, object> progress = new Dictionary<string, object>();
		var categoriesTuple = LoadCategories().Item2;
		foreach (var pair in categoriesTuple)
		{
			string key;
			CategoryButton.State state;

			for (int i = 0; i < GameConstants.CategoryParts; i++)
			{
				foreach (HintType hint in Enum.GetValues(typeof(HintType)))
				{
					key = $"{pair.Key}{i}_{hint.ToString()}_used";
					bool used = IsHintUsed(pair.Key, i, hint);
					progress.Add(key, used);
				}

				foreach (string letter in abc)
				{
					key = $"{pair.Key}{i}_{letter}_hidden";
					bool hidden = IsAnswerHidden(pair.Key, i, letter);
					progress.Add(key, hidden);
					key = $"{pair.Key}{i}_answer_{letter}";
					int number = GetAnswerNumber(pair.Key, i, letter);
					progress.Add(key, number);
				}

				key = $"curr_question_{pair.Key}{i}";
				int currQuestion = GetCurrQuestion(pair.Key, i);
				progress.Add(key, currQuestion);

				foreach (int number in answerNumbers)
				{
					key = $"{pair.Key}{i}_answer_{number}";
					string letter = GetAnswerLetter(pair.Key, i, number);
					progress.Add(key, letter);
				}

				key = $"category{pair.Key}{i}_state";
				state = GetCategoryState(pair.Key, i);
				progress.Add(key, (int)state);
			}

			key = $"category{pair.Key}-1_state";
			state = GetCategoryState(pair.Key, -1);
			progress.Add(key, (int)state);
		}

		progress.Add("score", GetScore());
		progress.Add("balance", GetBalance());
		progress.Add("categories_done", GetCategoriesDone());
		int timestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
		progress.Add("timestamp", timestamp);
		return progress;
	}

	public static void SetProgress(Dictionary<string, object> progress)
	{
		string[] abc = new string[] { "A", "B", "C", "D" };
		int[] answerNumbers = new int[] { 0, 1, 2, 3 };

		var categoriesTuple = LoadCategories().Item2;
		foreach (var pair in categoriesTuple)
		{
			string key;
			CategoryButton.State state;

			for (int i = 0; i < GameConstants.CategoryParts; i++)
			{
				foreach (HintType hint in Enum.GetValues(typeof(HintType)))
				{
					key = $"{pair.Key}{i}_{hint.ToString()}_used";
					if (progress.ContainsKey(key))
					{
						bool used = Convert.ToBoolean(progress[key]);
						SetHintUsed(pair.Key, i, hint, used);
					}
				}

				foreach (string letter in abc)
				{
					key = $"{pair.Key}{i}_{letter}_hidden";
					//bool hidden = IsAnswerHidden(pair.Key, i, letter);
					if (progress.ContainsKey(key))
					{
						bool hidden = Convert.ToBoolean(progress[key]);
						SetAnswerHidden(pair.Key, i, letter, hidden);
					}

					key = $"{pair.Key}{i}_answer_{letter}";
					//int number = GetAnswerNumber(pair.Key, i, letter);
					if (progress.ContainsKey(key))
					{
						int number = Convert.ToInt32(progress[key]);
						SetAnswerNumber(pair.Key, i, letter, number);
					}
				}

				key = $"curr_question_{pair.Key}{i}";
				//int currQuestion = GetCurrQuestion(pair.Key, i);
				if (progress.ContainsKey(key))
				{
					int currQuestion = Convert.ToInt32(progress[key]);
					SetCurrQuestion(pair.Key, i, currQuestion);
				}

				foreach (int number in answerNumbers)
				{
					key = $"{pair.Key}{i}_answer_{number}";
					//string letter = GetAnswerLetter(pair.Key, i, number);
					if (progress.ContainsKey(key))
					{
						string letter = Convert.ToString(progress[key]);
						SetAnswerLetter(pair.Key, i, number, letter);
					}
				}

				key = $"category{pair.Key}{i}_state";
				//locked = IsCategoryLocked(pair.Key, i);
				if (progress.ContainsKey(key))
				{
					state = (CategoryButton.State)Convert.ToInt32(progress[key]);
					SetCategoryState(pair.Key, i, state);
				}
			}

			key = $"category{pair.Key}-1_state";
			//locked = IsCategoryLocked(pair.Key, -1);
			if (progress.ContainsKey(key))
			{
				state = (CategoryButton.State)Convert.ToInt32(progress[key]);
				SetCategoryState(pair.Key, -1, state);
			}
		}

		if (progress.ContainsKey("score"))
		{
			int score = Convert.ToInt32(progress["score"]);
			SetScore(score);
		}

		if (progress.ContainsKey("balance"))
		{
			int balance = Convert.ToInt32(progress["balance"]);
			SetBalance(balance);
		}
		
		if (progress.ContainsKey("categories_done"))
		{
			int categoriesDone = Convert.ToInt32(progress["categories_done"]);
			SetCategoriesDone(categoriesDone);
		}
	}


	public static byte[] EncodeTimestamp(long timestamp)
	{
		byte[] intBytes = BitConverter.GetBytes(timestamp);
		if (BitConverter.IsLittleEndian)
			Array.Reverse(intBytes);
		byte[] result = intBytes;

		return result;
	}

	public static bool GetHaveScoreToReport() => PlayerPrefs.GetInt("have_score_to_report", 0) != 0;
	public static void SetHaveScoreToReport(bool has) => PlayerPrefs.SetInt("have_score_to_report", has ? 1 : 0);


	public static string GetFBFriendIds() => PlayerPrefs.GetString("fb_friend_ids", "");
	public static void SetFBFriendIds(string ids) => PlayerPrefs.SetString("fb_friend_ids", ids);
	public static string GetFBFriendTimestamps() => PlayerPrefs.GetString("fb_friend_timestamps");
	public static void SetFBFriendTimestamps(string timestamps) => PlayerPrefs.SetString("fb_friend_timestamps", timestamps);

	/*public static bool IsCategoryLocked(string category, int part)
	{
		bool locked;

		if (part < 0) locked = PlayerPrefs.GetInt($"{category}_locked", 1) > 0;
		else locked = PlayerPrefs.GetInt($"{category}{part}_locked", 1) > 0;

		return locked;
	}*/

	/*public static void SetCategoryLocked(string category, int part, bool locked)
	{
		int val = locked ? 1 : 0;

		if (part < 0) PlayerPrefs.SetInt($"{category}_locked", val);
		else PlayerPrefs.SetInt($"{category}{part}_locked", val);
	}*/

	public static void SetCategoryState(string category, int part, CategoryButton.State state) =>
		PlayerPrefs.SetInt($"category{category}{part}_state", (int)state);
	public static CategoryButton.State GetCategoryState(string category, int part) =>
		(CategoryButton.State)PlayerPrefs.GetInt($"category{category}{part}_state", 0);

	public static int GetCategoriesDone() => PlayerPrefs.GetInt("categories_done", 0);
	public static void SetCategoriesDone(int val) => PlayerPrefs.SetInt("categories_done", val);

	public static int GetCategoryPartTime(string category, int part) => PlayerPrefs.GetInt($"{category}{part}_time", 0);
	public static void SetCategoryPartTime(string category, int part, int time) => PlayerPrefs.SetInt($"{category}{part}_time", time);

	public static int GetSessionTime() => PlayerPrefs.GetInt("session_time", 0);
	public static void SetSessionTime(int time) => PlayerPrefs.SetInt("session_time", time);

	public static int GetAchievementProgress(string category) => PlayerPrefs.GetInt($"{category}_achievement_progress", 0);
	public static void SetAchievementProgress(string category, int progress) => PlayerPrefs.SetInt($"{category}_achievement_progress", progress);

	public static bool HasAchievementToReport(string category) => PlayerPrefs.GetInt($"{category}_to_report", 0) != 0;
	public static void SetAchievementToReport(string category, bool has) => PlayerPrefs.SetInt($"{category}_to_report", has ? 1 : 0);

	public static string GetTicket()
	{
		string ticket = null;
		int time = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
		int expTime = PlayerPrefs.GetInt("expiration_time", 0);
		if (expTime < time)
		{
			PlayerPrefs.SetString("ticket", null);
			PlayerPrefs.SetInt("expiration_time", 0);
		}
		else ticket = PlayerPrefs.GetString("ticket", null);

		return ticket;
	}

	public static void SetTicket(string ticket, int expirationTime)
	{
		PlayerPrefs.SetString("ticket", ticket);
		PlayerPrefs.SetInt("expiration_time", expirationTime);
	}

	public static string GetPrevProgress() => PlayerPrefs.GetString("prev_progress", "{}");
	public static void SetPrevProgress(string progress) => PlayerPrefs.SetString("prev_progress", progress);
	public static int GetSyncTimestamp() => PlayerPrefs.GetInt("sync_timestamp", 0);
	public static void SetSyncTimestamp(int timestamp) => PlayerPrefs.SetInt("sync_timestamp", timestamp);

	public static int GetAdsCount()
	{
		int curr = PlayerPrefs.GetInt("curr_ads", 1);

		PlayerPrefs.SetInt("curr_ads", (curr + 1) % 15);

		return curr;
	}
}



