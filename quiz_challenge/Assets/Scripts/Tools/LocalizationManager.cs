﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public enum SupportedLanguages { En, Ru, Fr, De, Es };
public static class LocalizationManager
{
	private static Dictionary<string, Dictionary<string, string>> strings = null;

	[Serializable]
	private class InternalStringElement
	{
		public string key;
		public string value;
	}

	[Serializable]
	private class InternalStringsGroup
	{
		public InternalStringElement[] lobby;
		public InternalStringElement[] premium;
		public InternalStringElement[] settings;
		public InternalStringElement[] categorySelection;
		public InternalStringElement[] passingLevel;
		public InternalStringElement[] downloadAdditionalLevels;
		public InternalStringElement[] bank;
		public InternalStringElement[] hints;
		public InternalStringElement[] internetConnection;
		public InternalStringElement[] notEnoughCoins;
		public InternalStringElement[] push;
		public InternalStringElement[] additional;
	}

	[Serializable]
	private class InternalStringsList
	{
		public InternalStringsGroup items;
	}

	private static void LoadStrings(SupportedLanguages lang)
	{
		if (strings == null)
		{
			strings = new Dictionary<string, Dictionary<string, string>>();
		}
		strings.Clear();

		string path = $"Localization/{GetCurrent().ToString().ToLower()}/strings";
		TextAsset text = Resources.Load<TextAsset>($"Localizations/{GetCurrent().ToString().ToLower()}/strings");
		InternalStringsList list = JsonUtility.FromJson<InternalStringsList>(text.text);

		FieldInfo[] fields = list.items.GetType().GetFields();
		foreach (var field in fields)
		{
			InternalStringElement[] fieldVal = (InternalStringElement[])field.GetValue(list.items);
			Dictionary<string, string> dict = new Dictionary<string, string>();
			foreach (var pair in fieldVal)
			{
				dict.Add(pair.key, pair.value);
			}
			strings.Add(field.Name, dict);
		}
	}

	public static string GetString(string formName, string key)
	{
		if (strings == null)
		{
			LoadStrings(GetCurrent());
		}

		if (strings.ContainsKey(formName) && strings[formName].ContainsKey(key))
		{
			return strings[formName][key];
		}

		return "";
	}

	public static SupportedLanguages GetCurrent()
	{
		if (!PlayerPrefs.HasKey("current_language"))
		{
			Debug.Log("LANGUAGE NO CURRENT");
			Debug.Log($"SET LANGUAGE {Helper.GetLangCode()}");
			PlayerPrefs.SetString("current_language", Helper.GetLangCode());
		}
		else Debug.Log("LANGUAGE HAS CURRENT");

		return (SupportedLanguages)Enum.Parse(typeof(SupportedLanguages),
			 PlayerPrefs.GetString("current_language"), true);
	}

	public static void SetCurrent(SupportedLanguages lang)
	{
		PlayerPrefs.SetString("current_language", lang.ToString());
		LoadStrings(lang);
	}

    public static string GetLanguageShort(SupportedLanguages lang)
    {
        Dictionary<SupportedLanguages, string> langs = new Dictionary<SupportedLanguages, string>()
        {
            { SupportedLanguages.En, "EN" },
            { SupportedLanguages.Ru, "РУ" },
            { SupportedLanguages.Fr, "FR" },
            { SupportedLanguages.De, "DE" },
            { SupportedLanguages.Es, "ES" }
        };

        return langs[lang];
    }

    public static Sprite GetFlag(SupportedLanguages lang)
    {
        Dictionary<SupportedLanguages, int> flags = new Dictionary<SupportedLanguages, int>()
        {
            { SupportedLanguages.En, 34 },
            { SupportedLanguages.Ru, 35 },
            { SupportedLanguages.De, 36 },
            { SupportedLanguages.Fr, 37 },
            { SupportedLanguages.Es, 38 }
        };

        int index = flags[lang];

        return URPNGLoader.LoadURPNG("sprite_2", index);
    }
}
