﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/***************************************************************************************/
public class Subscription<TEvent> where TEvent : struct
{
	private readonly Action handler;
	private readonly TEvent eventType;

	public Subscription(Action handler, TEvent eventType)
	{
		this.handler = handler;
		this.eventType = eventType;
	}

	public void Unsubscribe() => Messanger<TEvent>.Unsubscribe(eventType, handler);
}

public class Subscription<TArg, TEvent> where TEvent : struct
{
	private readonly Action<TArg> handler;
	private readonly TEvent eventType;

	public Subscription(Action<TArg> handler, TEvent eventType)
	{
		this.handler = handler;
		this.eventType = eventType;
	}


	public void Unsubscribe() => Messanger<TArg, TEvent>.Unsubscribe(eventType, handler);
}

public class Subscription<TArg1, TArg2, TEvent> where TEvent : struct
{
	private readonly Action<TArg1, TArg2> handler;
	private readonly TEvent eventType;

	public Subscription(Action<TArg1, TArg2> handler, TEvent eventType)
	{
		this.handler = handler;
		this.eventType = eventType;
	}


	public void Unsubscribe() => Messanger<TArg1, TArg2, TEvent>.Unsubscribe(eventType, handler);
}

public static class Messanger<TArg, TEvent> where TEvent : struct
{
	private static Dictionary<TEvent, Delegate> handlers =
		new Dictionary<TEvent, Delegate>();

	public static Subscription<TArg, TEvent> Subscribe(TEvent eventType, Action<TArg> handler)
	{
		if (!handlers.ContainsKey(eventType))
		{
			handlers.Add(eventType, handler);
			return new Subscription<TArg, TEvent>(handler, eventType);
		}

		handlers[eventType] = Action<object>.Combine(handlers[eventType], handler);
		return new Subscription<TArg, TEvent>(handler, eventType);
	}

	public static void Unsubscribe(TEvent eventType, Action<TArg> handler)
	{
		if (!handlers.ContainsKey(eventType))
			return;

		handlers[eventType] = Delegate.Remove(handlers[eventType], handler);
		if (handlers[eventType] == null)
			handlers.Remove(eventType);
	}

	public static void SendMessage(TEvent eventType, TArg arg)
	{
		if (!handlers.ContainsKey(eventType) || handlers[eventType] == null)
			return;
		var delegates = handlers[eventType].GetInvocationList();
		foreach (Action<TArg> act in delegates)
		{
			try
			{
				act.Invoke(arg);
			}
			catch (Exception e)
			{
				string argStr = arg == null ? "null" : arg.ToString();
				Debug.LogError("Err sending message: " + eventType + " with arg " +
				argStr + ": " + e.Message);
				Debug.LogError(e.StackTrace);
			}
		}
	}
}

public static class Messanger<TArg1, TArg2, TEvent> where TEvent : struct
{
	private static Dictionary<TEvent, Delegate> handlers =
		new Dictionary<TEvent, Delegate>();

	public static Subscription<TArg1, TArg2, TEvent> Subscribe(TEvent eventType, Action<TArg1, TArg2> handler)
	{
		if (!handlers.ContainsKey(eventType))
		{
			handlers.Add(eventType, handler);
			return new Subscription<TArg1, TArg2, TEvent>(handler, eventType);
		}

		handlers[eventType] = Action<object>.Combine(handlers[eventType], handler);
		return new Subscription<TArg1, TArg2, TEvent>(handler, eventType);
	}

	public static void Unsubscribe(TEvent eventType, Action<TArg1, TArg2> handler)
	{
		if (!handlers.ContainsKey(eventType))
			return;

		handlers[eventType] = Delegate.Remove(handlers[eventType], handler);
		if (handlers[eventType] == null)
			handlers.Remove(eventType);
	}

	public static void SendMessage(TEvent eventType, TArg1 arg1, TArg2 arg2)
	{
		if (!handlers.ContainsKey(eventType) || handlers[eventType] == null)
			return;
		var delegates = handlers[eventType].GetInvocationList();
		foreach (Action<TArg1, TArg2> act in delegates)
		{
			try
			{
				act.Invoke(arg1, arg2);
			}
			catch (Exception e)
			{
				string argStr = arg1 == null ? "null" : arg1.ToString();
				Debug.LogError("Err sending message: " + eventType + " with arg " +
				argStr + ": " + e.Message);
				Debug.LogError(e.StackTrace);
			}
		}
	}
}

public static class Messanger<TEvent> where TEvent : struct
{
	private static Dictionary<TEvent, Delegate> handlers =
		new Dictionary<TEvent, Delegate>();

	public static Subscription<TEvent> Subscribe(TEvent eventType, Action handler)
	{
		if (!handlers.ContainsKey(eventType))
		{
			handlers.Add(eventType, handler);
			return new Subscription<TEvent>(handler, eventType);
		}

		handlers[eventType] = Action<object>.Combine(handlers[eventType], handler);
		return new Subscription<TEvent>(handler, eventType);
	}

	public static void Unsubscribe(TEvent eventType, Action handler)
	{
		if (!handlers.ContainsKey(eventType))
			return;

		handlers[eventType] = Delegate.Remove(handlers[eventType], handler);
		if (handlers[eventType] == null)
			handlers.Remove(eventType);
	}

	public static void SendMessage(TEvent eventType)
	{
		if (!handlers.ContainsKey(eventType) || handlers[eventType] == null)
			return;
		var delegates = handlers[eventType].GetInvocationList();
		foreach (Action act in delegates)
		{
			try
			{
				act.Invoke();
			}
			catch (Exception e)
			{
				Debug.LogError("Err sending message: " + eventType + ": " + e.Message);
				Debug.LogError(e.StackTrace);
			}
		}
	}
}

/***************************************************************************************/
public class SubscriptionFunc<TOutArg, TEvent> where TEvent : struct
{
	private readonly Func<TOutArg> handler;
	private readonly TEvent eventType;

	public SubscriptionFunc(Func<TOutArg> handler, TEvent eventType)
	{
		this.handler = handler;
		this.eventType = eventType;
	}


	public void Unsubscribe() => MessangerFunc<TOutArg, TEvent>.Unsubscribe(eventType, handler);
}

public class SubscriptionFunc<TInArg, TOutArg, TEvent> where TEvent : struct
{
	private readonly Func<TInArg, TOutArg> handler;
	private readonly TEvent eventType;

	public SubscriptionFunc(Func<TInArg, TOutArg> handler, TEvent eventType)
	{
		this.handler = handler;
		this.eventType = eventType;
	}


	public void Unsubscribe() => MessangerFunc<TInArg, TOutArg, TEvent>.Unsubscribe(eventType, handler);
}

public static class MessangerFunc<TOutArg, TEvent> where TEvent : struct
{
	private static Dictionary<TEvent, Delegate> handlers =
		new Dictionary<TEvent, Delegate>();

	public static SubscriptionFunc<TOutArg, TEvent> Subscribe(TEvent eventType, Func<TOutArg> handler)
	{
		if (!handlers.ContainsKey(eventType))
		{
			handlers.Add(eventType, handler);
			return new SubscriptionFunc<TOutArg, TEvent>(handler, eventType);
		}

		handlers[eventType] = Func<object>.Combine(handlers[eventType], handler);
		return new SubscriptionFunc<TOutArg, TEvent>(handler, eventType);
	}

	public static void Unsubscribe(TEvent eventType, Func<TOutArg> handler)
	{
		if (!handlers.ContainsKey(eventType))
			return;

		handlers[eventType] = Delegate.Remove(handlers[eventType], handler);
		if (handlers[eventType] == null)
			handlers.Remove(eventType);
	}

	public static TOutArg SendMessage(TEvent eventType)
	{
		if (!handlers.ContainsKey(eventType) || handlers[eventType] == null)
			return default(TOutArg);
		var delegates = handlers[eventType].GetInvocationList();
		foreach (Func<TOutArg> func in delegates)
		{
			try
			{
				return (func.Invoke());
			}
			catch (Exception e)
			{
				Debug.LogError("Err sending message: " + eventType + ": " + e.Message);
				Debug.LogError(e.StackTrace);

			}
		}
		return default(TOutArg);
	}
}

public static class MessangerFunc<TInArg, TOutArg, TEvent> where TEvent : struct
{
	private static Dictionary<TEvent, Delegate> handlers =
		new Dictionary<TEvent, Delegate>();

	public static SubscriptionFunc<TInArg, TOutArg, TEvent> Subscribe(TEvent eventType, Func<TInArg, TOutArg> handler)
	{
		if (!handlers.ContainsKey(eventType))
		{
			handlers.Add(eventType, handler);
			return new SubscriptionFunc<TInArg, TOutArg, TEvent>(handler, eventType);
		}

		handlers[eventType] = Func<object>.Combine(handlers[eventType], handler);
		return new SubscriptionFunc<TInArg, TOutArg, TEvent>(handler, eventType);
	}

	public static void Unsubscribe(TEvent eventType, Func<TInArg, TOutArg> handler)
	{
		if (!handlers.ContainsKey(eventType))
			return;

		handlers[eventType] = Delegate.Remove(handlers[eventType], handler);
		if (handlers[eventType] == null)
			handlers.Remove(eventType);
	}

	public static TOutArg SendMessage(TEvent eventType, TInArg inArg)
	{
		if (!handlers.ContainsKey(eventType) || handlers[eventType] == null)
			return default(TOutArg);
		var delegates = handlers[eventType].GetInvocationList();
		foreach (Func<TInArg, TOutArg> func in delegates)
		{
			try
			{
				return (func.Invoke(inArg));
			}
			catch (Exception e)
			{
				Debug.LogError("Err sending message: " + eventType + " with arg " +
				inArg.ToString() + ": " + e.Message);
				Debug.LogError(e.StackTrace);

			}
		}
		return default(TOutArg);
	}
}