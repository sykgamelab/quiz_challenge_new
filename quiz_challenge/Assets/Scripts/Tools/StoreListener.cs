﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class StoreListener : MonoBehaviour, IStoreListener
{
	private Subscription<string, EventMessages> subscription;
	private bool initializing = false, initializeFailed = false;
	private static IStoreController controller = null;
	private static IExtensionProvider extension = null;
	private IYandexAppMetrica appmetrica;

	public static Dictionary<string, string> Prices { get; private set; }
	public static StoreListener I { get; set; } = null;

	public bool Initialized { get; set; } = false;
	
	private bool IsInitialized() => controller != null && extension != null;

	private const string coins250 = "250coins";
	private const string coins690 = "690coins";
	private const string coins1520 = "1520coins";
	private const string coins3500 = "3500coins";
	private const string coins7350 = "7350coins";

	private void Awake()
	{
		I = this;
		subscription = Messanger<string, EventMessages>.Subscribe(EventMessages.BuyProduct, OnBuyProduct);
	}

	private void Init()
	{
		if (IsInitialized()) return;

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		builder.AddProduct(coins250, ProductType.Consumable, new IDs
		{
			{ coins250, MacAppStore.Name },
			{ coins250, GooglePlay.Name }
		});
		builder.AddProduct(coins690, ProductType.Consumable, new IDs
		{
			{ coins690, MacAppStore.Name },
			{ coins690, GooglePlay.Name }
		});
		builder.AddProduct(coins1520, ProductType.Consumable, new IDs
		{
			{ coins1520, MacAppStore.Name },
			{ coins1520, GooglePlay.Name }
		});
		builder.AddProduct(coins3500, ProductType.Consumable, new IDs
		{
			{ coins3500, MacAppStore.Name },
			{ coins3500, GooglePlay.Name }
		});
		builder.AddProduct(coins7350, ProductType.Consumable, new IDs
		{
			{ coins7350, MacAppStore.Name },
			{ coins7350, GooglePlay.Name }
		});


		/*for (int i = 1; i <= 9; i++ )
		{
			builder.AddProduct(category + i, ProductType.NonConsumable, new IDs
			{
				{ category + i, MacAppStore.Name },
				{ category + i, GooglePlay.Name }
			});
		}*/

		Dictionary<string, (string, int)> categories = Helper.LoadCategories().Item2;
		foreach (var pair in categories)
		{
			if (pair.Value.Item1 != null && pair.Value.Item1.Length > 0)
			{
				builder.AddProduct(pair.Value.Item1, ProductType.NonConsumable, new IDs
				{
					{ pair.Value.Item1, MacAppStore.Name },
					{ pair.Value.Item1, GooglePlay.Name }
				});
			}
		}

		UnityPurchasing.Initialize(this, builder);
		Debug.Log("STORE_LISTENER: RUN UnityPurchasing.Initialize");
	}

	public void OnInitialized(IStoreController storeController, IExtensionProvider extensionProvider)
	{
		initializing = false;
		controller = storeController;
		extension = extensionProvider;
		Dictionary<string, string> prices = new Dictionary<string, string>()
		{
			{"250coins", null },
			{"690coins", null },
			{ "1520coins", null },
			{"3500coins", null },
			{"7350coins", null }
		};

		List<string> keys = new List<string>()
		{
			"250coins",
			"690coins",
			"1520coins",
			"3500coins",
			"7350coins"
		};

		Dictionary<string, (string, int)> categories = Helper.LoadCategories().Item2;
		foreach (var pair in categories)
		{
			if (pair.Value.Item1 != null && pair.Value.Item1.Length > 0)
			{
				prices.Add(pair.Value.Item1, null);
				keys.Add(pair.Value.Item1);
			}
		}

		foreach (string id in keys)
		{
			Product prod = controller.products.WithID(id);
			prices[id] = prod.metadata.localizedPriceString;
		}

		Prices = prices;
		RestorePurchases();
		Initialized = true;
	}

	private static bool IsPurchased(string productID)
	{
		bool result = false;

		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
			var appleConfig = builder.Configure<IAppleConfiguration>();
			var receiptData = Convert.FromBase64String(appleConfig.appReceipt);
			AppleReceipt receipt = new AppleValidator(AppleTangle.Data()).Validate(receiptData);

			foreach (AppleInAppPurchaseReceipt productReceipt in receipt.inAppPurchaseReceipts)
			{
				if (productReceipt.productID == productID)
				{
					result = true;
					break;
				}
			}
		}

		return result;
	}

	public static void RestorePurchases()
	{
		if (extension != null && controller != null)
		{
			extension.GetExtension<IAppleExtensions>().RestoreTransactions(result =>
			{
				if (result)
				{
					var categories = Helper.LoadCategories().Item2;
					int count = categories.Count;

					foreach (var pair in categories)
					{
						string category = pair.Value.Item1;
						Product product = controller.products.WithID(category);
						if (product != null && product.availableToPurchase && IsPurchased(product.definition.id))
						{
							if (Helper.GetCategoryState(category, -1) == CategoryButton.State.Locked)
							{
								Helper.SetCategoryState(category, -1, CategoryButton.State.ReadyToGet);
								if (count == 1) Categories.I.SortCategories();
								count--;
								Debug.Log($"RestorePurchases: {category} UNLOCK");
							}
						}
					}
				}
			});
		}
	}

	private void OnBuyProduct(string id)
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
			Messanger<EventMessages>.SendMessage(EventMessages.UnlockBank);
			Messanger<string, EventMessages>.SendMessage(EventMessages.CategoryBought, null);
			return;
		}
		else if (IsInitialized())
		{
			Debug.Log("STORE_LISTENER: RUN OnBuyProduct");
			if (controller == null)
			{
				Debug.Log("STORE CONTROLLER IS NULL");
			}
			Product prod = controller.products.WithID(id);
			if (prod == null || !prod.availableToPurchase)
			{
				Debug.Log($"{id} is unavailable or not existing");
				Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("additional", "PurchaseError"));
				Messanger<EventMessages>.SendMessage(EventMessages.UnlockBank);
				Messanger<string, EventMessages>.SendMessage(EventMessages.CategoryBought, null);
				return;
			}

			Debug.Log($"Buying: {prod.definition.id}");
			controller.InitiatePurchase(prod);
		}
		else
		{
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured,
				 LocalizationManager.GetString("additional", "PurchaseError"));
			Messanger<EventMessages>.SendMessage(EventMessages.UnlockBank);
			Messanger<string, EventMessages>.SendMessage(EventMessages.CategoryBought, null);
		}
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		string prodid = args.purchasedProduct.definition.id;
		int balance = Helper.GetBalance();
		int amount = 0;
		switch (prodid)
		{
			case coins250:
				amount = 250;
				break;
			case coins690:
				amount = 690;
				break;
			case coins1520:
				amount = 1520;
				break;
			case coins3500:
				amount = 3500;
				break;
			case coins7350:
				amount = 7350;
				break;
		}

		if (prodid.Contains("coins"))
		{
			appmetrica.ReportEvent(MetricaConstants.CoinsBought,
				new Dictionary<string, object>()
				{
				{ MetricaConstants.Amount, amount }
				});
			balance += amount;
			Helper.SetBalance(balance);

			Messanger<int, EventMessages>.SendMessage(EventMessages.CoinsBought, amount);
			Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
			Messanger<EventMessages>.SendMessage(EventMessages.UnlockBank);
		}
		else
		{
			appmetrica.ReportEvent(MetricaConstants.CategoryBought,
				new Dictionary<string, object>()
				{
				{ MetricaConstants.Category, prodid }
				});
			Messanger<string, EventMessages>.SendMessage(EventMessages.CategoryBought, prodid);
		}

		Debug.Log("SERVER SET PURCHASE PROGRESS");
		SocialManager.I.SetProgressToReport();

		return PurchaseProcessingResult.Complete;
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		initializing = false;
		initializeFailed = true;
		/*Debug.Log("STORE INIT FAILED");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("internetConnection", "NoConnection"));
        }
        else
        {
            Debug.Log($"error = {error.ToString()}");
            Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, "Ошибка открытия магазина");
        }

		Messanger<string, EventMessages>.SendMessage(EventMessages.CategoryBought, null);
		Messanger<EventMessages>.SendMessage(EventMessages.UnlockBank);*/
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		if (!failureReason.ToString().Contains("UserCancelled"))
		{
			Debug.Log($"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("additional", "PurchaseError"));
		}

		Messanger<string, EventMessages>.SendMessage(EventMessages.CategoryBought, null);
		Messanger<EventMessages>.SendMessage(EventMessages.UnlockBank);
	}

	private void Update()
	{
		if (!initializing && initializeFailed) Init();
	}

	private void OnDestroy()
	{
		subscription.Unsubscribe();
	}

	private void Start()
	{
		appmetrica = AppMetrica.Instance;
		Init();
	}
}
