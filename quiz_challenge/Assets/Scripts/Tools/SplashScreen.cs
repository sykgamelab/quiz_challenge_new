﻿using System.Collections;
using UnityEngine;

public class SplashScreen : MonoBehaviour {
	[SerializeField] private AlphaAnimation logo;
	[SerializeField] private GameObject FormsRenderer;
	bool firstUpdate = true;
	bool gameLoaded = false;
    
	Subscription<MessangerEvents> subscription;

	private void OnGameLoadingDone()
	{
		gameLoaded = true;
	}

	private void Awake() =>	subscription = Messanger<MessangerEvents>.Subscribe(MessangerEvents.GameLoadingDone, OnGameLoadingDone);
	private void OnDestroy() => subscription.Unsubscribe();

	private IEnumerator WaitCoroutine()
	{
		yield return new WaitForSeconds(3f);
		yield return new WaitWhile(() => gameLoaded == false);
		logo.T = 1f;
		logo.Target = 0f;
        logo.OnComplete = () =>
        {
            Messanger<bool, MessangerEvents>.SendMessage(MessangerEvents.SetMusicActive, true);
            Messanger<string, MessangerEvents>.SendMessage(MessangerEvents.PlayMusic, SoundClips.Lobby);

            SocialManager.Login();
            gameObject.SetActive(false);
        };
		logo.DoAlpha();
	}

    void Update () {
		if (firstUpdate)
		{
			// gameObject.GetComponent<Canvas>().sortingLayerName = "Foreground";
			AdsManager.Initialize();
			logo.Target = 1f;
			logo.T = 1f;
			logo.DoAlpha();
			logo.OnComplete = () =>
			{
				StartCoroutine(WaitCoroutine());
				FormsRenderer.SetActive(true);
			};
			firstUpdate = false;
        }
	}
}
