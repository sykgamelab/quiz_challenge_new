﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
#if UNITY_IOS
#endif

public class SocialManager : MonoBehaviour
{
	//private static Button btnProgress = Lobby.I.transform.GetChild(5).GetChild(0).GetComponent<Button>();

	public static SocialManager I { get; set; } = null;
	private bool needRetry = false;
	private string[] categories = null;
	private bool isReportInProgress = false, isReportFailed = false;
	private string categoryFailed = null;
	private static SGLTools.SocialManagerInstantiator.SocialManager sm;
	private static bool needLogin = false;

	private IEnumerator ProgressAwaitCoroutine()
	{
		yield return new WaitWhile(() => !ServerManager.ProgressReady);
		Dictionary<string, object> progress = ServerManager.GetProgressToSync();
		if (progress != null)
		{
			int localTimestamp = Helper.GetSyncTimestamp();
			int timestamp = Convert.ToInt32(progress["timestamp"]);
			Debug.Log($"SERVER timestamp local:{localTimestamp} server:{timestamp}");
			if (timestamp > localTimestamp)
			{
				RefinementProgress.I.Show((overwrite) =>
				{
					if (overwrite)
					{
						Bank.I.gameObject.SetActive(false);
						Categories.I.gameObject.SetActive(false);
						CategoryBuyRefinement.I.gameObject.SetActive(false);
						Congratulation.I.gameObject.SetActive(false);
						Error.I.gameObject.SetActive(false);
						FacebookAccount.I.gameObject.SetActive(false);
						FriendAnswer.I.gameObject.SetActive(false);
						Game.I.gameObject.SetActive(false);
						GoToBank.I.gameObject.SetActive(false);
						HallAssistance.I.gameObject.SetActive(false);
						Language.I.gameObject.SetActive(false);
						NeedLoginFacebook.I.gameObject.SetActive(false);
						RefinementAnswer.I.gameObject.SetActive(false);
						RefinementHint.I.gameObject.SetActive(false);
						RefinementProgress.I.gameObject.SetActive(false);
						Settings.I.gameObject.SetActive(false);

						Lobby.I.gameObject.SetActive(true);
						Helper.SetProgress(progress);
						Categories.I.SortCategories();
						Messanger<EventMessages>.SendMessage(EventMessages.UpdateBalance);
						Debug.Log("SET PROGRESS OVERWRITE");
					}

					Helper.SetSyncTimestamp(timestamp);
					ServerManager.ProgressSynchronized = true;
				});
			}
			else
			{
				Debug.Log("SET PROGRESS IGNORE");
				ServerManager.ProgressSynchronized = true;
			}
		}
		else
		{
			Debug.Log("PROGRESS NOT FOUND");
			ServerManager.ProgressSynchronized = true;
		}

	}

	private void Awake()
	{
		I = this;
		categories = new string[SocialConstants.CategoriesAchievementsIDs.Count];
		int i = 0;
		Helper.SetHaveScoreToReport(true);
		sm = SGLTools.SocialManagerInstantiator.GetInstance();
		foreach (var pair in SocialConstants.CategoriesAchievementsIDs)
		{
			Helper.SetAchievementToReport(pair.Key, true);
			categories[i++] = pair.Key;
		}
		StartCoroutine(ProgressAwaitCoroutine());
	}

	private static void UpdateScore()
	{
#if !UNITY_EDITOR
/*      Action<SGLTools.SocialManagerInstantiator.SocialManager.Score[]> callback = (scores) =>
        {
            if (scores != null && scores.Length > 0) Helper.SetRating(false, (int)sm.GetPlayerScore());
            Helper.UnfixButton(btnProgress);
        };
        sm.GetScores(SocialConstants.CoinsTable, callback);*/
#else
		//Helper.UnfixButton(btnProgress);
#endif

		/*Social.LoadScores(coinsTable, (scores) =>
        {
            if (scores.Length > 0) PlayerPrefs.SetInt("rating", (int)scores[0].value);
            Helper.UnfixButton(btnProgress);
        });*/
	}

	public static void Login()
	{
		//Helper.FixButton(btnProgress);

		/*Helper.FixButton(btnProgress);

        if (IsAuthenticated())
        {
            UpdateScore();
            return;
        }

        Social.localUser.Authenticate((bool success) =>
        {
            if (success) UpdateScore();
            else Helper.UnfixButton(btnProgress);
        });*/

		needLogin = true;
	}

	public static bool IsAuthenticated()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable) return false;
		else return sm.Authenticated;
	}

	public static void ShowLeaderboard()
	{
#if UNITY_IOS
		if (!IsAuthenticated())
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("additional", "GameCenterMessage"));
		else sm.ShowLeaderboard(SocialConstants.CoinsTable);
#elif UNITY_ANDROID
		if (!IsAuthenticated())
			Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, LocalizationManager.GetString("additional", "PlayGamesAuth"));
		else sm.ShowLeaderboard(SocialConstants.CoinsTable);
#endif
	}

	public static void ShowAchievements() => sm.ShowAchievements();

	private void ReportAchievement(string category)
	{
		int total = 0;
		for (int i = 0; i < GameConstants.CategoryParts; i++) total += Helper.GetCurrQuestion(category, i);
		string id = SocialConstants.CategoriesAchievementsIDs[category];
		double progress = (double)total / GameConstants.PerCategoryQuestions;
		Debug.Log($"REPORT WITH PROGRESS {progress}");
		sm.ReportAchievementProgress(SGLTools.SocialManagerInstantiator.SocialManager.TypeAchievement.Increment,
			id, progress,
			() =>
			{
				Debug.Log("REPORTED");
				if (sm.LastError != null)
				{
					Debug.Log($"REPORT ACHIEVEMENT {category}:{id} FAILED: {SGLTools.SocialManagerInstantiator.GetInstance().LastError}");
					categoryFailed = category;
					isReportFailed = true;
					isReportInProgress = false;
				}
				else
				{
					Thread.Sleep(2000);
#if UNITY_ANDROID
					//Thread.Sleep(5000);
#elif UNITY_IOS
					//Task.Delay(5000);
#endif
					isReportInProgress = false;
				}

			});
	}

	private void ReportScore()
	{
		Debug.Log("START REPORTING SCORES");
		sm.ReportScore(SocialConstants.CoinsTable, Helper.GetScore(), () =>
		{
			Debug.Log("ReportedScore");
			if (sm.LastError != null)
			{
				needRetry = true;
				Debug.Log("NEED RETRY");
			}
		});
	}

	private void Update()
	{
		if (needLogin && Application.internetReachability != NetworkReachability.NotReachable)
		{
			needLogin = false;
			sm.Authenticate(() =>
			{
				if (sm.LastError != null)
				{
					Debug.Log($"SocialManager.Authenticate: Authenticate FAIL. lastError = {sm.LastError}");

#if UNITY_ANDROID
				string error = "Google Play Games: " + sm.LastError;
#elif UNITY_IOS
					string error = "Gamecenter: " + sm.LastError;
#endif
					Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, error);
				}
				else
				{
					Debug.Log("SocialManager.Authenticate: Authenticate SUCCESS.");
					ServerManager.StartServerClient(Helper.GetTicket(), Helper.GetSyncTimestamp());
					Messanger<EventMessages>.SendMessage(EventMessages.SwitchAuthenticateButtonText);
				}
			});
		}
		if (sm.Authenticated)
		{
			if (needRetry || Helper.GetHaveScoreToReport())
			{
				Helper.SetHaveScoreToReport(false);
				needRetry = false;
				ReportScore();
			}


			if (!isReportInProgress && !isReportFailed)
			{
				foreach (string category in categories)
				{
					if (Helper.HasAchievementToReport(category))
					{
						Debug.Log($"HAS TO REPORT {category}");
						Helper.SetAchievementToReport(category, false);
						isReportInProgress = true;
						ReportAchievement(category);
						break;
					}
				}
			}
			else if (isReportFailed)
			{
				Debug.Log($"FAIL ON {categoryFailed} {sm.LastError}");
				Helper.SetAchievementToReport(categoryFailed, true);
				isReportFailed = false;
			}
		}
	}

	public void SetProgressToReport()
	{
		Dictionary<string, object> progressToReport = Helper.GetProgress();
		int timestamp = Convert.ToInt32(progressToReport["timestamp"]);
		Helper.SetSyncTimestamp(ServerManager.GetSyncTimestamp());
		string ticket = ServerManager.GetTicket();
		int expirationTime = ServerManager.GetTicketExpirationTime();
		Helper.SetTicket(ticket, expirationTime);
		ServerManager.SetProgressToReport(progressToReport);
	}

	private void OnDestroy()
	{
		ServerManager.StopServerClient();
	}

	public static string GetUserID()
	{
		string prefix = "editor_";
#if UNITY_IOS
		prefix = "ios_";
#elif UNITY_ANDROID
		prefix = "android_";
#endif
		return prefix + Social.localUser.id;
	}
}