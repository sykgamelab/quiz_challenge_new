﻿//using Facebook.Unity;
using UnityEngine;
using System.Collections.Generic;

public static class FacebookManager
{
    private static bool initialized = false, failInitialization = false;
    public static string AccountAvatarUrl, AccountFriendAvatarUrl, AccountFullname;

    private static void Initialize()
    {
     /*   if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                    initialized = true;
                    failInitialization = false;
                }
                else
                {
                    initialized = false;
                    failInitialization = true;
                    Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, "Ошибка инициализации Facebook");
                }
            });
        }
        else
        {
            FB.ActivateApp();
            initialized = true;
            failInitialization = false;
        }*/
    }

    private static void SendMessageSettings()
    {
        Messanger<EventMessages>.SendMessage(EventMessages.SwitchFacebookButtonText);
    }

    private static void SaveAccountData()
    {
     /*   FB.API("/me?fields=first_name,last_name,picture", HttpMethod.GET, (IGraphResult result) => {
            IDictionary<string, object> dict = (IDictionary<string, object>) result.ResultDictionary["picture"];
            IDictionary<string, object> data = dict.GetValueOrDefault<IDictionary<string, object>>("data");
            string id = data.GetValueOrDefault<string>("url").Split('=')[1].Split('&')[0];
            AccountAvatarUrl = $"https://graph.facebook.com/{id}/picture?type=large";
            AccountFullname = $"{result.ResultDictionary["first_name"]} {result.ResultDictionary["last_name"]}";

            Messanger<EventMessages>.SendMessage(EventMessages.LoadFacebookData);
        });*/
    }

    public static void SaveFriendAvatarUrl()
    {
        /*FB.AppRequest(title: "Запрос другу", message: "Помоги мне найти правильный ответ в Quiz Challenge!", callback: result =>
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                Debug.Log($"FB.AppRequest: result.Error = {result.Error}");
                Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, result.Error);
            }
            else if (result.Cancelled)
            {
                Debug.Log($"FB.AppRequest: result.Cancelled = {result.Cancelled}");
            }
            else
            {
                string request = result.ResultDictionary.GetValueOrDefault<string>("request");
                if (request == null) return;

#if UNITY_EDITOR
                string id = "100912520867637";
#elif UNITY_IOS
                string id = result.ResultDictionary.GetValueOrDefault<string>("to[0]");
#elif UNITY_ANDROID
                string id = result.ResultDictionary.GetValueOrDefault<string>("to").Split(',')[0];
#endif
                AccountFriendAvatarUrl = $"https://graph.facebook.com/{id}/picture?type=large";

                Messanger<EventMessages>.SendMessage(EventMessages.FacebookSuccessGame);
                Messanger<EventMessages>.SendMessage(EventMessages.LoadFacebookFriendAvatar);
            }
        });*/
    }

    private static void Action(string formName)
    {
        switch (formName)
        {
            case "FacebookAccount":
                Messanger<EventMessages>.SendMessage(EventMessages.FacebookSuccessSettings);
                break;
            case "NeedLoginFacebook":
                SaveFriendAvatarUrl();
                break;
        }
    }

    private static void LogInWithReadPermissions(string formName)
    {
     /*   List<string> perms = new List<string> { "public_profile", "user_friends" };

        FB.LogInWithReadPermissions(perms, (ILoginResult result) =>
        {
            if (FB.IsLoggedIn)
            {
                if (!string.IsNullOrEmpty(result.Error))
                {
                    Debug.Log($"FB.LogInWithReadPermissions: result.Error = {result.Error}");
                    Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, result.Error);
                }
                else if (result.Cancelled)
                {
                    Debug.Log($"FB.LogInWithReadPermissions: result.Cancelled = {result.Cancelled}");
                }
                else
                {
                    Action(formName);
                    if (formName == "FacebookAccount") SaveAccountData();
                }
            }
            else
            {
                Debug.Log($"FB.LogInWithReadPermissions: FB.IsLoggedIn = {FB.IsLoggedIn}");
            }

            SendMessageSettings();
        });*/
    }

    public static bool IsLoggedIn()
    {
		/*   if (initialized)
		   {
			   return FB.IsLoggedIn;
		   }
		   else
		   {
			   Initialize();

			   while (!initialized && !failInitialization) break;

			   if (failInitialization) return false;
			   else return FB.IsLoggedIn;
		   }*/

		return false;
    }

    public static void Login(string formName)
    {
        if (IsLoggedIn())
        {
            Action(formName);
            if (formName == "FacebookAccount") SaveAccountData();
        }
        else
        {
            LogInWithReadPermissions(formName);
        }
    }

    public static void Logout()
    {
    /*    if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                    FB.LogOut();
                    SendMessageSettings();
                }
                else
                {
                    Messanger<string, EventMessages>.SendMessage(EventMessages.ErrorOccured, "Ошибка инициализации Facebook");
                }
            });
        }
        else
        {
            FB.ActivateApp();
            FB.LogOut();
            SendMessageSettings();
        }*/
    }
}
