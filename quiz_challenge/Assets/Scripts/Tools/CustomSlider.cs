﻿using UnityEngine;

public class CustomSlider : MonoBehaviour
{
    public enum SliderType { Horizontal, Vertical };

    public SliderType type = SliderType.Horizontal;

    [Range(0.0f, 1.0f)] public float value;

	[SerializeField] private GameObject handle;
	[SerializeField] private GameObject fill;
	[SerializeField] private Canvas canvas;

	RectTransform fillParentRectTransform, fillRectTransform;
	RectTransform handleRectTransform, handleParentRectTransform;
	float fillSide, fillParentSide, handlePosition, handleSide, handleParentSide;

	private void Start()
	{
		canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        fillParentRectTransform = fill.transform.parent.GetComponent<RectTransform>();
        fillRectTransform = fill.transform.GetComponent<RectTransform>();
        handleParentRectTransform = handle.transform.parent.GetComponent<RectTransform>();
        handleRectTransform = handle.GetComponent<RectTransform>();

        if (type == SliderType.Horizontal)
        {
            fillParentSide = RectTransformUtility.PixelAdjustRect(fillParentRectTransform, canvas).width;
            fillSide = RectTransformUtility.PixelAdjustRect(fillRectTransform, canvas).width;
            handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).width;
            handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).width;
        }
        else if (type == SliderType.Vertical)
        {
            fillParentSide = RectTransformUtility.PixelAdjustRect(fillParentRectTransform, canvas).height;
            fillSide = RectTransformUtility.PixelAdjustRect(fillRectTransform, canvas).height;
            handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).height;
            handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).height;
        }
	}

	private void Update()
	{
        fillParentRectTransform = fill.transform.parent.GetComponent<RectTransform>();
        fillRectTransform = fill.transform.GetComponent<RectTransform>();
        handleParentRectTransform = handle.transform.parent.GetComponent<RectTransform>();
        handleRectTransform = handle.GetComponent<RectTransform>();
        Vector2 fillSizeDelta = fillRectTransform.sizeDelta;
        float deltaSize, deltaPosition, currFillSide, newFillSide, newHandlePosition;

        if (type == SliderType.Horizontal)
        {
            fillParentSide = RectTransformUtility.PixelAdjustRect(fillParentRectTransform, canvas).width;
            currFillSide = RectTransformUtility.PixelAdjustRect(fillRectTransform, canvas).width;
            handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).width;
            handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).width;
            
            deltaSize = value * (fillParentSide - fillSide);
            newFillSide = fillSide + deltaSize;
            newFillSide = newFillSide < 0 ? 0 : newFillSide;
            deltaPosition = Mathf.Floor((newFillSide - currFillSide) / 2f + 0.5f);
            fillRectTransform.sizeDelta = new Vector2(newFillSide, fillSizeDelta.y);
            Vector3 fillPosition = fill.transform.localPosition;
            Vector3 newFillPosition = new Vector3(fillPosition.x + deltaPosition, fillPosition.y, fillPosition.z);
            fill.transform.localPosition = newFillPosition;

            Vector3 handlePosition = handle.transform.localPosition;
            newHandlePosition = Mathf.Floor((handleSide - handleParentSide) / 2f + value * (handleParentSide - handleSide) + 0.5f);
            handle.transform.localPosition = new Vector3(newHandlePosition, handlePosition.y, handlePosition.z);
        }
        else if (type == SliderType.Vertical)
        {
            fillParentSide = RectTransformUtility.PixelAdjustRect(fillParentRectTransform, canvas).height;
            currFillSide = RectTransformUtility.PixelAdjustRect(fillRectTransform, canvas).height;
            handleParentSide = RectTransformUtility.PixelAdjustRect(handleParentRectTransform, canvas).height;
            handleSide = RectTransformUtility.PixelAdjustRect(handleRectTransform, canvas).height;

            deltaSize = value * (fillParentSide - fillSide);
            newFillSide = fillSide + deltaSize;
            newFillSide = newFillSide < 0 ? 0 : newFillSide;
            deltaPosition = Mathf.Floor((newFillSide - currFillSide) / 2f + 0.5f);
            fillRectTransform.sizeDelta = new Vector2(fillSizeDelta.x, newFillSide);
            Vector3 fillPosition = fill.transform.localPosition;
            Vector3 newFillPosition = new Vector3(fillPosition.x, fillPosition.y + deltaPosition, fillPosition.z);
            fill.transform.localPosition = newFillPosition;

            Vector3 handlePosition = handle.transform.localPosition;
            newHandlePosition = Mathf.Floor((handleSide - handleParentSide) / 2f + value * (handleParentSide - handleSide) + 0.5f);
            handle.transform.localPosition = new Vector3(handlePosition.x, newHandlePosition, handlePosition.z);
        }
    }
}
