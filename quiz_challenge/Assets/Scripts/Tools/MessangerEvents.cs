﻿public enum MessangerEvents
{
	PlayUI,
	SetSoundActive,
	SetMusicActive,
	PlayAnswerEffect,
	UpdateAnchors,
    PlayMusic,
	GameLoadingDone,
    SetProgressServer,
    GetProgressServer
}