﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public static class AdsManager
{
	static InterstitialAd iadVideo, iadNoVideo;
	static Action onCloseCallback;
	static AdRequest request;

	private static void OnAdClosedVideo(object sender, EventArgs args)
	{
		iadVideo.LoadAd(request);
		onCloseCallback?.Invoke();
	}

	private static void OnAdClosedNoVideo(object sender, EventArgs args)
	{
		iadNoVideo.LoadAd(request);
		onCloseCallback?.Invoke();
	}

	public static void Initialize()
	{
		MobileAds.Initialize(AdsConstants.AppID);
		iadVideo = new InterstitialAd(AdsConstants.InterstitialVideo);
		iadNoVideo = new InterstitialAd(AdsConstants.InterstitialNoVideo);
		iadVideo.OnAdClosed += OnAdClosedVideo;
		iadNoVideo.OnAdClosed += OnAdClosedNoVideo;

		request = new AdRequest.Builder().Build();
		iadNoVideo.LoadAd(request);
		iadVideo.LoadAd(request);
	}

	public static void ShowVideo(Action callback)
	{
		onCloseCallback = callback;
		if (iadVideo.IsLoaded()) iadVideo.Show();
		else callback?.Invoke();
	}

	public static void ShowNoVideo(Action callback)
	{
		onCloseCallback = callback;
		if (iadNoVideo.IsLoaded()) iadNoVideo.Show();
		else callback?.Invoke();
	}
}
