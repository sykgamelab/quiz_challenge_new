﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionTracker : MonoBehaviour
{
	private IYandexAppMetrica appmetrica;

	// Start is called before the first frame update

	private IEnumerator SessionCoroutine()
	{
		int time = 0;
		while (true)
		{
			yield return new WaitForSeconds(1);
			time += 1;
			Helper.SetSessionTime(time);
		}
	}

	private void Start()
	{
		appmetrica = AppMetrica.Instance;
		int time = Helper.GetSessionTime();
		if (time != 0)
		{
			appmetrica.ReportEvent(MetricaConstants.Session, new Dictionary<string, object>()
			{
				{ MetricaConstants.Duration, (int)(time / 60f + 0.5f)}
			});
			Helper.SetSessionTime(0);
		}
		StartCoroutine(SessionCoroutine());
	}

	// Update is called once per frame
	private void Update()
	{

	}
}
