#import <GameKit/GameKit.h>
#import <Foundation/Foundation.h>
#import <stdlib.h>
#import <string.h>

@interface SocialManagerViewController : UIViewController<GKGameCenterControllerDelegate>

+ (SocialManagerViewController *)instance;
- (void)presentGKViewController:(GKGameCenterViewController *)controller leaderboardID:(NSString *)leaderboardID;
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)controller;
- (void)presentAchievementsGKViewController:(GKGameCenterViewController *)controller;

@property (nonatomic, strong) UIViewController *rootViewController;

@end

@implementation SocialManagerViewController

@synthesize rootViewController;

+ (SocialManagerViewController *)instance {
    static SocialManagerViewController *instance = nil;
    if (!instance) {
        instance = [SocialManagerViewController new];
        instance.rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    }
    return instance;
}

- (void)presentGKViewController:(GKGameCenterViewController *)controller leaderboardID:(NSString *)leaderboardID {
    controller.gameCenterDelegate = self;
    controller.viewState = GKGameCenterViewControllerStateLeaderboards;
    controller.leaderboardTimeScope = GKLeaderboardTimeScopeAllTime;
    controller.leaderboardIdentifier = leaderboardID;
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    [rootViewController presentViewController:controller animated:YES completion:nil];
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)controller {
    [controller dismissViewControllerAnimated:true completion:nil];
}

- (void)presentAchievementsGKViewController:(GKGameCenterViewController *)controller {
    controller.gameCenterDelegate = self;
    controller.viewState = GKGameCenterViewControllerStateAchievements;
    [controller dismissViewControllerAnimated:YES completion:nil];
    [rootViewController presentViewController:controller animated:YES completion:nil];
}
@end

@interface VerificationSignature : NSObject

@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSData *signature;
@property (strong, nonatomic) NSData *salt;
@property uint64_t timestamp;
@property (strong, nonatomic) NSString *userID;

@end

@implementation VerificationSignature

@synthesize url;
@synthesize signature;
@synthesize salt;
@synthesize timestamp;
@synthesize userID;

@end

@interface SocialManager : NSObject

+ (SocialManager *)instance;
- (BOOL)isAuthenticated;
- (BOOL)isAuthenticateCancelled;
- (void)authenticate;
- (void)showLeaderboard:(NSString *)leaderboardID;
- (void)loadPlayerScore:(NSString *)leaderboardID;
- (void)reportScore:(int64_t)score leaderboardID:(NSString *)leaderboardID;
- (BOOL)scoreReady;
- (BOOL)isScoreReported;
- (void)generateIdentityVerificationSignature;
- (BOOL)isVerificationSignatureReady;
- (VerificationSignature *)getVerificationSignature;
- (BOOL)isAchievementsReady;
- (BOOL)isAchievementReported;
- (void)showAchievements;
- (void)loadAchievements;
- (void)reportAchievement:(double)progress achievementID:(NSString *)achievementID;
- (void)resetAchievements;

@property (strong,  nonatomic) NSString *lastError;
@property BOOL isAuthenticationDone;
@property (strong, nonatomic) UIViewController *authView;
@property (strong, nonatomic) NSString *playerID;
@property int64_t playerScore;
@property (strong, nonatomic) NSArray *achievements;

@end

@implementation SocialManager {
    BOOL isAuthenticateCancelled;
    BOOL scoreReady;
    BOOL isScoreReported;
    BOOL isVerificationSignatureReady;
    VerificationSignature *verificationSignature;
    BOOL isAchievementsReady;
    BOOL isAchievementReported;
}

@synthesize lastError;
@synthesize isAuthenticationDone;
@synthesize authView;
@synthesize playerID;
@synthesize playerScore;
@synthesize achievements;

+ (SocialManager *)instance {
    static SocialManager *instance = nil;
    if (!instance) {
        instance = [SocialManager new];
        instance.lastError = nil;
        instance.isAuthenticationDone = FALSE;
        instance.playerID = nil;
        instance.playerScore = 0;
    }
    return instance;
}

- (BOOL)isAuthenticateCancelled {
    return isAuthenticateCancelled;
}

- (BOOL)scoreReady {
    return scoreReady;
}

- (BOOL)isScoreReported {
    return isScoreReported;
}

- (BOOL)isVerificationSignatureReady {
    return isVerificationSignatureReady;
}

- (BOOL)isAuthenticated {
    return [GKLocalPlayer localPlayer].isAuthenticated;
}

- (void)authenticate {
    self.lastError = nil;
    isAuthenticateCancelled = FALSE;
    NSLog(@"NATIVE AUTH CALL");
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    UIViewController *rootViewController = [SocialManagerViewController instance].rootViewController;
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error) {
        if (error != nil) {
            self.lastError = error.localizedDescription;
            if (error.code == 2) isAuthenticateCancelled = TRUE;
        } else if (viewController) {
            [rootViewController presentViewController:viewController animated:YES completion:^{
                self.authView = viewController;
            }];
        }
        
        isAuthenticationDone = TRUE;
    };
}

- (void)showLeaderboard:(NSString *)leaderboardID {
    self.lastError = nil;
    if (self.isAuthenticated) {
        @autoreleasepool {
            GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
            SocialManagerViewController *smViewController = [SocialManagerViewController instance];
            [smViewController presentGKViewController:gcViewController leaderboardID:leaderboardID];
        }
    }
}

- (void)loadPlayerScore:(NSString *)leaderboardID {
    scoreReady = FALSE;
    self.lastError = nil;
    self.playerID = nil;
    self.playerScore = 0;
    GKLeaderboard *leaderboardRequest = [[GKLeaderboard alloc] init];
    if (leaderboardRequest != nil) {
        leaderboardRequest.playerScope = GKLeaderboardPlayerScopeGlobal;
        leaderboardRequest.identifier = leaderboardID;
        [leaderboardRequest loadScoresWithCompletionHandler:^(NSArray *scores, NSError *error) {
            if (error != nil) {
                self.lastError = error.localizedDescription;
                NSLog(@"loadPlayerScore: error = %@", self.lastError);
            } else if (scores != nil) {
            	self.playerID = [GKLocalPlayer localPlayer].playerID;
                self.playerScore = leaderboardRequest.localPlayerScore.value;

                NSLog(@"loadPlayerScore -> SUCCESS: playerID = %@, playerScore = %lld", self.playerID, self.playerScore);
            }
            
            scoreReady = TRUE;
        }];
    }
}

- (void)reportScore:(int64_t)score leaderboardID:(NSString *)leaderboardID {
    GKScore *gkScore = [[GKScore alloc] initWithLeaderboardIdentifier:(NSString *)leaderboardID];
    gkScore.value = score;
    gkScore.context = 0;
    self.lastError = nil;
    isScoreReported = FALSE;
    NSArray *scores = @[gkScore];
    [GKScore reportScores:scores withCompletionHandler:^(NSError *error) {
        if (error != nil) {
            self.lastError = error.localizedDescription;
        }
        else {
            NSLog(@"[GKScore reportScores] -> SUCCESS");
        }

        isScoreReported = TRUE;
    }];
}

- (VerificationSignature *)getVerificationSignature {
    return verificationSignature;
}

- (void)generateIdentityVerificationSignature {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    isVerificationSignatureReady = FALSE;
    
    if (localPlayer.isAuthenticated) {
        [localPlayer generateIdentityVerificationSignatureWithCompletionHandler:^(NSURL *url, NSData *signature, NSData *salt, uint64_t timestamp, NSError *error) {
            verificationSignature = [VerificationSignature new];
            if(error != nil)
            {
                self.lastError = error.localizedDescription;
                verificationSignature = nil;
            } else {
                verificationSignature.url = url;
                verificationSignature.signature = signature;
                verificationSignature.salt = salt;
                verificationSignature.timestamp = timestamp;
                verificationSignature.userID = localPlayer.playerID;
            }
            isVerificationSignatureReady = TRUE;
        }];
    } else {
        self.lastError = @"Not Authenticated";
        isVerificationSignatureReady = TRUE;
    }
}

- (BOOL)isAchievementsReady {
    return isAchievementsReady;
}

- (BOOL)isAchievementReported {
    return isAchievementReported;
}

- (void)showAchievements {
    self.lastError = nil;
    if (self.isAuthenticated) {
        @autoreleasepool {
            GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
            SocialManagerViewController *smViewController = [SocialManagerViewController instance];
            [smViewController presentAchievementsGKViewController:gcViewController];
        }
    }
}

- (void)loadAchievements {
    isAchievementsReady = FALSE;
    self.lastError = nil;
    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements, NSError *error) {
        if (error != nil) {
            self.lastError = error.localizedDescription;
        } else if (achievements != nil) {
            self.achievements = achievements;
            NSLog(@"[GKAchievement loadAchievementsWithCompletionHandler] -> SUCCESS");
        }
        isAchievementsReady = TRUE;
    }];
}

- (void)reportAchievement:(double)progress achievementID:(NSString *)achievementID {
    GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier: achievementID];
    isAchievementReported = FALSE;
    self.lastError = nil;
    progress *= 100;
    if (progress < 0) progress = 0;
    else if (progress > 100) progress = 100;
    if (achievement && achievement.percentComplete < progress) {
        achievement.showsCompletionBanner = YES;
        achievement.percentComplete = progress;
        [GKAchievement reportAchievements:@[achievement] withCompletionHandler:^(NSError *error) {
			if (error != nil) self.lastError = error.localizedDescription;
            isAchievementReported = TRUE;
        }];
    } else {
        isAchievementReported = TRUE;
    }
}

- (void)resetAchievements {
    [GKAchievement resetAchievementsWithCompletionHandler:^(NSError *error) {
		if (error != nil)
			self.lastError = error.localizedDescription;
    }];
}
@end

struct NativeVerificationSignature {
    unsigned char *url;
    int urlSize;
    unsigned char *signature;
    int signatureSize;
    unsigned char *salt;
    int saltSize;
    uint64_t timestamp;
    unsigned char *userID;
    int userIDSize;
};

BOOL isAuthenticatedNative() {
    return [[SocialManager instance] isAuthenticated];
}

void authenticateNative() {
    [[SocialManager instance] authenticate];
}

BOOL isAuthenticateCancelledNative() {
    return [[SocialManager instance] isAuthenticateCancelled];
}

const char* getLastErrorNative() {
    char *error = NULL;
    NSString *lastError = [[SocialManager instance] lastError];
    if (lastError) {
        const char *cherror = [lastError UTF8String];
        size_t len = strlen(cherror);
        error = malloc(sizeof(char) * (len +1 ));
        memcpy(error, cherror, len + 1);
    }
    return error;
}

const char* getPlayerIDNative() {
	char *playerID = NULL;
    NSString *nsPlayerID = [[SocialManager instance] playerID];
    if (nsPlayerID) {
        const char *chPlayerID = [nsPlayerID UTF8String];
        size_t len = strlen(chPlayerID);
        playerID = malloc(sizeof(char) * (len +1 ));
        memcpy(playerID, chPlayerID, len + 1);
    }
    return playerID;
}

int64_t getPlayerScoreNative() {
	return [[SocialManager instance] playerScore];
}

void showLeaderboardNative(char *leaderboardID) {
    NSString *string = [[NSString alloc] initWithUTF8String:leaderboardID];
    [[SocialManager instance] showLeaderboard: string];
}

void loadPlayerCurrentScoreNative(char *leaderboardID) {
    NSString *string = [[NSString alloc] initWithUTF8String:leaderboardID];
    [[SocialManager instance] loadPlayerScore:string];
}

BOOL isScoreReadyNative() {
	BOOL ready = [[SocialManager instance] scoreReady];
	NSLog(@"isScoreReadyNative: ready = %s", ready ? "TRUE" : "FALSE");
    return ready;
}

BOOL isAuthenticationDoneNative() {
    BOOL done = FALSE;
    SocialManager *mgr = [SocialManager instance];
    if (mgr.isAuthenticationDone && mgr.lastError != nil)
        done = TRUE;
    else if (mgr.isAuthenticationDone && (mgr.authView.isViewLoaded && mgr.authView.view.window) && mgr.lastError == nil)
        done = FALSE;
    else if (mgr.isAuthenticationDone && !(mgr.authView.isViewLoaded && mgr.authView.view.window) && mgr.lastError == nil) {
        mgr.authView = nil;
        done = TRUE;
    }
    
    return done;
}

void reportScoreNative(int64_t score, char *leaderboardID) {
    NSString *nsLeaderboardID = [[NSString alloc] initWithUTF8String:leaderboardID];
    [[SocialManager instance] reportScore:score leaderboardID:nsLeaderboardID];
}

BOOL isScoreReportedNative() {
    return [[SocialManager instance] isScoreReported];
}

void generateIdentityVerificationSignatureNative() {
    [[SocialManager instance] generateIdentityVerificationSignature];
}

struct NativeVerificationSignature getVerificationSignatureNative() {
    SocialManager *sm = [SocialManager instance];
    struct NativeVerificationSignature data;
    
    VerificationSignature *sig = sm.getVerificationSignature;
    if (sig != nil) {
        NSData *nsurl = [sig.url.absoluteString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        int nsurllen = (int)[nsurl length];
        data.url = malloc(sizeof(unsigned char) * (nsurllen + 1));
        data.urlSize = nsurllen;
        memcpy(data.url, [nsurl bytes], nsurllen + 1);
        
        int siglen = (int)[sig.signature length];
        data.signature = malloc(sizeof(unsigned char) * siglen);
        data.signatureSize = siglen;
        memcpy(data.signature, [sig.signature bytes], siglen);
        
        int saltlen = (int)[sig.salt length];
        data.salt = malloc(sizeof(unsigned char) * saltlen);
        data.saltSize = saltlen;
        memcpy(data.salt, [sig.salt bytes], saltlen);
        
        data.timestamp = sig.timestamp;

        NSData *nsuserid = [sig.userID dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        int nsuseridlen = (int)[nsuserid length];
        data.userID = malloc(sizeof(unsigned char) * (nsuseridlen + 1));
        data.userIDSize = nsuseridlen;
        memcpy(data.userID, [nsuserid bytes], nsuseridlen + 1);
    }
    return data;
}

BOOL isVerificationSignatureReadyNative() {
    return [[SocialManager instance] isVerificationSignatureReady];
}

void freeNative(unsigned char *ptr) {
    free(ptr);
}

BOOL isAchievementsReadyNative() {
    return [[SocialManager instance] isAchievementsReady];
}

BOOL isAchievementReportedNative() {
    return [[SocialManager instance] isAchievementReported];
}

void showAchievementsNative() {
	[[SocialManager instance] showAchievements];
}

void loadAchievementsNative() {
	[[SocialManager instance] loadAchievements];
}

struct NativeAchievement {
    char *achievementID;
    int achievementIDSize;
    double percent;
};

struct NativeAchievement* getAchievementsNative() {
    NSLog(@"START getAchievementsNative from iOS");
    struct NativeAchievement *achievements = NULL;
    SocialManager *manager = [SocialManager instance];
    if (manager.achievements != nil) {
        achievements = malloc(sizeof(struct NativeAchievement) * manager.achievements.count);
        if (achievements == NULL) {
            manager.lastError = @"Not enough memory";
            goto end;
        }
        
        for (int i = 0; i < manager.achievements.count; i++) {
            GKAchievement *gkAchievement = (GKAchievement *)[manager.achievements objectAtIndex:i];
            achievements[i].percent = [gkAchievement percentComplete];
            NSString *nsuid = [gkAchievement identifier];
            const char *chuid = [nsuid UTF8String];
            size_t len = strlen(chuid);
            achievements[i].achievementID = malloc(sizeof(char) * (len + 1));
            achievements[i].achievementIDSize = len + 1;
            memcpy(achievements[i].achievementID, chuid, len + 1);
        }
    }
end: return achievements;
}

void reportAchievementProgressNative(const char *achievementID, double progress) {
    NSLog(@"START reportAchievementProgressNative");
	NSString *string = [[NSString alloc] initWithUTF8String:achievementID];
    [[SocialManager instance] reportAchievement:progress achievementID:string];
}

void resetAchievementsForTestersNative() {
	[[SocialManager instance] resetAchievements];
}
