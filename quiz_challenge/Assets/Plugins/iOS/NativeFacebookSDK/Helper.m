#import "Helper.h"

@implementation Helper

static UINavigationController *navController;
static NSString *sendedIDFriend;
static pthread_t *mypthreads;
static int numThreads;

+ (UINavigationController *)getNavController {
    return navController;
}

+ (void)setNavController:(UINavigationController *)controller {
    navController = controller;
}

+ (NSString *)getSendedIDFriend {
    return sendedIDFriend;
}

+ (void)setSendedIDFriend:(NSString *)idFriend {
    sendedIDFriend = idFriend;
}

+ (pthread_t *)getPThreads {
    return mypthreads;
}

+ (void)setPThreads:(pthread_t *)pthreads {
    mypthreads = pthreads;
}

@end
