#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
#import "FriendListViewController.h"
#import "Helper.h"
#import <pthread.h>

void *threadLoadImage();

@interface FBSDKManager : NSObject

+ (FBSDKManager *)instance;
- (void)login:(NSArray *)permissions alertTitleError:(NSString *)title;
- (void)logout;
- (BOOL)isLoggedIn;
- (BOOL)isLoginDone;
- (BOOL)isAuthenticationSuccess;
- (BOOL)isAuthenticationCancel;
- (void)graphRequest:(NSString *)graphPath alertTitleError:(NSString *)title callback:(void(^)(void))callback;
- (BOOL)isGraphRequestDone;
- (void)showFriendList:(NSString *)graphPath ids:(NSString *)ids alertTitleError:(NSString *)alertTitleError alertTitleInfo:(NSString *)alertTitleInfo 
	alertMessage:(NSString *)alertMessage appRequestMessage:(NSString *)appRequestMessage alertButtonYesText:(NSString *)alertButtonYesText
    alertButtonNoText:(NSString *)alertButtonNoText buttonSendText:(NSString *)buttonSendText;
- (void)createUIFriendList:(NSArray *)data ids:(NSString *)ids alertTitle:(NSString *)alertTitle alertMessage:(NSString *)alertMessage
         appRequestMessage:(NSString *)appRequestMessage alertButtonYesText:(NSString *)alertButtonYesText
         alertButtonNoText:(NSString *)alertButtonNoText buttonSendText:(NSString *)buttonSendText;
- (void)onClickButtonSend:(UIButton *)button;
- (void)showAlert:(NSString *)title message:(NSString *)message numButtons:(int)n buttonYesText:(NSString *)yesText
     buttonNoText:(NSString *)noText appRequestID:(NSString *)friendID appRequestMessage:(NSString *)appRequestMessage;
- (void)appRequest:(NSString *)userID message:(NSString *)message alertTitleError:(NSString *)title;
- (void)createImageThreads:(NSArray *)data numFriends:(int)n;
- (void)changeImageView:(NSDictionary *)data;
- (void)loadChangeImageView:(NSDictionary *)data;

@property (nonatomic, strong) NSString *lastError;
@property (nonatomic, strong) NSString *graphResponse;

@end

@implementation FBSDKManager {
    BOOL authenticationSuccess;
    BOOL authenticationCancel;
    BOOL isLoginDone;
    BOOL isGraphRequestDone;
}

@synthesize lastError;
@synthesize graphResponse;

+ (FBSDKManager *)instance {
    static FBSDKManager *instance = nil;
    if (!instance) {
        instance = [FBSDKManager new];
        instance.lastError = nil;
        instance.graphResponse = nil;
    }
    return instance;
}

- (void)login:(NSMutableArray *)permissions alertTitleError:(NSString *)title {
    lastError = nil;
    isLoginDone = false;
    authenticationSuccess = false;
    authenticationCancel = false;
    if (!self.isLoggedIn) {
        UIViewController *controller = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        @autoreleasepool {
            FBSDKLoginManager *loginManager = [FBSDKLoginManager new];
            [loginManager logInWithReadPermissions:permissions
                                fromViewController:(controller)
                                           handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                               if (error) {
                                                   lastError = error.localizedDescription;
                                                   NSLog(@"FBSDKManager.login -> ERROR: %@", lastError);
                                                   [self showAlert:title message:lastError numButtons:1 buttonYesText:nil buttonNoText:nil appRequestID:nil appRequestMessage:nil];
                                               } else if (result.isCancelled) {
                                                   authenticationCancel = true;
                                                   NSLog(@"FBSDKManager.login -> CANCELLED");
                                               } else {
                                                   authenticationSuccess = true;
                                                   NSLog(@"FBSDKManager.login -> SUCCESS");
                                               }
                                               isLoginDone = true;
                                           }];
        }
    }
}

- (void)logout {
    @autoreleasepool {
        [[FBSDKLoginManager new] logOut];
    }
}

- (BOOL)isLoggedIn {
    return [FBSDKAccessToken currentAccessToken];
}

- (BOOL)isLoginDone {
    return isLoginDone;
}

- (BOOL)isAuthenticationSuccess {
    return authenticationSuccess;
}

- (BOOL)isAuthenticationCancel {
    return authenticationCancel;
}

- (void)graphRequest:(NSString *)graphPath alertTitleError:(NSString *)title callback:(void(^)(void))callback {
    NSLog(@"FBSDKManager.graphRequest: graphPath = %@", graphPath);
    isGraphRequestDone = false;
    lastError = nil;
    graphResponse = nil;
    
    @autoreleasepool {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:graphPath parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (error) {
                lastError = error.localizedDescription;
                NSLog(@"FBSDKManager.graphRequest -> ERROR: %@, code = %ld", lastError, [error code]);
                if ([error code] != -1009) {
                    [self showAlert:title message:lastError numButtons:1 buttonYesText:nil buttonNoText:nil appRequestID:nil appRequestMessage:nil];
                } else {
                    [Helper setSendedIDFriend:@"error_connection"];
                    [[[UnityGetGLView() subviews] lastObject] removeFromSuperview];
                }
            } else {
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
                if (!jsonData) {
                    lastError = error.localizedDescription;
                    NSLog(@"FBSDKManager.graphRequest -> ERROR: %@", lastError);
                    [self showAlert:title message:lastError numButtons:1 buttonYesText:nil buttonNoText:nil appRequestID:nil appRequestMessage:nil];
                } else {
                    graphResponse = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    NSLog(@"FBSDKManager.graphRequest -> SUCCESS: graphResponse = %@", graphResponse);
                    callback();
                }
            }
            
            isGraphRequestDone = true;
        }];
    }
}

- (BOOL)isGraphRequestDone {
    return isGraphRequestDone;
}

- (void)showFriendList:(NSString *)graphPath ids:(NSString *)ids alertTitleError:(NSString *)alertTitleError alertTitleInfo:(NSString *)alertTitleInfo 
	alertMessage:(NSString *)alertMessage appRequestMessage:(NSString *)appRequestMessage alertButtonYesText:(NSString *)alertButtonYesText
    alertButtonNoText:(NSString *)alertButtonNoText buttonSendText:(NSString *)buttonSendText {
    [Helper setSendedIDFriend:nil];
        NSLog(@"FBSDKManager.showFriendList: graphPath = %@, ids = %@", graphPath, ids);
        
        [self graphRequest:graphPath alertTitleError:alertTitleError callback:^{
            NSData *data = [self.graphResponse dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSArray *arrData = [json objectForKey:@"data"];
            [self createUIFriendList:arrData ids:ids alertTitle:alertTitleInfo alertMessage:alertMessage appRequestMessage:appRequestMessage
                  alertButtonYesText:alertButtonYesText alertButtonNoText:alertButtonNoText buttonSendText:buttonSendText];
        }];
}

- (void)createUIFriendList:(NSArray *)data ids:(NSString *)ids alertTitle:(NSString *)alertTitle alertMessage:(NSString *)alertMessage
         appRequestMessage:(NSString *)appRequestMessage alertButtonYesText:(NSString *)alertButtonYesText
         alertButtonNoText:(NSString *)alertButtonNoText buttonSendText:(NSString *)buttonSendText {
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    float screenHeight = [UIScreen mainScreen].bounds.size.height;
    float margin = 40;
    float imageWidth = 80;
    float imageHeight = 80;
    float labelWidth = 0.5 * screenWidth - 2 * margin;
    float labelHeight = 80;
    float buttonWidth = 100;
    float buttonHeight = 60;
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    scrollView.backgroundColor = [UIColor whiteColor];
    int numFriends = (int)[data count];
    NSMutableArray *imagesData = [[NSMutableArray alloc] initWithCapacity:numFriends];
    for (int i = 0; i < numFriends; i++) {
        NSDictionary *item = [data objectAtIndex:0];
        NSString *friendID = [item objectForKey:@"id"];
        if (![ids containsString:friendID]) continue;
        
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", [item objectForKey:@"first_name"], [item objectForKey:@"last_name"]];
        NSString *url = [[[item objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(margin, margin + (margin + imageHeight) * i, imageWidth, imageHeight)];
        imageView.backgroundColor = [UIColor blackColor];
        [scrollView addSubview:imageView];
        
        NSDictionary *dict = @{@"imageView":imageView, @"url":url};
        [imagesData addObject:dict];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(2 * margin + imageWidth, margin + (margin + labelHeight) * i, labelWidth, labelHeight)];
        label.text = fullName;
        label.font = [label.font fontWithSize:25];
        [scrollView addSubview:label];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(screenWidth - margin - buttonWidth, margin + margin * 1/4 + (margin + labelHeight) * i, buttonWidth, buttonHeight);
        button.backgroundColor = [[UIColor alloc] initWithRed:0.258 green:0.403 blue:0.698 alpha:1.0];
        [button setTitle:buttonSendText forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(onClickButtonSend:) forControlEvents:UIControlEventTouchUpInside];
        [button.layer setValue:friendID forKey:@"friendID"];
        [button.layer setValue:alertTitle forKey:@"alertTitle"];
        [button.layer setValue:alertMessage forKey:@"alertMessage"];
        [button.layer setValue:appRequestMessage forKey:@"appRequestMessage"];
        [button.layer setValue:alertButtonYesText forKey:@"alertButtonYesText"];
        [button.layer setValue:alertButtonNoText forKey:@"alertButtonNoText"];
        [scrollView addSubview:button];
    }
    
    [self createImageThreads:imagesData numFriends:numFriends];
    
    scrollView.contentSize = CGSizeMake(screenWidth, numFriends * (imageHeight + margin) + margin);
    FriendListViewController *friendListVC = [[FriendListViewController alloc] initWithNibName:nil bundle:nil];
    [friendListVC.view addSubview:scrollView];
    UINavigationController *navController = [Helper getNavController];
    [navController pushViewController:friendListVC animated:YES];
    [UnityGetGLView() addSubview:navController.view];
}

- (void)createImageThreads:(NSArray *)data numFriends:(int)n {
     int processorCount = (int)[[NSProcessInfo processInfo] processorCount];
     pthread_t *pthreads = malloc(sizeof(pthread_t) * processorCount);
     [Helper setPThreads:pthreads];
    
     pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
     for (int i = 0; i < processorCount; i++) {
         NSRange range;
         if (i == processorCount-1 && n % processorCount != 0) {
             range = NSMakeRange(i * n / processorCount, n / processorCount + n % processorCount);
         } else {
             range = NSMakeRange(i * n / processorCount, n / processorCount);
         }
         
         NSArray *arr = [data subarrayWithRange:range];
         pthread_t pthread;
         pthread_create(&pthread, NULL, threadLoadImage, (__bridge void *)arr);
         pthreads[i] = pthread;
     }
 }

- (void)changeImageView:(NSDictionary *)data {
    if (data) {
        UIImage *image = [data valueForKey:@"image"];
        UIImageView *imageView = [data valueForKey:@"imageView"];
        imageView.image = image;
        imageView.layer.cornerRadius = imageView.frame.size.height / 2;
        imageView.clipsToBounds = YES;
    } else {
        [Helper setSendedIDFriend:@"error_connection"];
        [[[UnityGetGLView() subviews] lastObject] removeFromSuperview];
    }
}

- (void)loadChangeImageView:(NSDictionary *)data {
    [self performSelectorOnMainThread:@selector(changeImageView:) withObject:data waitUntilDone:NO];
}

- (void)onClickButtonSend:(UIButton *)button {
    NSString *friendID = (NSString *)[button.layer valueForKey:@"friendID"];
    NSString *alertTitle = (NSString *)[button.layer valueForKey:@"alertTitle"];
    NSString *alertMessage = (NSString *)[button.layer valueForKey:@"alertMessage"];
    NSString *appRequestMessage = (NSString *)[button.layer valueForKey:@"appRequestMessage"];
    NSString *alertButtonYesText = (NSString *)[button.layer valueForKey:@"alertButtonYesText"];
    NSString *alertButtonNoText = (NSString *)[button.layer valueForKey:@"alertButtonNoText"];
    
    [self showAlert:alertTitle message:alertMessage numButtons:2 buttonYesText:alertButtonYesText
       buttonNoText:alertButtonNoText appRequestID:friendID appRequestMessage:appRequestMessage];
}

- (void)showAlert:(NSString *)title message:(NSString *)message numButtons:(int)n
	buttonYesText:(NSString *)yesText buttonNoText:(NSString *)noText appRequestID:(NSString *)friendID appRequestMessage:(NSString *)appRequestMessage {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (n == 1) {
        [Helper setSendedIDFriend:@""];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [controller addAction:actionOk];
    } else if (n == 2) {
        UIAlertAction *actionYes = [UIAlertAction actionWithTitle:yesText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self appRequest:friendID message:appRequestMessage alertTitleError:title];
            [[[UnityGetGLView() subviews] lastObject] removeFromSuperview];
        }];
        UIAlertAction *actionNo = [UIAlertAction actionWithTitle:noText style:UIAlertActionStyleDefault handler:nil];
        [controller addAction:actionYes];
        [controller addAction:actionNo];
    }
    
    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [rootViewController presentViewController:controller animated:YES completion:nil];
}

- (void)appRequest:(NSString *)userID message:(NSString *)message alertTitleError:(NSString *)title {
    NSString *encodedMessage = [message stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *graphPath = [NSString stringWithFormat:@"/%@/apprequests?action_type=SEND&message=%@", userID, encodedMessage];
    NSLog(@"FBSDKManager.appRequest: graphPath = %@", graphPath);
    
    [self graphRequest: graphPath alertTitleError:title callback:^{
        NSString *response = [self graphResponse];
        if ([response containsString:@"data"]) {
            [Helper setSendedIDFriend:userID];
        } else {
            [Helper setSendedIDFriend:@""];
        }
    }];
}
@end

void loginNative(char *permissions[], int length, char *alertTitleError) {
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:length];
    for (int i = 0; i < length; i++) {
        [arr addObject:[NSString stringWithCString:permissions[i] encoding:NSASCIIStringEncoding]];
    }
    NSString *nsAlertTitleError = nil;
    if (alertTitleError != NULL) nsAlertTitleError = [[NSString alloc] initWithUTF8String:alertTitleError];
    [[FBSDKManager instance] login:arr alertTitleError:nsAlertTitleError];
}

void logoutNative() {
    [[FBSDKManager instance] logout];
}

BOOL isLoggedInNative() {
    return [[FBSDKManager instance] isLoggedIn];
}

BOOL isLoginDoneNative() {
    return [[FBSDKManager instance] isLoginDone];
}

BOOL isAuthenticationSuccessNative() {
    return [[FBSDKManager instance] isAuthenticationSuccess];
}

BOOL isAuthenticationCancelNative() {
    return [[FBSDKManager instance] isAuthenticationCancel];
}

const char* getFBLastErrorNative() {
    char *error = NULL;
    NSString *lastError = [[FBSDKManager instance] lastError];
    if (lastError) {
        const char *chError = [lastError UTF8String];
        size_t len = strlen(chError);
        error = malloc(sizeof(char) * (len + 1));
        memcpy(error, chError, len + 1);
    }
    return error;
}

void graphRequestNative(const char* graphPath, const char* alertTitleError) {
    @autoreleasepool {
        NSLog(@"FBSDKManager: RUN graphRequestNative");
        NSString *nsGraphPath = [[NSString alloc] initWithUTF8String:graphPath];
        NSString *nsAlertTitleError = nil;
        if (alertTitleError != NULL) nsAlertTitleError = [[NSString alloc] initWithUTF8String:alertTitleError];
        [[FBSDKManager instance] graphRequest:nsGraphPath alertTitleError:nsAlertTitleError callback:^{}];
    }
}

BOOL isGraphRequestDoneNative() {
    return [[FBSDKManager instance] isGraphRequestDone];
}

const char* graphResponseNative() {
    NSLog(@"FBSDKManager: RUN graphResponseNative");
    char *response = NULL;
    NSString *nsResponse = [[FBSDKManager instance] graphResponse];
    if (nsResponse) {
        const char *chResponse = [nsResponse UTF8String];
        size_t len = strlen(chResponse);
        response = malloc(sizeof(char) * (len + 1));
        memcpy(response, chResponse, len + 1);
    }
    return response;
}

void showFriendListNative(const char* graphPath, const char* ids, const char* alertTitleError, const char* alertTitleInfo,
    const char* alertMessageNoFriends, const char* alertMessageNoFriendsNow, const char* alertMessageConfirmSend,
    const char* appRequestMessage, const char* alertButtonYesText, const char* alertButtonNoText, const char* buttonSendText) {
    NSString *nsIds = [[NSString alloc] initWithUTF8String:ids];
    NSLog(@"FBSDKManager.showFriendListNative: nsIds = %@", nsIds);
    NSString *nsAlertTitleInfo = [[NSString alloc] initWithUTF8String:alertTitleInfo];
    NSString *nsAlertMessageNoFriends = [[NSString alloc] initWithUTF8String:alertMessageNoFriends];
    NSString *nsAlertMessageNoFriendsNow = [[NSString alloc] initWithUTF8String:alertMessageNoFriendsNow];
    if ([nsIds isEqualToString:@"0"]) {
        [[FBSDKManager instance] showAlert:nsAlertTitleInfo message:nsAlertMessageNoFriends numButtons:1
                             buttonYesText:nil buttonNoText:nil appRequestID:nil appRequestMessage:nil];
    } else if ([nsIds isEqualToString:@""]) {
        [[FBSDKManager instance] showAlert:nsAlertTitleInfo message:nsAlertMessageNoFriendsNow numButtons:1
                             buttonYesText:nil buttonNoText:nil appRequestID:nil appRequestMessage:nil];
    } else {
        NSString *nsGraphPath = [[NSString alloc] initWithUTF8String:graphPath];
        NSString *nsAlertTitleError = [[NSString alloc] initWithUTF8String:alertTitleError];
        NSString *nsAlertMessageConfirmSend = [[NSString alloc] initWithUTF8String:alertMessageConfirmSend];
        NSString *nsAppRequestMessage = [[NSString alloc] initWithUTF8String:appRequestMessage];
        NSString *nsAlertButtonYesText = [[NSString alloc] initWithUTF8String:alertButtonYesText];
        NSString *nsAlertButtonNoText = [[NSString alloc] initWithUTF8String:alertButtonNoText];
        NSString *nsButtonSendText = [[NSString alloc] initWithUTF8String:buttonSendText];
        [[FBSDKManager instance] showFriendList:nsGraphPath ids:nsIds alertTitleError:nsAlertTitleError alertTitleInfo:nsAlertTitleInfo
                                   alertMessage:nsAlertMessageConfirmSend appRequestMessage:nsAppRequestMessage
                             alertButtonYesText:nsAlertButtonYesText alertButtonNoText:nsAlertButtonNoText buttonSendText:nsButtonSendText];
    }
}

const char* sendedIDFriendNative() {
    char *sendedID = NULL;
    NSString *nsSendedID = [Helper getSendedIDFriend];
    if (nsSendedID) {
        const char *chSendedID = [nsSendedID UTF8String];
        size_t len = strlen(chSendedID);
        sendedID = malloc(sizeof(char) * (len + 1));
        memcpy(sendedID, chSendedID, len + 1);
    }
    return sendedID;
}

void *threadLoadImage(void *voidPtr) {
    NSArray *arr = (__bridge NSArray *)voidPtr;
    if (arr) {
        for (NSDictionary *dict in arr) {
            NSString *url = [dict valueForKey:@"url"];
            UIImageView *imageView = [dict valueForKey:@"imageView"];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
            NSDictionary *data = nil;
            if (image) data = @{@"image":image, @"imageView":imageView};
            [[FBSDKManager instance] loadChangeImageView:data];
            if (data == nil) break;
        }
    }
    
    pthread_exit(0);
    
    return NULL;
}

