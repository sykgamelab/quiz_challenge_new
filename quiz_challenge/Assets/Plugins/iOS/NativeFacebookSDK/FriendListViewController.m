#import "Helper.h"
#import <signal.h>
#import "FriendListViewController.h"

@implementation FriendListViewController

- (void)viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        [Helper setSendedIDFriend:@""];
        int processorCount = (int)[[NSProcessInfo processInfo] processorCount];
        pthread_t *pthreads = [Helper getPThreads];
        for (int i = 0; i < processorCount; i++) {
            if (pthread_kill(pthreads[i], 0) != ESRCH) {
                pthread_cancel(pthreads[i]);
            }
        }
        free(pthreads);
        [Helper setPThreads:NULL];
        [[[UnityGetGLView() subviews] lastObject] removeFromSuperview];
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
