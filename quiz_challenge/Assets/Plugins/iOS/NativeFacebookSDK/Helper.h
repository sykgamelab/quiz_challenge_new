#import <pthread.h>

@interface Helper : NSObject

+ (UINavigationController *)getNavController;
+ (void)setNavController:(UINavigationController *)controller;
+ (NSString *)getSendedIDFriend;
+ (void)setSendedIDFriend:(NSString *)sendedIDFriend;
+ (pthread_t *)getPThreads;
+ (void)setPThreads:(pthread_t *)pthreads;

@end
