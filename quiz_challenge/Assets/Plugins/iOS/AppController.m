#import <UIKit/UIKit.h>
#import "UnityAppController.h"
#import "UI/UnityView.h"
#import "UI/UnityViewControllerBase.h"
#import <FBSDKCoreKit/FBSDKApplicationDelegate.h>
#import "./NativeFacebookSDK/FriendListViewController.h"
#import "./NativeFacebookSDK/Helper.h"

@interface AppController : UnityAppController

@property (nonatomic, strong) UINavigationController *navController;

- (void)willStartWithViewController:(UIViewController *)controller;

@end

@implementation AppController

@synthesize navController;

- (void)willStartWithViewController:(UIViewController *)controller {
    NSLog(@"12345");
    _rootController = [[UIViewController alloc] init];
    _rootView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _rootController.view = _rootView;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    self.navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [Helper setNavController:self.navController];
    NSLog(@"1) Helper.NavController = %@", [Helper getNavController]);
    
    [_rootView addSubview:_unityView];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance]
                    application:application
                    openURL:url
                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                    annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    
    return handled;
}

@end

IMPL_APP_CONTROLLER_SUBCLASS(AppController)
