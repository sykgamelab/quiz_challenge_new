#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("1EXrAq6J+DzqCVSwn56cnZw+H5ybrZKbnsiAjpycYpmYrZ6cnGKtgPT79P786fTy873c6On18u/06eSs7/z+6fT++L3u6fzp+PD48+nus62or6ypra6rx4qQrqitr62kr6yprZpx4KQeFs69TqVZLCIH0pf2YrZhqwTRsOUqcBEGQW7qBm/rT+qt0lytH5kmrR+ePj2en5yfn5yfrZCblF3+rupqp5qxy3ZHkryTRyfuhNIoEu4c/VuGxpSyDy9l2dVt/aUDiGixvf747+n0+/T+/On4ve3y8fT+5OS9/O7u6PD47r38/v747en88/74+hKVKb1qVjGxvfLtK6KcrREq3lJUhO9owJNI4sIGb7ieJ8gS0MCQbJXDrR+cjJueyIC9mR+cla0fnJmt8fi91PP+s6y7rbmbnsiZlo6A3O298vu96fX4ven1+PO9/O3t8fT+/K2Mm57ImZeOl9zt7fH4vdTz/rOsggxGg9rNdphww+QZsHarP8rRyHEqhiAO37mPt1qSgCvQAcP+VdYditjjgtH2zQvcFFnp/5aNHtwarhccH5ydm5S3G9Ubav75mJytHG+tt5uQm5S3G9UbapCcnJiYnZ4fnJydwe3x+L3P8vLpvd7crYOKkK2rramvuX92TCrtQpLYfLpXbPDlcHooioqVtpucmJian5yLg/Xp6e3up7Ky6ggD55E52hbGSYuqrlZZktBTifRM4tw1BWRMV/sBufaMTT4meYa3XoK3G9UbapCcnJiYna3/rJatlJueyJueyICTmYuZibZN9NoJ65RjafYQNUHjv6hXuEhEkkv2ST+5voxqPDG7rbmbnsiZlo6A3O3t8fi93vjv6SytxXHHma8R9S4SgEP47mL6w/ghRKviXBrIRDoEJK/fZkVI7APjPM+yrRxem5W2m5yYmJqfn60cK4ccLvP5vf7y8/n06fTy8+698vu96O74oLv6vReu92qQH1JDdj6yZM73xvmuq8et/6yWrZSbnsiZm46fyM6sjurqs/zt7fH4s/7y8LL87e3x+P787fH4vd747+n0+/T+/On08vO93OgWhBRDZNbxaJo2v62fdYWjZc2UTvmovojWiMSALglqawEDUs0nXMXNvfzz+b3++O/p9Pv0/vzp9PLzve293tytH5y/rZCblLcb1RtqkJycnOn18u/06eSsi62Jm57ImZ6OkNzti62Jm57ImZ6OkNzt7fH4vc/y8unnrR+c662Tm57IgJKcnGKZmZ6fnM/48fT88/74vfLzven19O69/vjvkgCgbrbUtYdVY1MoJJNEw4FLVqCYnZ4fnJKdrR+cl58fnJydeQw0lJmbjp/IzqyOrYybnsiZl46X3O3tKKcwaZKTnQ+WLLyLs+lIoZBG/4vEOpiU4Yrdy4yD6U4qFr6m2j5I8jY+7A/azshcMrLcLmVmfu1Qez7Rs907atrQ4pXDrYKbnsiAvpmFrYvp9Pv0/vzp+L3/5L388+S97fzv6SNp7gZzT/mSVuTSqUU/o2TlYvZVghgeGIYEoNqqbzQG3ROxSSwNj0UdibZN9NoJ65RjafYQs907atrQ4v/x+L3u6fzz+fzv+b3p+O/w7r38zTcXSEd5YU2Umqot6Oi8");
        private static int[] order = new int[] { 27,29,11,46,7,51,48,27,57,55,44,40,21,19,33,46,41,55,46,59,25,34,47,48,53,35,46,55,29,30,54,44,46,58,37,43,56,57,56,55,41,43,45,59,51,57,54,56,56,58,59,56,52,53,56,55,59,58,59,59,60 };
        private static int key = 157;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
