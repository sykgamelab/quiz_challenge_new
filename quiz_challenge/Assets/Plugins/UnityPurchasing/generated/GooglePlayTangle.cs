#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("94VbEqzODNjJ0dR/vdOn8kEVKVhz8P7xwXPw+/Nz8PDxXZFYK0SVGM7MGnUXozEnmfnrugjiEqLNDJ/RaCShpYMfUbpF1l21IHSS60XXc3w7cUtFr7SqcREEd7c1RWxBjYorIbAAFyPX3KEFQOoArfxt7RNH6xiTl5eu2psIEP5I8kuPh+RpzwwHLIq7LWbLM1/hlIZTb2kXsjjou4mALcFz8NPB/Pf423e5dwb88PDw9PHywloO/HFEi2CAgi9Xd+ARe2hVgF4TfVv0sfthkhK8q5dkjH8LEk9HhV1Lukyso44DJljGHig+BujMJCWqleM3Wj/95DYYSS7E11OZfCmF7U+AnIRiYFVTMcRlIaGFhcYDkwANLmBxuAVnJFzjQPPy8PHw");
        private static int[] order = new int[] { 8,1,9,13,9,9,10,10,8,13,10,11,12,13,14 };
        private static int key = 241;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
